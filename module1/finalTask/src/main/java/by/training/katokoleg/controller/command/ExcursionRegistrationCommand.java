package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.controller.validators.EntityValidator;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.dto.ValidationResult;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.TouristService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Bean("excursion_registration_command")
public class ExcursionRegistrationCommand implements Command {

    private TouristService touristService;
    private EntityValidator<TouristDTO> formatValidator;

    public ExcursionRegistrationCommand(TouristService touristService, EntityValidator<TouristDTO> formatValidator) {
        this.touristService = touristService;
        this.formatValidator = formatValidator;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String touristName = request.getParameter("tourist_name");
        String lastName = request.getParameter("last_name");
        String birthDate = request.getParameter("birth_date");
        String phone = request.getParameter("phone_number");
        String personID = request.getParameter("person_id");
        String excursionID = request.getParameter("excursion_id");
        String excursionDateTime = request.getParameter("excursion_date_time");
        TouristDTO touristDTO = new TouristDTO(touristName, lastName, birthDate, phone, personID, excursionID, excursionDateTime);

        ValidationResult<TouristDTO> formatValidation = formatValidator.validate(touristDTO);
        if (formatValidation.isValidated()) {
            try {
                ValidationResult<TouristDTO> validationResult = touristService.saveTourists(List.of(touristDTO));
                if (validationResult.isValidated()) {
                    request.getSession().setAttribute(AppConstants.MESSAGE.getName(), validationResult.getAnswer());
                    request.getSession().setAttribute("flag", 0); //flag to output validationResult message only one time
                    request.getSession().setAttribute("reference", "/excursionServlet?" +
                            "excursion_id=" + touristDTO.getExcursionGroup().getExcursionID() +
                            "&command_name="  + CommandType.WATCH_EXCURSION_COMMAND.getName() +
                            "&scope=" + AppConstants.SESSION +
                            "&view_name=" + AppConstants.INDEX_INCLUDE_VIEW);
                    response.sendRedirect("/excursionServlet?" +
                            "excursion_id=" + touristDTO.getExcursionGroup().getExcursionID() +
                            "&command_name="  + CommandType.WATCH_EXCURSION_COMMAND.getName() +
                            "&scope=" + AppConstants.SESSION +
                            "&view_name=" + AppConstants.INDEX_INCLUDE_VIEW);
                } else {
                    request.setAttribute("alreadyRegisteredTourists", validationResult.getBadData());
                    request.setAttribute(AppConstants.EXCURSION_INCLUDE_VIEW.getName(), "excursion_registration_form");
                    request.setAttribute(AppConstants.MESSAGE.getName(), "exception.message.already_registered");
                    request.getRequestDispatcher("/index.jsp").forward(request, response);
                }
            } catch (ServiceException e) {
                request.setAttribute(AppConstants.MESSAGE.getName(), e.getMessage());
                request.setAttribute(AppConstants.EXCURSION_INCLUDE_VIEW.getName(), AppConstants.MESSAGE.getName());
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("wrong_fields", formatValidation.getWrongFields());
            request.setAttribute(AppConstants.EXCURSION_INCLUDE_VIEW.getName(), "excursion_registration_form");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}
