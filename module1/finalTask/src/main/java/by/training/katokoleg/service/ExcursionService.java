package by.training.katokoleg.service;

import by.training.katokoleg.dto.ExcursionDTO;

public interface ExcursionService extends EntityService<ExcursionDTO> {
}