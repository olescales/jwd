package by.training.katokoleg.dal.connectionpool;

import java.sql.Connection;

public interface DataSource {

    Connection getConnection ();
}
