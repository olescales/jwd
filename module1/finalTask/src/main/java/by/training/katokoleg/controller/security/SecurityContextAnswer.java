package by.training.katokoleg.controller.security;

public class SecurityContextAnswer {

    private boolean valid;
    private String message;

    public SecurityContextAnswer() {
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
