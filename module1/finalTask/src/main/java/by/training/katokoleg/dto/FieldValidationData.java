package by.training.katokoleg.dto;

public class FieldValidationData {

    private String fieldContent;
    private String validationMessage;

    public FieldValidationData(String fieldContent, String validationMessage) {
        this.fieldContent = fieldContent;
        this.validationMessage = validationMessage;
    }

    public String getFieldContent() {
        return fieldContent;
    }

    public void setFieldContent(String fieldContent) {
        this.fieldContent = fieldContent;
    }

    public String getValidationMessage() {
        return validationMessage;
    }

    public void setValidationMessage(String validationMessage) {
        this.validationMessage = validationMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FieldValidationData)) return false;

        FieldValidationData that = (FieldValidationData) o;

        if (fieldContent != null ? !fieldContent.equals(that.fieldContent) : that.fieldContent != null) return false;
        return validationMessage != null ? validationMessage.equals(that.validationMessage) : that.validationMessage == null;
    }

    @Override
    public int hashCode() {
        int result = fieldContent != null ? fieldContent.hashCode() : 0;
        result = 31 * result + (validationMessage != null ? validationMessage.hashCode() : 0);
        return result;
    }
}
