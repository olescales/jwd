package by.training.katokoleg.listeners;

import by.training.katokoleg.ApplicationContext;
import by.training.katokoleg.SecurityContext;
import by.training.katokoleg.dto.ExcursionDTO;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.List;

public class ApplicationListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ApplicationContext.getInstance().initBeans();
        List<ExcursionDTO> excursions = ApplicationContext.getInstance().getInitExcursionData();
        sce.getServletContext().setAttribute("excursions", excursions);
        SecurityContext.getInstance().init(sce.getServletContext());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ApplicationContext.getInstance().destroy();
    }
}