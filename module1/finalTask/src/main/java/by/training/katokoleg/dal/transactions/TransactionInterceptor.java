package by.training.katokoleg.dal.transactions;

import java.lang.reflect.Method;

@Interceptor(clazz = TransactionSupport.class)
public class TransactionInterceptor implements BeanInterceptor {

    private TransactionalManager transactionalManager;

    public TransactionInterceptor(TransactionalManager transactionalManager) {
        this.transactionalManager = transactionalManager;
    }

    @Override
    public void before(Method method) {
        if (isMethodTransactional(method)) {
            transactionalManager.prepareTransaction();
        }
    }

    @Override
    public void success(Method method) {
        if (isMethodTransactional(method)) {
            transactionalManager.commitTransaction();
        }
    }

    @Override
    public void fail(Method method) {
        if (isMethodTransactional(method)) {
            transactionalManager.rollBackTransaction();
        }
    }

    private boolean isMethodTransactional(Method method) {
        Transaction annotation = method.getDeclaredAnnotation(Transaction.class);
        return annotation != null;
    }
}
