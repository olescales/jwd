package by.training.katokoleg.service;

import by.training.katokoleg.dto.CommentDTO;
import by.training.katokoleg.exceptions.ServiceException;

import java.util.List;

public interface CommentService extends EntityService<CommentDTO> {

    List<CommentDTO> findAllByExcursionID(long excursionID) throws ServiceException;

    List<CommentDTO> findAllByPersonID(long personID) throws ServiceException;
}
