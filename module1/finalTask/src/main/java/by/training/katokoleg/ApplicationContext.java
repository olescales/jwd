package by.training.katokoleg;

import by.training.katokoleg.controller.command.*;
import by.training.katokoleg.controller.security.PasswordAuthenticationWrapper;
import by.training.katokoleg.controller.validators.ControllerPersonValidator;
import by.training.katokoleg.controller.validators.ControllerTouristValidator;
import by.training.katokoleg.core.BeanRegistry;
import by.training.katokoleg.core.BeanRegistryImpl;
import by.training.katokoleg.dal.*;
import by.training.katokoleg.dal.transactions.ConnectionManager;
import by.training.katokoleg.dal.transactions.ConnectionManagerImpl;
import by.training.katokoleg.dal.connectionpool.DataSource;
import by.training.katokoleg.dal.connectionpool.MySQLDataSource;
import by.training.katokoleg.dto.ExcursionDTO;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.mapper.*;
import by.training.katokoleg.service.*;
import by.training.katokoleg.dal.transactions.BeanInterceptor;
import by.training.katokoleg.dal.transactions.TransactionInterceptor;
import by.training.katokoleg.dal.transactions.TransactionalManager;
import by.training.katokoleg.dal.transactions.TransactionalManagerImpl;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.List;

public class ApplicationContext {

    private static final Logger LOGGER = Logger.getLogger(ApplicationContext.class);
    private static final ApplicationContext INSTANCE = new ApplicationContext();
    private static final BeanRegistry beanRegistry = new BeanRegistryImpl();

    private ApplicationContext() {
    }

    public static ApplicationContext getInstance() {
        return INSTANCE;
    }

    public void initBeans() {
        DataSource mySQLDataSource = new MySQLDataSource();
        TransactionalManager transactionalManager = new TransactionalManagerImpl(mySQLDataSource);
        BeanInterceptor transactionalInterceptor = new TransactionInterceptor(transactionalManager);
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionalManager);
        ExcursionDAL excursionDAL = new ExcursionDALImpl(connectionManager);
        PersonDAL personDAL = new PersonDALImpl(connectionManager);
        UserRoleDAL userRoleDAL = new UserRoleDALImpl(connectionManager);
        TouristDAL touristDAL = new TouristDALImpl(connectionManager);
        CommentDAL commentDAL = new CommentDALImpl(connectionManager);
        ExcursionGroupDAL excursionGroupDAL = new ExcursionGroupDALImpl(connectionManager);
        beanRegistry.addBean(mySQLDataSource);
        beanRegistry.addBean(transactionalManager);
        beanRegistry.addBean(transactionalInterceptor);
        beanRegistry.addBean(connectionManager);
        beanRegistry.addBean(excursionDAL);
        beanRegistry.addBean(personDAL);
        beanRegistry.addBean(userRoleDAL);
        beanRegistry.addBean(touristDAL);
        beanRegistry.addBean(commentDAL);
        beanRegistry.addBean(excursionGroupDAL);

        beanRegistry.addBean(ExcursionMapper.class);
        beanRegistry.addBean(ExcursionServiceImpl.class);

        beanRegistry.addBean(PersonMapper.class);
        beanRegistry.addBean(PasswordAuthenticationWrapper.class);
        beanRegistry.addBean(ControllerPersonValidator.class);
        beanRegistry.addBean(PersonServiceImpl.class);

        beanRegistry.addBean(ExcursionGroupMapper.class);
        beanRegistry.addBean(ExcursionGroupServiceImpl.class);

        beanRegistry.addBean(TouristMapper.class);
        beanRegistry.addBean(ControllerTouristValidator.class);
        beanRegistry.addBean(TouristServiceImpl.class);

        beanRegistry.addBean(CommentMapper.class);
        beanRegistry.addBean(CommentServiceImpl.class);

        beanRegistry.addBean(PersonAccountServiceImpl.class);
        beanRegistry.addBean(WatchExcursionsCommand.class);
        beanRegistry.addBean(WatchExcursionCommand.class);
        beanRegistry.addBean(SignUpCommand.class);
        beanRegistry.addBean(LoginCommand.class);
        beanRegistry.addBean(LogoutCommand.class);
        beanRegistry.addBean(SignUpFormCommand.class);
        beanRegistry.addBean(ExcursionRegistrationCommand.class);
        beanRegistry.addBean(ExcursionFormRegistrationCommand.class);
        beanRegistry.addBean(WatchExcursionCommentsCommand.class);
        beanRegistry.addBean(WatchPersonAccountCommand.class);
        beanRegistry.addBean(WatchPersonExcursionsCommand.class);
        beanRegistry.addBean(WatchHomePageCommand.class);
        beanRegistry.addBean(WatchPersonCommentsCommand.class);
        beanRegistry.addBean(DeleteRegisteredTouristsCommand.class);
        beanRegistry.addBean(EditRegisteredTouristCommand.class);
        beanRegistry.addBean(WatchTouristDataCommand.class);
        beanRegistry.addBean(WatchPersonInfoCommand.class);
    }

    public List<ExcursionDTO> getInitExcursionData() {
        List<ExcursionDTO> excursions = null;
        try {
            excursions = ((ExcursionService) beanRegistry.getBean(ExcursionService.class)).findAll();
        } catch (ServiceException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return excursions;
    }

    @SuppressWarnings("unchecked")
    public <T> T getBean(Class<T> clazz) {
        return (T) beanRegistry.getBean(clazz);
    }

    public <T> T getBean(String beanValue) {
        return beanRegistry.getBean(beanValue);
    }

    public void destroy() {
        beanRegistry.destroy();
    }
}