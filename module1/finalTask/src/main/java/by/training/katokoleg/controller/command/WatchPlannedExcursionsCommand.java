package by.training.katokoleg.controller.command;

import by.training.katokoleg.controller.command.criteria.Criteria;
import by.training.katokoleg.controller.command.criteria.SearchCriteria;
import by.training.katokoleg.dto.ExcursionGroupDTO;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.ExcursionGroupService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class WatchPlannedExcursionsCommand implements Command {

    private ExcursionGroupService excursionGroupService;

    public WatchPlannedExcursionsCommand(ExcursionGroupService excursionGroupService) {
        this.excursionGroupService = excursionGroupService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Criteria<SearchCriteria.ExcursionGroup> searchCriteria = getCriteriaFromParams(SearchCriteria.ExcursionGroup.class, request.getParameterMap());
        try {
            List<ExcursionGroupDTO> activeExcursionGroups = excursionGroupService.findExcursionGroups(searchCriteria);
            request.setAttribute("excursionGroups", activeExcursionGroups);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

    }

    private <E extends Enum<E>> Criteria<E> getCriteriaFromParams(Class<E> enumClass, Map<String, String[]> parameterMap){
        Criteria<E> criteria = new Criteria<>(enumClass);
        Set<String> values = Arrays.stream(enumClass.getEnumConstants())
                .map(Enum::name)
                .map(String::toLowerCase)
                .collect(Collectors.toSet());

        parameterMap.entrySet()
                .stream()
                .filter(parameter -> values.contains(parameter.getKey().toUpperCase()))
                .forEach(pair -> criteria.add(E.valueOf(enumClass, pair.getKey()), pair.getValue()));

        // add CriteriaValidator
        return criteria;
    }
}