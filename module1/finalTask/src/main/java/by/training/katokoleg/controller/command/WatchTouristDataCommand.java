package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.TouristService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Bean("watch_tourist_data_command")
public class WatchTouristDataCommand implements Command {

    private TouristService touristService;

    public WatchTouristDataCommand(TouristService touristService) {
        this.touristService = touristService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long touristID = Long.parseLong(request.getParameter("tourist_id"));
        try {
            TouristDTO touristDTO = touristService.get(touristID);
            //its temporary
            request.getSession().setAttribute("edit_tourist_data", touristDTO);

            request.setAttribute("group_id", request.getParameter("group_id"));
            request.setAttribute("tourist_id", touristID);
            request.setAttribute("edit_tourist_data", touristDTO);
            request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), "edit_tourist_data");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } catch (ServiceException e) {
            request.setAttribute(AppConstants.MESSAGE.getName(), e.getMessage());
            request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), AppConstants.MESSAGE.getName());
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}