package by.training.katokoleg;

import by.training.katokoleg.controller.security.SecurityContextAnswer;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.entity.UserRole;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class SecurityContext {

    private static final SecurityContext INSTANCE = new SecurityContext();
    private static List<String> allowedCommands = new ArrayList<>();
    private final Map<String, PersonDTO> currentUsers = new ConcurrentHashMap<>(200);
    private Properties properties;
    private ThreadLocal<String> currentUserSessionID = new ThreadLocal<>();

    private SecurityContext() {
    }

    public static SecurityContext getInstance() {
        return INSTANCE;
    }

    private static List<String> initAllowedCommands(Properties properties) {
        String allowedCommands = properties.getProperty("allowed_commands");
        return List.of(allowedCommands.split(","));
    }

    public void init(ServletContext servletContext) {
        InputStream inputStream = getClass().getResourceAsStream("/security/security.properties");
        try {
            properties = new Properties();
            properties.load(inputStream);
            allowedCommands = initAllowedCommands(properties);
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to load properties.");
        }
    }

    public SecurityContextAnswer process(String commandType) {
        SecurityContextAnswer securityContextAnswer = new SecurityContextAnswer();
        if (allowedCommands.contains(commandType)) {
            securityContextAnswer.setValid(true);
            return securityContextAnswer;
        }
        String roles = properties.getProperty(commandType);
        Optional<PersonDTO> currentUser = getCurrentPerson(currentUserSessionID.get());
        if (AppConstants.CLIENT.name().equalsIgnoreCase(roles)
                && !currentUser.isPresent()) {
            securityContextAnswer.setMessage("security.should_register_first");
        }

        if (currentUser.filter(dto -> process(dto, roles)).isPresent()) {
            securityContextAnswer.setValid(true);
        }
        return securityContextAnswer;
    }

    public Optional<PersonDTO> getCurrentPerson(String sessionID) {
        return Optional.ofNullable(currentUsers.get(sessionID));
    }

    public void setCurrentUserSessionID(String sessionID) {
        currentUserSessionID.set(sessionID);
    }

    public void login(String sessionID, PersonDTO user) {
        currentUsers.put(sessionID, user);
    }

    public void logout(String sessionID) {
        currentUsers.remove(sessionID);
    }

    private boolean process(PersonDTO currentUser, String rolesCanExecuteCommand) {
        for (UserRole role : currentUser.getRoles()) {
            return Arrays.stream(rolesCanExecuteCommand.split(",")).anyMatch(r -> r.equalsIgnoreCase(role.name()));
        }
        return false;
    }

}