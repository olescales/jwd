package by.training.katokoleg.dto;

public class Message <T> {

    private String badMessage;
    private String goodMessage;
    private T dto;

    public String getBadMessage() {
        return badMessage;
    }

    public void setBadMessage(String badMessage) {
        this.badMessage = badMessage;
    }

    public String getGoodMessage() {
        return goodMessage;
    }

    public void setGoodMessage(String goodMessage) {
        this.goodMessage = goodMessage;
    }

    public T getDto() {
        return dto;
    }

    public void setDto(T dto) {
        this.dto = dto;
    }
}
