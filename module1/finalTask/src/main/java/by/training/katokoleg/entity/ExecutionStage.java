package by.training.katokoleg.entity;

public enum ExecutionStage {
    ASSEMBLE(1),
    WILL_TAKE_PLACE(2),
    FINISHED(3);

    private int id;

    ExecutionStage(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
