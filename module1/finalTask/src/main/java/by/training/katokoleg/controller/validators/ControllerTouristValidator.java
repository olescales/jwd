package by.training.katokoleg.controller.validators;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.FieldValidationData;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.dto.ValidationResult;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Bean
public class ControllerTouristValidator implements EntityValidator<TouristDTO> {

    @Override
    public ValidationResult validate(TouristDTO tourist) {
        ValidationResult validationResult = new ValidationResult();

        String personName = tourist.getName();
        if (personName.isEmpty()) {
            validationResult.getWrongFields().put("touristName", new FieldValidationData(tourist.getName(),"<fmt:message key=\"validation.js.cant_be_blank\"/>"));
        } else if (!checkName(personName)) {
            validationResult.getWrongFields().put("touristName", new FieldValidationData(tourist.getName(),"<fmt:message key=\"validation.js.invalid_name\"/>"));
        }

        String lastName = tourist.getLastName();
        if (lastName.isEmpty()) {
            validationResult.getWrongFields().put("lastName", new FieldValidationData(tourist.getName(),"<fmt:message key=\"validation.js.cant_be_blank\"/>"));
        } else if (!checkName(lastName)) {
            validationResult.getWrongFields().put("lastName", new FieldValidationData(tourist.getLastName(),"<fmt:message key=\"validation.js.invalid_name\"/>"));
        }

        String birthDate = tourist.getBirthDate();
        if (!checkBirthDateFormat(birthDate) || !checkBirthDateContent(birthDate)) {
            validationResult.getWrongFields().put("birthDate", new FieldValidationData(tourist.getBirthDate(),"<fmt:message key=\"validation.js.invalid_date\"/>"));
        }

        String phone = tourist.getPhone();
        if (phone.isEmpty()) {
            validationResult.getWrongFields().put("phone", new FieldValidationData(tourist.getName(),"<fmt:message key=\"validation.js.cant_be_blank\"/>"));
        } else if (!checkPhone(phone)) {
            validationResult.getWrongFields().put("phone", new FieldValidationData(tourist.getPhone(),"<fmt:message key=\"validation.js.invalid_phone\"/>"));
        }

        if (!validationResult.getWrongFields().isEmpty()) {
            validationResult.setValidated(false);
        }
        return validationResult;
    }

    private boolean checkName(String name) {
        return name.matches("^[A-Z][a-z]{1,29}$|^[А-Я][а-я]{1,29}$");
    }

    private boolean checkPhone(String phone) {
        return phone.matches("^(25|29|33|44)-[\\d]{3}-[\\d]{2}-[\\d]{2}$");
    }

    private boolean checkBirthDateFormat(String birthDate) {
        return birthDate.matches("^[\\d]{4}-[\\d]{2}-[\\d]{2}");
    }

    private boolean checkBirthDateContent(String birthDate) {
        List<Integer> date = Arrays.stream(birthDate.split("-")).map(Integer::parseInt).collect(Collectors.toList());
        int year = date.get(0);
        int month = date.get(1);
        int days = date.get(2);
        return DateChecker.checkDate(days, month, year);
    }
}
