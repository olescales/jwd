package by.training.katokoleg.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class ExcursionDTO {

    private Long id;
    private String topic;
    private String description;
    private int amountOfPeople;
    private BigDecimal cost;
    private String photoReference;
    private String minPhotoReference;
    private String location;
    private List<LocalDateTime> excursionSchedule;
    private List<String> formattedExcursionSchedule;

    public ExcursionDTO() {
    }

    public ExcursionDTO(String topic, String description, BigDecimal cost, String photoReference, String location) {
        this.topic = topic;
        this.description = description;
        this.cost = cost;
        this.photoReference = photoReference;
        this.location = location;
    }

    public List<LocalDateTime> getExcursionSchedule() {
        return excursionSchedule;
    }

    public void setExcursionSchedule(List<LocalDateTime> excursionSchedule) {
        this.excursionSchedule = excursionSchedule;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAmountOfPeople() {
        return amountOfPeople;
    }

    public void setAmountOfPeople(int amountOfPeople) {
        this.amountOfPeople = amountOfPeople;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMinPhotoReference() {
        return minPhotoReference;
    }

    public void setMinPhotoReference(String minPhotoReference) {
        this.minPhotoReference = minPhotoReference;
    }

    public List<String> getFormattedExcursionSchedule() {
        return formattedExcursionSchedule;
    }

    public void setFormattedExcursionSchedule(List<String> formattedExcursionSchedule) {
        this.formattedExcursionSchedule = formattedExcursionSchedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExcursionDTO)) return false;

        ExcursionDTO that = (ExcursionDTO) o;

        if (amountOfPeople != that.amountOfPeople) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (topic != null ? !topic.equals(that.topic) : that.topic != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (cost != null ? !cost.equals(that.cost) : that.cost != null) return false;
        if (photoReference != null ? !photoReference.equals(that.photoReference) : that.photoReference != null)
            return false;
        if (minPhotoReference != null ? !minPhotoReference.equals(that.minPhotoReference) : that.minPhotoReference != null)
            return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (excursionSchedule != null ? !excursionSchedule.equals(that.excursionSchedule) : that.excursionSchedule != null)
            return false;
        return formattedExcursionSchedule != null ? formattedExcursionSchedule.equals(that.formattedExcursionSchedule) : that.formattedExcursionSchedule == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + amountOfPeople;
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (photoReference != null ? photoReference.hashCode() : 0);
        result = 31 * result + (minPhotoReference != null ? minPhotoReference.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (excursionSchedule != null ? excursionSchedule.hashCode() : 0);
        result = 31 * result + (formattedExcursionSchedule != null ? formattedExcursionSchedule.hashCode() : 0);
        return result;
    }
}
