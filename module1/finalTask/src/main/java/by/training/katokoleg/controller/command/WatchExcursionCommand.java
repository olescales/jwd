package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.ExcursionDTO;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.ExcursionService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Bean("watch_excursion_command")
public class WatchExcursionCommand implements Command {
    private ExcursionService excursionService;

    public WatchExcursionCommand(ExcursionService excursionService) {
        this.excursionService = excursionService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long excursionID = Long.parseLong(request.getParameter("excursion_id"));
        String viewName = request.getParameter("view_name");
        String scope = request.getParameter("scope");
        try {
            ExcursionDTO excursionDTO = excursionService.get(excursionID);
            if (AppConstants.REQUEST.getName().equalsIgnoreCase(scope)) {
                request.setAttribute(AppConstants.valueOf(viewName).getName(), "excursion");
            } else {
                request.getSession().setAttribute(AppConstants.valueOf(viewName).getName(), "excursion");
            }
            request.getSession().setAttribute("excursion", excursionDTO);
            Cookie excursionCookie = new Cookie("excursionName", excursionDTO.getTopic());
            response.addCookie(excursionCookie);
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } catch (ServiceException e) {
            request.setAttribute(AppConstants.MESSAGE.getName(), e.getMessage());
            request.getRequestDispatcher("/jsp/error_page.jsp").forward(request, response);
        }
    }
}