package by.training.katokoleg.dal;

import by.training.katokoleg.controller.command.criteria.Criteria;
import by.training.katokoleg.dal.transactions.ConnectionManager;
import by.training.katokoleg.entity.ExcursionGroup;
import by.training.katokoleg.entity.ExecutionStage;
import by.training.katokoleg.exceptions.DAOException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ExcursionGroupDALImpl implements ExcursionGroupDAL {

    private static final Logger LOGGER = Logger.getLogger(ExcursionGroupDALImpl.class);
    private static final String SAVE_PARAMS_QUERY = "INSERT INTO excursions.excursion_group (excursion_id, excursion_date_time, execution_stage_id," +
            "guide_id, registered_tourists, group_size) VALUES (?,?,?,?,?,?)";
    private static final String GET_EXCURSION_SCHEDULE = "SELECT DISTINCT * FROM (SELECT eg.excursion_id, eg.excursion_date_time FROM excursions.excursion_group as eg WHERE excursion_id = ? and execution_stage_id != ?) t";
    private static final String WATCH_EXCURSIONS_BY_PERSON_ID = "SELECT DISTINCT eg.id, eg.excursion_id, eg.excursion_date_time, eg.execution_stage_id, eg.guide_id, eg.registered_tourists, eg.group_size " +
            "FROM excursions.tourist as t INNER JOIN excursions.excursion_group as eg ON t.excursion_group_id = eg.id " +
            "WHERE t.person_id = ? and eg.execution_stage_id != ?";
    private static final String FIND_EXCURSION_GROUPS_BY_EXCURSION_ID_AND_EXCURSION_DATE_TIME = "SELECT * FROM excursions.excursion_group WHERE excursion_id = ? AND excursion_date_time = ?";
    private static final String GET_EXCURSION_GROUP_BY_ITS_ID = "SELECT * FROM excursions.excursion_group WHERE id = ?";
    private static final String INCREMENT_REGISTERED_TOURIST_FIELD = "UPDATE excursions.excursion_group SET registered_tourists = registered_tourists + ? WHERE id = ?";
    private static final String GET_EXECUTION_STAGE_BY_ID = "SELECT execution_stage FROM excursions.execution_stage WHERE id = ? ";
    private static final String DECREMENT_REGISTERED_TOURISTS_FIELD = "UPDATE excursions.excursion_group SET registered_tourists = registered_tourists - ? WHERE id = ?";
    private ConnectionManager connectionManager;

    public ExcursionGroupDALImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(ExcursionGroup entity) throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public ExcursionGroup get(Long aLong) throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Long aLong, ExcursionGroup entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long aLong) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<ExcursionGroup> findAll() throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<ExcursionGroup> getExcursionSchedule(long excursionID) throws DAOException {
        List<ExcursionGroup> excursionParams = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_EXCURSION_SCHEDULE)) {
            preparedStatement.setLong(1, excursionID);
            preparedStatement.setLong(2, ExecutionStage.FINISHED.getId());
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    LocalDateTime localDateTime = resultSet.getTimestamp("excursion_date_time").toLocalDateTime();
                    ExcursionGroup excursionGroup = new ExcursionGroup(localDateTime);
                    excursionParams.add(excursionGroup);
                }
                return excursionParams;
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public List<ExcursionGroup> getExcursionsGroupByTouristID(long touristID) throws DAOException {
        List<ExcursionGroup> excursionsScheduleAndState = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(WATCH_EXCURSIONS_BY_PERSON_ID)) {
            preparedStatement.setLong(1, touristID);
            preparedStatement.setLong(2, ExecutionStage.FINISHED.getId());
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    long id = resultSet.getLong("id");
                    long excursionID = resultSet.getLong("excursion_id");
                    LocalDateTime excursionDateTime = resultSet.getTimestamp("excursion_date_time").toLocalDateTime();
                    int executionStageID = resultSet.getInt("execution_stage_id");
                    long guideID = resultSet.getLong("guide_id");
                    ExcursionGroup excursionGroup = new ExcursionGroup(id, excursionID, excursionDateTime, executionStageID, guideID);
                    excursionsScheduleAndState.add(excursionGroup);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException(e.getMessage(), e);
        }
        return excursionsScheduleAndState;
    }

    @Override
    public List<ExcursionGroup> getExcursionGroupsByExcursionIDAndExcursionDate(String excursionID, String excursionDateTime) throws DAOException {
        List<ExcursionGroup> excursionParams = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_EXCURSION_GROUPS_BY_EXCURSION_ID_AND_EXCURSION_DATE_TIME)) {
            preparedStatement.setLong(1, Long.parseLong(excursionID));
            LocalDateTime localDateTime = LocalDateTime.parse(excursionDateTime);
            preparedStatement.setTimestamp(2, Timestamp.valueOf(localDateTime));
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    long id = resultSet.getLong("id");
                    int executionStageID = resultSet.getInt("execution_stage_id");
                    long guideID = resultSet.getLong("guide_id");
                    int registeredTourists = resultSet.getInt("registered_tourists");
                    int groupSize = resultSet.getInt("group_size");
                    ExcursionGroup excursionGroup = new ExcursionGroup(id, Long.parseLong(excursionID), localDateTime, executionStageID, guideID, registeredTourists, groupSize);
                    excursionParams.add(excursionGroup);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException(e.getMessage(), e);
        }
        return excursionParams;
    }

    @Override
    public Optional<ExcursionGroup> getExcursionGroupByID(long excursionParamsID) throws DAOException {
        Optional<ExcursionGroup> excursionGroup = Optional.empty();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_EXCURSION_GROUP_BY_ITS_ID)) {
            preparedStatement.setLong(1, excursionParamsID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    long excursionID = resultSet.getLong("excursion_id");
                    LocalDateTime excursionDateTime = resultSet.getTimestamp("excursion_date_time").toLocalDateTime();
                    int executionStageID = resultSet.getInt("execution_stage_id");
                    long guideID = resultSet.getLong("guide_id");
                    int registeredTourists = resultSet.getInt("registered_tourists");
                    int groupSize = resultSet.getInt("group_size");
                    excursionGroup = Optional.of(new ExcursionGroup(excursionParamsID, excursionID, excursionDateTime, executionStageID, guideID, registeredTourists, groupSize));
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException(e.getMessage(), e);
        }
        return excursionGroup;
    }

    @Override
    public ExcursionGroup createNewExcursionGroup(ExcursionGroup group) throws DAOException {
        long newGroupID = 0L;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SAVE_PARAMS_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setLong(1, group.getExcursionID());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(group.getExcursionDateTime()));
            preparedStatement.setInt(3, 1);
            preparedStatement.setLong(4, group.getGuideID());
            preparedStatement.setInt(5, 0);
            preparedStatement.setInt(6, group.getGroupSize());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Create excursion group failed.");
            }
            ResultSet generatedKey = preparedStatement.getGeneratedKeys();
            if (generatedKey.next()) {
                newGroupID = generatedKey.getLong(1);
            }
            group.setId(newGroupID);
            group.setRegisteredTourist(0);
            group.setExecutionStageID(1);
            return group;
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException("exception.message.db_save_tourists_error", e);
        }
    }

    @Override
    public void addTouristsToExcursionGroup(long groupID, int numberOfTourists) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INCREMENT_REGISTERED_TOURIST_FIELD)) {
            preparedStatement.setInt(1, numberOfTourists);
            preparedStatement.setLong(2, groupID);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException("exception.message.db_save_tourists_error", e);
        }
    }

    @Override
    public void reduceTheNumberOfRegisteredTourists(int numberOfTourists, long groupID) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DECREMENT_REGISTERED_TOURISTS_FIELD)) {
            preparedStatement.setInt(1, numberOfTourists);
            preparedStatement.setLong(2, groupID);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException("exception.message.deletion_failed", e);
        }
    }

    @Override
    public List<ExcursionGroup> getExcursionGroupsByCriteria(String criteriaQuery) throws DAOException {
        List<ExcursionGroup> excursionGroups = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(criteriaQuery)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    long excursionGroupID = resultSet.getLong("id");
                    long excursionID = resultSet.getLong("excursion_id");
                    LocalDateTime excursionDateTime = resultSet.getTimestamp("excursion_date_time").toLocalDateTime();
                    int executionStageID = resultSet.getInt("execution_stage_id");
                    long guideID = resultSet.getLong("guide_id");
                    int registeredTourist = resultSet.getInt("registered_tourists");
                    int groupSize = resultSet.getInt("group_size");
                    excursionGroups.add(new ExcursionGroup(excursionGroupID, excursionID, excursionDateTime, executionStageID, guideID, registeredTourist, groupSize));
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException("exception.message.db_get_data_error", e);
        }
        return excursionGroups;
    }

    @Override
    public ExecutionStage getExecutionStageByID(long executionStageID) throws DAOException {
        ExecutionStage executionStage = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_EXECUTION_STAGE_BY_ID)) {
            preparedStatement.setLong(1, executionStageID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    String strExecutionStage = resultSet.getString("execution_stage");
                    executionStage = ExecutionStage.valueOf(strExecutionStage.toUpperCase());
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException(e.getMessage(), e);
        }
        return executionStage;
    }
}
