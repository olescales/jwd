package by.training.katokoleg.mapper;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.ExcursionDTO;
import by.training.katokoleg.entity.Excursion;

@Bean
public class ExcursionMapper implements Mapper<ExcursionDTO, Excursion> {
    @Override
    public Excursion fromDTOtoEntity(ExcursionDTO dto) {
        Excursion excursion = new Excursion();
        if (dto.getId() != null) {
            excursion.setId(dto.getId());
        }
        if (dto.getTopic() != null) {
            excursion.setTopic(dto.getTopic());
        }
        if (dto.getDescription() != null) {
            excursion.setDescription(dto.getDescription());
        }
        if (dto.getAmountOfPeople() > 0) {
            excursion.setAmountOfPeople(dto.getAmountOfPeople());
        }
        if (dto.getCost() != null) {
            excursion.setCost(dto.getCost());
        }
        if (dto.getPhotoReference() != null) {
            excursion.setPhotoReference(dto.getPhotoReference());
        }
        if (dto.getLocation() != null) {
            excursion.setLocation(dto.getLocation());
        }
        return excursion;
    }

    @Override
    public ExcursionDTO fromEntityToDTO(Excursion entity) {
        ExcursionDTO excursionDTO = new ExcursionDTO();
        if (entity.getId() != null) {
            excursionDTO.setId(entity.getId());
        }
        if (entity.getTopic() != null) {
            excursionDTO.setTopic(entity.getTopic());
        }
        if (entity.getDescription() != null) {
            excursionDTO.setDescription(entity.getDescription());
        }
        if (entity.getAmountOfPeople() > 0) {
            excursionDTO.setAmountOfPeople(entity.getAmountOfPeople());
        }
        if (entity.getCost() != null) {
            excursionDTO.setCost(entity.getCost());
        }
        if (entity.getPhotoReference() != null) {
            String photoReference = entity.getPhotoReference();
            String minPhotoReference = new StringBuilder(photoReference)
                    .insert(photoReference.length()-4, "_min").toString();
            excursionDTO.setPhotoReference(photoReference);
            excursionDTO.setMinPhotoReference(minPhotoReference);
        }
        if (entity.getLocation() != null) {
            excursionDTO.setLocation(entity.getLocation());
        }
        return excursionDTO;
    }
}
