package by.training.katokoleg.service.validator;

import by.training.katokoleg.dto.ValidationResult;

import java.util.List;

public interface ServiceValidator<T> {

    ValidationResult<T> validate(List<T> t);
}
