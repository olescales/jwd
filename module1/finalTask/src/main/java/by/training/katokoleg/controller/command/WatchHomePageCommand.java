package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Bean("watch_homepage_command")
public class WatchHomePageCommand implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().removeAttribute(AppConstants.INDEX_INCLUDE_VIEW.getName());
        request.removeAttribute(AppConstants.INDEX_INCLUDE_VIEW.getName());
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}