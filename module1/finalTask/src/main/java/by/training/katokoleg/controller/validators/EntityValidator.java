package by.training.katokoleg.controller.validators;

import by.training.katokoleg.dto.ValidationResult;

public interface EntityValidator<T> {

    ValidationResult<T> validate (T dto);
}
