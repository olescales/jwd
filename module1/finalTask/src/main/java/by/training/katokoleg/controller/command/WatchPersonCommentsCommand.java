package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.CommentDTO;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.CommentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Bean("watch_person_comments_command")
public class WatchPersonCommentsCommand implements Command {

    private CommentService commentService;

    public WatchPersonCommentsCommand(CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long personID = ((PersonDTO) request.getSession().getAttribute("person")).getId();
        try {
            List<CommentDTO> comments = commentService.findAllByPersonID(personID);
            if (comments.isEmpty()) {
                request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), "message");
                request.setAttribute(AppConstants.MESSAGE.getName(), "message.no_comments_yet");
            } else {
                request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), "comments");
                request.setAttribute("comments", comments);
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } catch (ServiceException e) {
            request.setAttribute(AppConstants.MESSAGE.getName(), "exception.message.error_occurred");
            request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), "message");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}
