package by.training.katokoleg.dto;

import java.io.Serializable;

public class TouristDTO implements Serializable {

    private static final long serialVersionUID = 196290448386305485L;
    private String id;
    private String name;
    private String lastName;
    private String birthDate;
    private String phone;
    private String personID;
    private String excursionID;
    private String excursionGroupID;
    private ExcursionGroupDTO excursionGroup;
    private String excursionDateTime;
    private String message;
    private String ticketID;

    public TouristDTO() {
    }

    public TouristDTO(String name, String lastName, String birthDate, String phone, String personID, String excursionID, String excursionDateTime) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.phone = phone;
        this.personID = personID;
        this.excursionID = excursionID;
        this.excursionDateTime = excursionDateTime;
    }

    public TouristDTO(String name, String lastName, String birthDate, String excursionDateTime) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.excursionDateTime = excursionDateTime;
    }

    public TouristDTO(String name, String lastName, String birthDate) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }

    public TouristDTO(String name, String lastName, String birthDate, String phone, String excursionGroupID) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.phone = phone;
        this.excursionGroupID = excursionGroupID;
    }

    public TouristDTO(String id, String name, String lastName, String birthDate, String phone, String excursionDateTime) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.phone = phone;
        this.excursionDateTime = excursionDateTime;
    }

    public String getExcursionGroupID() {
        return excursionGroupID;
    }

    public void setExcursionGroupID(String excursionGroupID) {
        this.excursionGroupID = excursionGroupID;
    }

    public String getExcursionID() {
        return excursionID;
    }

    public void setExcursionID(String excursionID) {
        this.excursionID = excursionID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public ExcursionGroupDTO getExcursionGroup() {
        return excursionGroup;
    }

    public void setExcursionGroup(ExcursionGroupDTO excursionGroup) {
        this.excursionGroup = excursionGroup;
    }

    public String getExcursionDateTime() {
        return excursionDateTime;
    }

    public void setExcursionDateTime(String excursionDateTime) {
        this.excursionDateTime = excursionDateTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTicketID() {
        return ticketID;
    }

    public void setTicketID(String ticketID) {
        this.ticketID = ticketID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TouristDTO)) return false;

        TouristDTO that = (TouristDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (birthDate != null ? !birthDate.equals(that.birthDate) : that.birthDate != null) return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
        if (personID != null ? !personID.equals(that.personID) : that.personID != null) return false;
        if (excursionID != null ? !excursionID.equals(that.excursionID) : that.excursionID != null) return false;
        if (excursionGroup != null ? !excursionGroup.equals(that.excursionGroup) : that.excursionGroup != null)
            return false;
        if (excursionDateTime != null ? !excursionDateTime.equals(that.excursionDateTime) : that.excursionDateTime != null)
            return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        return ticketID != null ? ticketID.equals(that.ticketID) : that.ticketID == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (personID != null ? personID.hashCode() : 0);
        result = 31 * result + (excursionID != null ? excursionID.hashCode() : 0);
        result = 31 * result + (excursionGroup != null ? excursionGroup.hashCode() : 0);
        result = 31 * result + (excursionDateTime != null ? excursionDateTime.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (ticketID != null ? ticketID.hashCode() : 0);
        return result;
    }
}
