package by.training.katokoleg.mapper;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.CommentDTO;
import by.training.katokoleg.entity.Comment;

@Bean
public class CommentMapper implements Mapper<CommentDTO, Comment> {

    @Override
    public Comment fromDTOtoEntity(CommentDTO dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public CommentDTO fromEntityToDTO(Comment entity) {
        CommentDTO commentDTO = new CommentDTO();
        if (entity.getTextContent() != null) {
            commentDTO.setTextContent(entity.getTextContent());
        }
        if (entity.getCommentDateTime() != null) {
            commentDTO.setCommentDateTime(entity.getCommentDateTime());
        }


        return commentDTO;
    }
}
