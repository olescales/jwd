package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.controller.validators.EntityValidator;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.dto.ValidationResult;
import by.training.katokoleg.entity.UserRole;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.PersonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Bean("sign_up_command")
public class SignUpCommand implements Command {

    private PersonService personService;
    private EntityValidator<PersonDTO> formatValidator;

    public SignUpCommand(PersonService personService, EntityValidator<PersonDTO> formatValidator) {
        this.personService = personService;
        this.formatValidator = formatValidator;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String personName = request.getParameter("person_name");
        String lastName = request.getParameter("last_name");
        String dateOfBirth = request.getParameter("birth_date");
        String email = request.getParameter("e-mail");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String passwordConfirmation = request.getParameter("password_confirmation");
        String phone = request.getParameter("phone_number");

        PersonDTO personDTO = new PersonDTO(personName, lastName, dateOfBirth, email, login, password, passwordConfirmation, phone, List.of(UserRole.CLIENT));
        ValidationResult<PersonDTO> formatValidation = formatValidator.validate(personDTO);
        if (formatValidation.isValidated()) {
            try {
                ValidationResult<PersonDTO> serviceValidation = personService.signUp(personDTO);
                if (serviceValidation.isValidated()) {
                    request.getSession().setAttribute(AppConstants.MESSAGE.getName(), "text.person_registration_is_ok");
                    request.getSession().setAttribute("flag", 0); //flag to output validationResult message only one time
                    request.getSession().setAttribute("reference", request.getRequestURL());
                    response.sendRedirect("/excursionServlet");
                } else {
                    request.setAttribute(AppConstants.WRONG_FIELDS.getName(), serviceValidation.getWrongFields());
                    request.getRequestDispatcher("/jsp/sign_up_form.jsp").forward(request, response);
                }
            } catch (ServiceException e) {
                request.setAttribute(AppConstants.MESSAGE.getName(), e.getMessage());
                request.getRequestDispatcher("/jsp/error_page.jsp").forward(request, response);
            }
        } else {
            request.setAttribute(AppConstants.WRONG_FIELDS.getName(), formatValidation.getWrongFields());
            request.getRequestDispatcher("/jsp/sign_up_form.jsp").forward(request, response);
        }
    }
}