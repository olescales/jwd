package by.training.katokoleg.mapper;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.entity.Person;

import java.time.LocalDate;

@Bean
public class PersonMapper implements Mapper<PersonDTO, Person> {

    public Person fromDTOtoEntity (PersonDTO personDTO) {
        Person person = new Person();
        if (personDTO.getId() != 0) {
            person.setId(personDTO.getId());
        }
        if (personDTO.getName() != null) {
            person.setName(personDTO.getName());
        }
        if (personDTO.getLastName() != null) {
            person.setLastName(personDTO.getLastName());
        }
        if (personDTO.getEmail() != null) {
            person.setEmail(personDTO.getEmail());
        }
        if (personDTO.getBirthDate() != null) {
            person.setBirthDate(LocalDate.parse(personDTO.getBirthDate()));
        }
        if (personDTO.getLogin() != null) {
            person.setLogin(personDTO.getLogin());
        }
        if (personDTO.getPassword() != null) {
            person.setPassword(personDTO.getPassword());
        }
        if (personDTO.getRoles() != null) {
            person.setRoles(personDTO.getRoles());
        }
        if (personDTO.getPhoneNumber() != null) {
            person.setPhoneNumber(personDTO.getPhoneNumber());
        }
        if (personDTO.getPhotoReference() != null) {
            person.setPhotoReference(personDTO.getPhotoReference());
        }
        return person;
    }

    public PersonDTO fromEntityToDTO (Person entity) {
        PersonDTO personDTO = new PersonDTO();
        if (entity.getId() != null) {
            personDTO.setId(entity.getId());
        }
        if (entity.getName() != null) {
            personDTO.setName(entity.getName());
        }
        if (entity.getLastName() != null) {
            personDTO.setLastName(entity.getLastName());
        }
        if (entity.getEmail() != null) {
            personDTO.setEmail(entity.getEmail());
        }
        if (entity.getBirthDate() != null) {
            personDTO.setBirthDate(entity.getBirthDate().toString());
        }
        if (entity.getLogin() != null) {
            personDTO.setLogin(entity.getLogin());
        }
        if (entity.getPassword() != null) {
            personDTO.setPassword(entity.getPassword());
        }
        if (entity.getRoles() != null) {
            personDTO.setRoles(entity.getRoles());
        }
        if (entity.getPhoneNumber() != null) {
            personDTO.setPhoneNumber(entity.getPhoneNumber());
        }
        if (entity.getPhotoReference() != null) {
            personDTO.setPhotoReference(entity.getPhotoReference());
        }
        return personDTO;
    }
}
