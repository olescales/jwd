package by.training.katokoleg.service;

import by.training.katokoleg.dto.Message;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.dto.ValidationResult;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.dal.transactions.Transaction;

public interface PersonService extends EntityService<PersonDTO> {

    @Transaction
    ValidationResult<PersonDTO> signUp(PersonDTO personDTO) throws ServiceException;

    Message<PersonDTO> login(String login, String password);

    long getPersonID(PersonDTO testPerson) throws ServiceException;
}
