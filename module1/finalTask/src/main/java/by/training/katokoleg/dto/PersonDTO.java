package by.training.katokoleg.dto;

import by.training.katokoleg.entity.UserRole;

import java.io.Serializable;
import java.util.List;

public class PersonDTO implements Serializable {

    private static final long serialVersionUID = 1819103158972390973L;
    private long id;
    private String name;
    private String lastName;
    private String birthDate;
    private String email;
    private String login;
    private String password;
    private String passwordConfirmation;
    private String phoneNumber;
    private List<UserRole> roles;
    private String photoReference;

    public PersonDTO() {
    }

    public PersonDTO(String name, String birthDate, String email, String login, String password, List<UserRole> roles) {
        this.name = name;
        this.birthDate = birthDate;
        this.email = email;
        this.login = login;
        this.password = password;
        this.roles = roles;
    }

    public PersonDTO(String name, String lastName, String birthDate, String email, String login, String password, String passwordConfirmation, String phoneNumber, List<UserRole> roles) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.email = email;
        this.login = login;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.phoneNumber = phoneNumber;
        this.roles = roles;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonDTO)) return false;

        PersonDTO personDTO = (PersonDTO) o;

        if (id != personDTO.id) return false;
        if (name != null ? !name.equals(personDTO.name) : personDTO.name != null) return false;
        if (lastName != null ? !lastName.equals(personDTO.lastName) : personDTO.lastName != null) return false;
        if (birthDate != null ? !birthDate.equals(personDTO.birthDate) : personDTO.birthDate != null) return false;
        if (email != null ? !email.equals(personDTO.email) : personDTO.email != null) return false;
        if (login != null ? !login.equals(personDTO.login) : personDTO.login != null) return false;
        if (password != null ? !password.equals(personDTO.password) : personDTO.password != null) return false;
        if (passwordConfirmation != null ? !passwordConfirmation.equals(personDTO.passwordConfirmation) : personDTO.passwordConfirmation != null)
            return false;
        if (phoneNumber != null ? !phoneNumber.equals(personDTO.phoneNumber) : personDTO.phoneNumber != null)
            return false;
        if (roles != null ? !roles.equals(personDTO.roles) : personDTO.roles != null) return false;
        return photoReference != null ? photoReference.equals(personDTO.photoReference) : personDTO.photoReference == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (passwordConfirmation != null ? passwordConfirmation.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        result = 31 * result + (photoReference != null ? photoReference.hashCode() : 0);
        return result;
    }
}
