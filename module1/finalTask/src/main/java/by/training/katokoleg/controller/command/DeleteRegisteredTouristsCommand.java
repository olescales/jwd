package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.TouristService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Bean("delete_registered_tourists_command")
public class DeleteRegisteredTouristsCommand implements Command {

    private TouristService touristService;

    public DeleteRegisteredTouristsCommand(TouristService touristService) {
        this.touristService = touristService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long groupID = Long.parseLong(request.getParameter("group_id"));
        boolean manyTourists = Boolean.parseBoolean(request.getParameter("many_tourists"));
        Long[] touristsID;
        if (manyTourists) {
            List<TouristDTO> tourists = (List) request.getSession().getAttribute("tourists");
            touristsID = tourists.stream().map(TouristDTO::getId).map(Long::parseLong).toArray(Long[]::new);
        } else {
            long touristID = Long.parseLong(request.getParameter("tourist_id"));
            touristsID = new Long[]{touristID};
        }
        try {
            touristService.deleteTouristsFromGroup(touristsID, groupID);
            response.sendRedirect("/excursionServlet/personal_account/excursions?command_name=" + CommandType.WATCH_PERSON_EXCURSIONS_COMMAND.getName());
        } catch (ServiceException e) {
            request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), AppConstants.MESSAGE.getName());
            request.setAttribute(AppConstants.MESSAGE.getName(), "exception.message.deletion_failed");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}
