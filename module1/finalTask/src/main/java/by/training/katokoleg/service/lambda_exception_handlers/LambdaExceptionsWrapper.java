package by.training.katokoleg.service.lambda_exception_handlers;

import java.util.function.Consumer;

public class LambdaExceptionsWrapper {

    public static <T> Consumer<T> throwingConsumerWrapper(ThrowingConsumer<T, Exception> throwingConsumer) {
        return i -> {
            try {
                throwingConsumer.accept(i);
            } catch (Exception ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        };
    }
}
