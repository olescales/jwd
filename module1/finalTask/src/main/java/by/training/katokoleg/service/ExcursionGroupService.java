package by.training.katokoleg.service;

import by.training.katokoleg.controller.command.criteria.Criteria;
import by.training.katokoleg.dto.ExcursionGroupDTO;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.entity.ExcursionGroup;
import by.training.katokoleg.entity.ExecutionStage;
import by.training.katokoleg.exceptions.ServiceException;

import java.util.List;
import java.util.function.Predicate;

public interface ExcursionGroupService extends EntityService<ExcursionGroupDTO> {

    List<ExcursionGroupDTO> getExcursionsByPersonID(long personID) throws ServiceException;

    List<ExcursionGroup> getExcursionGroupsByExcursionIDAndExcursionDateTime(String excursionID, String excursionDate) throws ServiceException;

    ExcursionGroup getExcursionGroupByID(long excursionParamsID) throws ServiceException;

    ExcursionGroup createNewExcursionGroup(ExcursionGroup excursionGroup) throws ServiceException;

    void registerTouristsInExcursionGroup(List<TouristDTO> touristDTO) throws ServiceException;

    List<ExcursionGroup> getExcursionSchedule(long excursionID) throws ServiceException;

    ExecutionStage getGroupExecutionStageByExecutionStageID(long executionStageID) throws ServiceException;

    void removeTouristsFromGroup(int numberOfTourists, long groupID) throws ServiceException;

    <E> List<ExcursionGroupDTO> findExcursionGroups(Criteria<E> criteria) throws ServiceException;
}
