package by.training.katokoleg.controller.command.criteria;

public final class SearchCriteria {

    public enum ExcursionGroup {
        EXECUTION_STAGE_VALUES,
        EXCURSION_DATE_TIME,
        GUIDE_ID,
        GROUP_SIZE,
        TOURIST_REGISTERED;
    }
}
