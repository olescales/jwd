package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.PersonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Bean("watch_person_info_command")
public class WatchPersonInfoCommand implements Command {

    private PersonService personService;

    public WatchPersonInfoCommand(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long guideID = Long.parseLong(request.getParameter("guide_id"));
        try {
            PersonDTO guide = personService.get(guideID);
            request.setAttribute("guide", guide);
            request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), "guide_info");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } catch (ServiceException e) {
            request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), AppConstants.MESSAGE.getName());
            request.setAttribute(AppConstants.MESSAGE.getName(), e.getMessage());
        }
    }
}
