package by.training.katokoleg.dal.transactions;

import java.sql.Connection;
import java.sql.SQLException;

public interface TransactionalManager {

    void prepareTransaction();

    void commitTransaction();

    void rollBackTransaction();

    Connection getConnection();
}
