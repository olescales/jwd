package by.training.katokoleg.entity;

public enum Gender {

    MALE,
    FEMALE
}
