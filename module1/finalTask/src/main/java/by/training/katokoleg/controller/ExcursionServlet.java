package by.training.katokoleg.controller;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.ApplicationContext;
import by.training.katokoleg.controller.command.Command;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "excursionServlet", urlPatterns = {"", "/login", "/excursions/*", "/personal_account/*", "/sign_up"})
public class ExcursionServlet extends HttpServlet {

    private static final long serialVersionUID = -3635461690267441746L;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String commandName = req.getParameter("command_name");
        Command command = ApplicationContext.getInstance().getBean(commandName);
        if (command == null) {
            req.getRequestDispatcher("/index.jsp").forward(req, resp);
        } else {
            command.execute(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}