package by.training.katokoleg.dal;

import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.entity.Tourist;
import by.training.katokoleg.exceptions.DAOException;

import java.util.List;

public interface TouristDAL extends CRUD<Long, Tourist> {

    boolean watchTouristByNameLastNameBirthDatePhoneAndExcursionDateTime(TouristDTO touristDTO);

    List<Tourist> findTouristsByExcursionGroupIDAndPersonID(long groupID, long personID) throws DAOException;

    boolean watchTouristByNameLastNameBirthDatePhoneAndGroupID(TouristDTO touristDTO);

    long getTouristID(Tourist tourist) throws DAOException;
}
