package by.training.katokoleg.dal;

import by.training.katokoleg.entity.ExcursionGroup;
import by.training.katokoleg.entity.ExecutionStage;
import by.training.katokoleg.exceptions.DAOException;

import java.util.List;
import java.util.Optional;


public interface ExcursionGroupDAL extends CRUD<Long, ExcursionGroup> {

    ExecutionStage getExecutionStageByID(long executionStageID) throws DAOException;

    List<ExcursionGroup> getExcursionSchedule(long excursionID) throws DAOException;

    List<ExcursionGroup> getExcursionsGroupByTouristID(long personID) throws DAOException;

    List<ExcursionGroup> getExcursionGroupsByExcursionIDAndExcursionDate(String excursionID, String excursionDate) throws DAOException;

    Optional<ExcursionGroup> getExcursionGroupByID(long excursionParamsID) throws DAOException;

    ExcursionGroup createNewExcursionGroup(ExcursionGroup excursionGroup) throws DAOException;

    void addTouristsToExcursionGroup(long groupID, int numberOfTourists) throws DAOException;

    void reduceTheNumberOfRegisteredTourists(int numberOfTourists, long groupID) throws DAOException;

    List<ExcursionGroup> getExcursionGroupsByCriteria(String criteriaQuery) throws DAOException;
}
