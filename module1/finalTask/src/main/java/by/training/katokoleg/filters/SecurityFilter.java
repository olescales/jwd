package by.training.katokoleg.filters;

import by.training.katokoleg.SecurityContext;
import by.training.katokoleg.controller.security.SecurityContextAnswer;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebFilter(filterName = "security_filter_order_4")
public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        SecurityContext securityContext = SecurityContext.getInstance();
        String sessionID = httpServletRequest.getSession().getId();
        securityContext.setCurrentUserSessionID(sessionID);
        String commandName = httpServletRequest.getParameter("command_name");
        Optional<String> commandType = Optional.ofNullable(commandName);

        if (!commandType.isPresent()) {
            chain.doFilter(request, response);
        } else {
            SecurityContextAnswer securityContextAnswer = securityContext.process(commandType.get());
            if (!securityContextAnswer.isValid()) {
                httpServletRequest.setAttribute("security_context_answer", securityContextAnswer.getMessage());
                httpServletRequest.getRequestDispatcher("/index.jsp").forward(request, response);
            } else {
                chain.doFilter(request, response);
            }
        }
    }

    @Override
    public void destroy() {

    }
}