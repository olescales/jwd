package by.training.katokoleg.dal;

import by.training.katokoleg.exceptions.DAOException;

import java.util.List;

public interface CRUD<T, E> {

    T save(E entity) throws DAOException;

    E get(T t) throws DAOException;

    void update(T t, E entity) throws DAOException;

    void delete(T t) throws DAOException;

    List<E> findAll() throws DAOException;
}