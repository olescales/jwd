package by.training.katokoleg.dal.transactions;


import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Interceptor {
    Class<? extends Annotation> clazz();
}
