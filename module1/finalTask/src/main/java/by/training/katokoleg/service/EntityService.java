package by.training.katokoleg.service;

import by.training.katokoleg.exceptions.EntityException;
import by.training.katokoleg.exceptions.ServiceException;

import java.util.List;

public interface EntityService<T> {

    T save(T element) throws ServiceException, EntityException;

    T get(Long id) throws ServiceException;

    void update(Long id, T element) throws ServiceException;

    void delete(Long id) throws ServiceException;

    List<T> findAll() throws ServiceException;

}
