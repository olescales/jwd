package by.training.katokoleg.dal.connectionpool;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {

    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);
    private static final Lock LOCK = new ReentrantLock();
    private static ConnectionPool INSTANCE;
    private final int minSufficientNumberOfConnections = 3;
    private final String jdbcDriver;
    private final String jdbcUrl;
    private final String jdbcLogin;
    private final String jdbcPassword;
    private BlockingQueue<Connection> connectionsStorage;
    private BlockingQueue<Connection> connectionsInUse;
    private int capacity;

    private ConnectionPool(int capacity,
                           final String jdbcDriver,
                           final String jdbcUrl,
                           final String jdbcLogin,
                           final String jdbcPassword) {
        this.capacity = capacity;
        connectionsStorage = new LinkedBlockingQueue<>(this.capacity);
        connectionsInUse = new LinkedBlockingQueue<>(this.capacity);
        this.jdbcDriver = jdbcDriver;
        this.jdbcUrl = jdbcUrl;
        this.jdbcLogin = jdbcLogin;
        this.jdbcPassword = jdbcPassword;
        fillConnectionPoolWithConnections();
    }

    public static ConnectionPool getInstance(final String JDBC_DRIVER,
                                             final String JDBC_URL,
                                             final String JDBC_LOGIN,
                                             final String JDBC_PASSWORD) {
        if (INSTANCE == null) {
            LOCK.lock();
            if (INSTANCE == null) {
                INSTANCE = new ConnectionPool(10, JDBC_DRIVER, JDBC_URL, JDBC_LOGIN, JDBC_PASSWORD);
            }
            LOCK.unlock();
        }
        return INSTANCE;
    }

    public static ConnectionPool getInstance(int capacity,
                                             final String JDBC_DRIVER,
                                             final String JDBC_URL,
                                             final String JDBC_LOGIN,
                                             final String JDBC_PASSWORD) {
        if (INSTANCE == null) {
            LOCK.lock();
            if (INSTANCE == null) {
                INSTANCE = new ConnectionPool(capacity, JDBC_DRIVER, JDBC_URL, JDBC_LOGIN, JDBC_PASSWORD);
            }
            LOCK.unlock();
        }
        return INSTANCE;
    }

    public Connection getConnection() {
        Connection connection;
        Connection proxyConnection = null;
        try {
            LOCK.lock();
            if (connectionsStorage.isEmpty() && connectionsInUse.size() < capacity) {
                connection = establishConnectionWithDB();
                connectionsInUse.offer(connection);
            } else {
                connection = connectionsStorage.take();
                connectionsInUse.offer(connection);
            }
            proxyConnection = createProxyConnection(connection);
            LOCK.unlock();
        } catch (InterruptedException | SQLException e) {
            LOGGER.log(Level.TRACE, e);
        }
        return proxyConnection;
    }

    private Connection createProxyConnection(Connection connection) {
        return (Connection) Proxy.newProxyInstance(
                Connection.class.getClassLoader(),
                new Class[]{Connection.class},
                ((proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        returnConnection(connection);
                        return null;
                    } else {
                        return method.invoke(connection, args);
                    }
                }));
    }

    private void returnConnection(Connection connectionForReturn) {
        try {
            connectionsInUse.remove(connectionForReturn);
            connectionsStorage.put(connectionForReturn);
        } catch (InterruptedException e) {
            LOGGER.log(Level.TRACE, e);
        }
    }

    private void fillConnectionPoolWithConnections() {
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.ERROR, e);
            throw new IllegalArgumentException("Can't find driver.");
        }
        for (int i = 0; i < Math.min(capacity, minSufficientNumberOfConnections); i++) {
            try {
                connectionsStorage.offer(establishConnectionWithDB());
            } catch (SQLException e) {
                LOGGER.log(Level.ERROR, e);
            }
        }
    }

    private Connection establishConnectionWithDB() throws SQLException {
        return DriverManager.getConnection(jdbcUrl, jdbcLogin, jdbcPassword);
    }


    public void destroy() {
        //????
        connectionsStorage.forEach(connection -> {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.log(Level.ERROR, e);
            }
        });
    }
}