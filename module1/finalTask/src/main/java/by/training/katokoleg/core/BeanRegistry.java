package by.training.katokoleg.core;

public interface BeanRegistry {

    <T> void addBean(T bean);

    public void addBean(Class<?> beanClass);

    <T> T getBean(Class<?> beanClass);

    <T> T getBean(String beanValue);

    <T> void removeBean(T bean);

    void destroy();

}
