package by.training.katokoleg.dal;

import by.training.katokoleg.entity.Excursion;

public interface ExcursionDAL extends CRUD<Long, Excursion> {
}
