package by.training.katokoleg.service;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dal.ExcursionDAL;
import by.training.katokoleg.dal.ExcursionGroupDAL;
import by.training.katokoleg.dto.ExcursionDTO;
import by.training.katokoleg.entity.Excursion;
import by.training.katokoleg.entity.ExcursionGroup;
import by.training.katokoleg.exceptions.DAOException;
import by.training.katokoleg.exceptions.EntityException;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.mapper.Mapper;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Bean
public class ExcursionServiceImpl implements ExcursionService {

    private static final Logger LOGGER = Logger.getLogger(ExcursionServiceImpl.class);
    private ExcursionDAL excursionDAL;
    private ExcursionGroupDAL excursionGroupDAL;
    private Mapper<ExcursionDTO, Excursion> excursionMapper;

    public ExcursionServiceImpl(ExcursionDAL excursionDAL, ExcursionGroupDAL excursionGroupDAL, Mapper<ExcursionDTO, Excursion> excursionMapper) {
        this.excursionDAL = excursionDAL;
        this.excursionGroupDAL = excursionGroupDAL;
        this.excursionMapper = excursionMapper;
    }

    @Override
    public ExcursionDTO save(ExcursionDTO element) throws ServiceException, EntityException {
        throw new UnsupportedOperationException();
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public ExcursionDTO get(Long id) throws ServiceException {
        try {
            Excursion excursion = excursionDAL.get(id);
            ExcursionDTO excursionForReturn = excursionMapper.fromEntityToDTO(excursion);
            setSchedule(excursionForReturn);
            return excursionForReturn;
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void update(Long id, ExcursionDTO element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long id) {
        throw new UnsupportedOperationException();
    }

    //CPU: O(n)
    //RAM: O(n)
    @Override
    public List<ExcursionDTO> findAll() throws ServiceException {
        try {
            List<ExcursionDTO> excursions = excursionDAL.findAll()
                    .stream()
                    .map(excursionMapper::fromEntityToDTO)
                    .collect(toList());
            for (ExcursionDTO excursion : excursions) {
                setSchedule(excursion);
            }
            return excursions;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    private void setSchedule(ExcursionDTO excursion) throws ServiceException {
        try {
            List<LocalDateTime> excursionSchedule = excursionGroupDAL.getExcursionSchedule(excursion.getId())
                    .stream()
                    .map(ExcursionGroup::getExcursionDateTime).collect(toList());
            excursion.setExcursionSchedule(excursionSchedule);

            List<String> formattedSchedule = excursionSchedule.stream().map(this::formatDate)
                    .collect(toList());
            excursion.setFormattedExcursionSchedule(formattedSchedule);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private String formatDate(LocalDateTime dateTime) {
        return String.format("Date and time: %s of %s at %s:00",
                dateTime.getDayOfMonth(),
                dateTime.getMonth(),
                dateTime.getHour());
    }
}