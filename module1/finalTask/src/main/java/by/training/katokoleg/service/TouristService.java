package by.training.katokoleg.service;

import by.training.katokoleg.dal.transactions.Transaction;
import by.training.katokoleg.dto.ValidationResult;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.exceptions.ServiceException;

import java.util.List;

public interface TouristService extends EntityService<TouristDTO> {

    @Transaction
    ValidationResult<TouristDTO> saveTourists(List<TouristDTO> touristData) throws ServiceException;

    List<TouristDTO> findTouristsByExcursionGroupIDAndPersonID(long id, long personID) throws ServiceException;

    @Transaction
    void deleteTouristsFromGroup(Long[] touristsID, long groupID) throws ServiceException;

    ValidationResult<TouristDTO> editTourists(List<TouristDTO> tourists);
}