package by.training.katokoleg.service;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.ExcursionGroupDTO;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.exceptions.ServiceException;

import java.util.List;

@Bean
public class PersonAccountServiceImpl implements PersonAccountService {

    private PersonService personService;
    private ExcursionService excursionService;
    private ExcursionGroupService excursionGroupService;
    private TouristService touristService;

    public PersonAccountServiceImpl(PersonService personService, ExcursionService excursionService, ExcursionGroupService excursionGroupService, TouristService touristService) {
        this.personService = personService;
        this.excursionService = excursionService;
        this.excursionGroupService = excursionGroupService;
        this.touristService = touristService;
    }

    //CPU: O(n)
    //RAM: O(n)
    public List<ExcursionGroupDTO> getExcursionsByPersonID(long personID) throws ServiceException {
        List<ExcursionGroupDTO> excursionGroups = excursionGroupService.getExcursionsByPersonID(personID);
        for (ExcursionGroupDTO eg : excursionGroups) {
            eg.setExcursion(excursionService.get(eg.getExcursionID()));

            PersonDTO guide = personService.get(eg.getGuideID());
            eg.setGuide(guide);

            eg.setRegisteredTourist(touristService.findTouristsByExcursionGroupIDAndPersonID(eg.getId(),personID));

            eg.setExecutionStage(excursionGroupService.getGroupExecutionStageByExecutionStageID(eg.getExecutionStageID()));
        }
        return excursionGroups;
    }
}
