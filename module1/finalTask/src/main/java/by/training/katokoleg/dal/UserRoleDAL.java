package by.training.katokoleg.dal;

import by.training.katokoleg.entity.UserRole;

import java.sql.Connection;
import java.util.List;

public interface UserRoleDAL {

    List<UserRole> findAllByPersonID(Long personID);

    void saveUserRole(Long personID, Long userRoleID);

    Long getUserRoleID(UserRole userRole);
}
