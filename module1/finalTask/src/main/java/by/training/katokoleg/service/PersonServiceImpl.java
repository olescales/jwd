package by.training.katokoleg.service;

import by.training.katokoleg.controller.security.PasswordAuthenticationWrapper;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dal.PersonDAL;
import by.training.katokoleg.dal.UserRoleDAL;
import by.training.katokoleg.dto.FieldValidationData;
import by.training.katokoleg.dto.Message;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.dto.ValidationResult;
import by.training.katokoleg.entity.Person;
import by.training.katokoleg.entity.UserRole;
import by.training.katokoleg.exceptions.DAOException;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.mapper.Mapper;
import by.training.katokoleg.dal.transactions.Transaction;
import by.training.katokoleg.dal.transactions.TransactionSupport;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Bean
@TransactionSupport
public class PersonServiceImpl implements PersonService {

    private PersonDAL personDAL;
    private Mapper<PersonDTO, Person> personMapper;
    private UserRoleDAL userRoleDAL;
    private PasswordAuthenticationWrapper passwordAuthenticationWrapper;

    public PersonServiceImpl(PersonDAL personDAL, Mapper<PersonDTO, Person> personMapper, UserRoleDAL userRoleDAL, PasswordAuthenticationWrapper passwordAuthenticationWrapper) {
        this.personDAL = personDAL;
        this.personMapper = personMapper;
        this.userRoleDAL = userRoleDAL;
        this.passwordAuthenticationWrapper = passwordAuthenticationWrapper;
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public PersonDTO save(PersonDTO personDTO) throws ServiceException {
        try {
            Person person = personMapper.fromDTOtoEntity(personDTO);
            Long personID = personDAL.save(person);
            person.getRoles()
                    .stream()
                    .mapToLong(role -> userRoleDAL.getUserRoleID(role))
                    .forEach(userRoleID -> userRoleDAL.saveUserRole(personID, userRoleID));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return personDTO;
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public PersonDTO get(Long personID) throws ServiceException {
        try {
            return personMapper.fromEntityToDTO(personDAL.get(personID));
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public void update(Long id, PersonDTO element) {
        throw new UnsupportedOperationException();
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public void delete(Long id) throws ServiceException {
        try {
            personDAL.delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<PersonDTO> findAll() {
        throw new UnsupportedOperationException();
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    @Transaction
    public ValidationResult<PersonDTO> signUp(PersonDTO personDTO) throws ServiceException {
        ValidationResult<PersonDTO> validationResult = checkPersonData(personDTO);
        if (validationResult.isValidated()) {
            hashingPassword(personDTO);
            save(personDTO);
        }
        return validationResult;
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public Message<PersonDTO> login(String login, String password) {
        Message<PersonDTO> message = new Message<>();
        Optional<Person> person = personDAL.findByLogin(login);
        if (!person.isPresent()) {
            message.setBadMessage("User with such login not exist!");
            return message;
        }
        if (!passwordAuthenticationWrapper.authenticate(password.toCharArray(), person.get().getPassword())) {
            message.setBadMessage("Wrong password!");
            return message;
        }
        List<UserRole> userRoles = userRoleDAL.findAllByPersonID(person.get().getId());
        person.get().setRoles(userRoles);
        message.setDto(personMapper.fromEntityToDTO(person.get()));
        return message;
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public long getPersonID(PersonDTO testPerson) throws ServiceException {
        try {
            return personDAL.getPersonID(personMapper.fromDTOtoEntity(testPerson));
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private ValidationResult<PersonDTO> checkPersonData(PersonDTO personDTO) {
        ValidationResult<PersonDTO> validationResult = new ValidationResult<>();
        if (checkLogin(personDTO.getLogin())) {
            validationResult.getWrongFields().put("login", new FieldValidationData(personDTO.getLogin(), "validation.user_already_exist"));
            validationResult.setValidated(false);
        }
        if (checkEmail(personDTO.getEmail())) {
            validationResult.getWrongFields().put("email", new FieldValidationData(personDTO.getEmail(), "validation.user_email_already_exist"));
            validationResult.setValidated(false);
        }
        if (checkDate(personDTO.getBirthDate())) {
            validationResult.getWrongFields().put("birthDate", new FieldValidationData(personDTO.getBirthDate(), "validation.too_young"));
            validationResult.setValidated(false);
        }
        return validationResult;
    }

    private boolean checkLogin(String login) {
        return personDAL.findByLogin(login).isPresent();
    }

    private boolean checkEmail(String email) {
        return personDAL.findByEmail(email).isPresent();
    }

    private boolean checkDate(String birthDate) {
        long l = LocalDate.now().toEpochDay();
        long l1 = LocalDate.parse(birthDate).toEpochDay();
        long l3 = l - l1;
        return l3 < 1280;
    }

    private void hashingPassword(PersonDTO personDTO) {
        char[] password = personDTO.getPassword().toCharArray();
        String token = passwordAuthenticationWrapper.hash(password);
        personDTO.setPassword(token);
    }
}