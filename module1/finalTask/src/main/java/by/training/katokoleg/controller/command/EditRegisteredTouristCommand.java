package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.controller.validators.EntityValidator;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.dto.ValidationResult;
import by.training.katokoleg.service.TouristService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Bean("edit_registered_tourists_command")
public class EditRegisteredTouristCommand implements Command {

    private TouristService touristService;
    private EntityValidator<TouristDTO> formatValidator;

    public EditRegisteredTouristCommand(TouristService touristService, EntityValidator<TouristDTO> formatValidator) {
        this.touristService = touristService;
        this.formatValidator = formatValidator;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String groupID = request.getParameter("group_id");
        String touristID = request.getParameter("tourist_id");
        String name = request.getParameter("tourist_name");
        String lastName = request.getParameter("last_name");
        String birthDate = request.getParameter("birth_date");
        String phone = request.getParameter("phone_number");
        TouristDTO tourist = new TouristDTO(name, lastName, birthDate, phone, groupID);
        tourist.setId(touristID);

        ValidationResult<TouristDTO> formatValidation = formatValidator.validate(tourist);
        if (formatValidation.isValidated()) {
            try {
                ValidationResult<TouristDTO> validationResult = touristService.editTourists(List.of(tourist));
                if (validationResult.isValidated()) {
                    response.sendRedirect("/excursionServlet/personal_account/excursions?command_name=" + CommandType.WATCH_PERSON_EXCURSIONS_COMMAND.getName());
                } else {
                    request.setAttribute("alreadyRegisteredTourists", validationResult.getBadData());
                    request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), "edit_tourist_data");
                    request.setAttribute(AppConstants.MESSAGE.getName(), "exception.message.already_registered");
                    request.getRequestDispatcher("/index.jsp").forward(request, response);
                }
            } catch (RuntimeException re) {
                request.setAttribute(AppConstants.MESSAGE.getName(), re.getMessage());
                request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), AppConstants.MESSAGE.getName());
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
        } else {
            request.setAttribute(AppConstants.WRONG_FIELDS.getName(), formatValidation.getWrongFields());
            request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), "edit_tourist_data");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}
