package by.training.katokoleg.entity;

public enum UserRole {
    CLIENT,
    ADMIN,
    GUIDE
}
