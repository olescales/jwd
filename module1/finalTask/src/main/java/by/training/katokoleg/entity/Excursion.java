package by.training.katokoleg.entity;

import java.math.BigDecimal;

public class Excursion {

    private Long id;
    private String topic;
    private String description;
    private int amountOfPeople;
    private BigDecimal cost;
    private String photoReference;
    private String location;

    public Excursion() {
    }

    public Excursion(Long id, String topic, String description, BigDecimal cost, String photoReference, String location) {
        this.id = id;
        this.topic = topic;
        this.description = description;
        this.cost = cost;
        this.photoReference = photoReference;
        this.location = location;
    }

    public Excursion(Long id, String topic, String description, BigDecimal cost, String photoReference) {
        this.id = id;
        this.topic = topic;
        this.description = description;
        this.cost = cost;
        this.photoReference = photoReference;
    }

    public Excursion(Long id, String topic, String description, int amountOfPeople, BigDecimal cost, String photoReference, String location) {
        this.id = id;
        this.topic = topic;
        this.description = description;
        this.amountOfPeople = amountOfPeople;
        this.cost = cost;
        this.photoReference = photoReference;
        this.location = location;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAmountOfPeople() {
        return amountOfPeople;
    }

    public void setAmountOfPeople(int amountOfPeople) {
        this.amountOfPeople = amountOfPeople;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Excursion)) return false;

        Excursion excursion = (Excursion) o;

        if (amountOfPeople != excursion.amountOfPeople) return false;
        if (id != null ? !id.equals(excursion.id) : excursion.id != null) return false;
        if (topic != null ? !topic.equals(excursion.topic) : excursion.topic != null) return false;
        if (description != null ? !description.equals(excursion.description) : excursion.description != null)
            return false;
        if (cost != null ? !cost.equals(excursion.cost) : excursion.cost != null) return false;
        if (photoReference != null ? !photoReference.equals(excursion.photoReference) : excursion.photoReference != null)
            return false;
        return location != null ? location.equals(excursion.location) : excursion.location == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + amountOfPeople;
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (photoReference != null ? photoReference.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Excursion{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", description='" + description + '\'' +
                ", amountOfPeople=" + amountOfPeople +
                ", cost=" + cost +
                ", photoReference='" + photoReference + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
