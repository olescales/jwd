package by.training.katokoleg.controller.command;

public enum CommandType {
    WATCH_EXCURSIONS_COMMAND("watch_excursions_command"),
    WATCH_EXCURSION_COMMAND("watch_excursion_command"),
    EXCURSION_REGISTRATION_COMMAND("excursion_registration_command"),
    EXCURSION_FORM_REGISTRATION_COMMAND("excursion_form_registration_command"),
    WATCH_EXCURSION_COMMENTS_COMMAND("watch_excursion_comments_command"),
    LOGIN_COMMAND("login_command"),
    LOGOUT_COMMAND("logout_command"),
    SIGN_UP_COMMAND("sign_up_command"),
    SIGN_UP_FORM_COMMAND("sign_up_form_command"),
    WATCH_PERSON_ACCOUNT_COMMAND("watch_person_account_command"),
    WATCH_PERSON_EXCURSIONS_COMMAND("watch_person_excursions_command"),
    WATCH_HOMEPAGE_COMMAND("watch_homepage_command"),
    WATCH_PERSON_COMMENTS_COMMAND("watch_person_comments_command"),
    DELETE_REGISTERED_TOURISTS_COMMAND("delete_registered_tourists_command"),
    EDIT_REGISTERED_TOURISTS_COMMAND("edit_registered_tourists_command"),
    WATCH_TOURIST_DATA_COMMAND("watch_tourist_data_command"),
    WATCH_PERSON_INFO_COMMAND("watch_person_info_command"),
    WATCH_PLANNED_EXCURSIONS_COMMAND("watch_planned_excursions_command");

    private final String name;

    CommandType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}