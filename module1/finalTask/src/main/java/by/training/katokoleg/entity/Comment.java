package by.training.katokoleg.entity;

import java.time.LocalDateTime;

public class Comment {

    private long id;
    private long excursionID;
    private long personID;
    private String textContent;
    private LocalDateTime commentDateTime;

    public Comment() {
    }

    public Comment(String textContent, long excursionID, LocalDateTime reviewDateTime) {
        this.textContent = textContent;
        this.excursionID = excursionID;
        this.commentDateTime = reviewDateTime;
    }

    public Comment(Long personID, String textContent, LocalDateTime reviewDateTime) {
        this.personID = personID;
        this.textContent = textContent;
        this.commentDateTime = reviewDateTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getExcursionID() {
        return excursionID;
    }

    public void setExcursionID(long excursionID) {
        this.excursionID = excursionID;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public LocalDateTime getCommentDateTime() {
        return commentDateTime;
    }

    public void setCommentDateTime(LocalDateTime commentDateTime) {
        this.commentDateTime = commentDateTime;
    }

    public long getPersonID() {
        return personID;
    }

    public void setPersonID(long personID) {
        this.personID = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;

        Comment comment = (Comment) o;

        if (id != comment.id) return false;
        if (excursionID != comment.excursionID) return false;
        if (personID != comment.personID) return false;
        if (textContent != null ? !textContent.equals(comment.textContent) : comment.textContent != null) return false;
        return commentDateTime != null ? commentDateTime.equals(comment.commentDateTime) : comment.commentDateTime == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (excursionID ^ (excursionID >>> 32));
        result = 31 * result + (int) (personID ^ (personID >>> 32));
        result = 31 * result + (textContent != null ? textContent.hashCode() : 0);
        result = 31 * result + (commentDateTime != null ? commentDateTime.hashCode() : 0);
        return result;
    }
}
