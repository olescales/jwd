package by.training.katokoleg.dal.connectionpool;

import java.sql.Connection;

public class MySQLDataSource implements DataSource {

    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String JDBC_URL = "jdbc:mysql://127.0.0.1:3306/excursions";
    private static final String JDBC_LOGIN = "root";
    private static final String JDBC_PASSWORD = "admin";
    private ConnectionPool connectionPool = ConnectionPool.getInstance(5, JDBC_DRIVER, JDBC_URL, JDBC_LOGIN, JDBC_PASSWORD);

    @Override
    public Connection getConnection() {
        return connectionPool.getConnection();
    }
}