package by.training.katokoleg.dto;

import java.time.LocalDateTime;

public class CommentDTO {

    private PersonDTO person;
    private ExcursionDTO excursionDTO;
    private String textContent;
    private LocalDateTime commentDateTime;

    public ExcursionDTO getExcursionDTO() {
        return excursionDTO;
    }

    public void setExcursionDTO(ExcursionDTO excursionDTO) {
        this.excursionDTO = excursionDTO;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public LocalDateTime getCommentDateTime() {
        return commentDateTime;
    }

    public void setCommentDateTime(LocalDateTime commentDateTime) {
        this.commentDateTime = commentDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommentDTO)) return false;

        CommentDTO that = (CommentDTO) o;

        if (person != null ? !person.equals(that.person) : that.person != null) return false;
        if (excursionDTO != null ? !excursionDTO.equals(that.excursionDTO) : that.excursionDTO != null) return false;
        if (textContent != null ? !textContent.equals(that.textContent) : that.textContent != null) return false;
        return commentDateTime != null ? commentDateTime.equals(that.commentDateTime) : that.commentDateTime == null;
    }

    @Override
    public int hashCode() {
        int result = person != null ? person.hashCode() : 0;
        result = 31 * result + (excursionDTO != null ? excursionDTO.hashCode() : 0);
        result = 31 * result + (textContent != null ? textContent.hashCode() : 0);
        result = 31 * result + (commentDateTime != null ? commentDateTime.hashCode() : 0);
        return result;
    }
}
