package by.training.katokoleg.mapper;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.entity.Tourist;

import java.time.LocalDate;

@Bean
public class TouristMapper implements Mapper<TouristDTO, Tourist> {

    @Override
    public Tourist fromDTOtoEntity(TouristDTO dto) {
        Tourist tourist = new Tourist();
        if (dto.getId() != null) {
            tourist.setId(Long.parseLong(dto.getId()));
        }
        if (dto.getName() != null) {
            tourist.setName(dto.getName());
        }
        if (dto.getLastName() != null) {
            tourist.setLastName(dto.getLastName());
        }
        if (dto.getBirthDate() != null) {
            tourist.setBirthDate(LocalDate.parse(dto.getBirthDate()));
        }
        if (dto.getPhone() != null) {
            tourist.setPhone(dto.getPhone());
        }
        if (dto.getTicketID() != null) {
            tourist.setTicketID(Long.parseLong(dto.getTicketID()));
        }
        if (dto.getPersonID() != null) {
            tourist.setPersonID(Long.parseLong(dto.getPersonID()));
        }
        if (dto.getExcursionGroup() != null) {
            tourist.setExcursionGroupID(dto.getExcursionGroup().getId());
        }
        return tourist;
    }

    @Override
    public TouristDTO fromEntityToDTO(Tourist entity) {
        TouristDTO touristDTO = new TouristDTO();
        if (entity.getId() != 0) {
            touristDTO.setId(String.valueOf(entity.getId()));
        }
        if (entity.getName() != null) {
            touristDTO.setName(entity.getName());
        }
        if (entity.getLastName() != null) {
            touristDTO.setLastName(entity.getLastName());
        }
        if (entity.getBirthDate() != null) {
            touristDTO.setBirthDate(entity.getBirthDate().toString());
        }
        if (entity.getPhone() != null) {
            touristDTO.setPhone(entity.getPhone());
        }
        if (entity.getTicketID() != 0) {
            touristDTO.setTicketID(String.valueOf(entity.getTicketID()));
        }
        if (entity.getPersonID() != 0) {
            touristDTO.setPersonID(String.valueOf(entity.getPersonID()));
        }
        return touristDTO;
    }
}
