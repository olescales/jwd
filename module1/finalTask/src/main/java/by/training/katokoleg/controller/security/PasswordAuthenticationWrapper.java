package by.training.katokoleg.controller.security;

import by.training.katokoleg.core.Bean;

@Bean
public class PasswordAuthenticationWrapper {

    private PasswordAuthentication passwordAuthentication;

    public PasswordAuthenticationWrapper() {
        passwordAuthentication = new PasswordAuthentication();
    }

    public boolean authenticate(char[] password, String token) {
        return passwordAuthentication.authenticate(password, token);
    }

    public String hash(char[] password) {
        return passwordAuthentication.hash(password);
    }
}
