package by.training.katokoleg.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "modal_message_output_filter_5")
public class ModalMessageOutputFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession();
        if (session.getAttribute("flag") != null) {
            Integer flagValue = (Integer) session.getAttribute("flag");
            if (flagValue < 2) {
                session.setAttribute("flag", ++flagValue);
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
