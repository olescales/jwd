package by.training.katokoleg.service;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dal.TouristDAL;
import by.training.katokoleg.dal.transactions.Transaction;
import by.training.katokoleg.dal.transactions.TransactionSupport;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.dto.ValidationResult;
import by.training.katokoleg.entity.ExecutionStage;
import by.training.katokoleg.entity.Tourist;
import by.training.katokoleg.exceptions.DAOException;
import by.training.katokoleg.exceptions.EntityException;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.mapper.Mapper;
import by.training.katokoleg.service.lambda_exception_handlers.LambdaExceptionsWrapper;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Bean
@TransactionSupport
public class TouristServiceImpl implements TouristService {

    private TouristDAL touristDAL;
    private Mapper<TouristDTO, Tourist> touristMapper;
    private ExcursionGroupService excursionGroupService;

    public TouristServiceImpl(TouristDAL touristDAL, Mapper<TouristDTO, Tourist> touristMapper, ExcursionGroupService excursionGroupService) {
        this.touristDAL = touristDAL;
        this.touristMapper = touristMapper;
        this.excursionGroupService = excursionGroupService;
    }

    //CPU: O(n)
    //RAM: O(1)
    @Override
    @Transaction
    public ValidationResult<TouristDTO> saveTourists(List<TouristDTO> tourists) throws ServiceException {
        ValidationResult<TouristDTO> validationResult = new ValidationResult<>();
        List<TouristDTO> alreadyRegisteredTourists = getAlreadyRegisteredTouristsDefTime(tourists);
        if (!alreadyRegisteredTourists.isEmpty()) {
            validationResult.setBadData(alreadyRegisteredTourists);
            return validationResult;
        }
        validationResult.setValidated(true);

        try {
            excursionGroupService.registerTouristsInExcursionGroup(tourists);
        } catch (RuntimeException re) {
            throw new ServiceException(re.getMessage());
        }

        try {
            for (TouristDTO touristDTO : tourists) {
                Tourist tourist = touristMapper.fromDTOtoEntity(touristDTO);
                touristDAL.save(tourist);
            }
            setAnswerMessage(tourists.get(0), validationResult);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return validationResult;
    }

    //CPU: O(n)
    //RAM: O(1)
    @Override
    public List<TouristDTO> findTouristsByExcursionGroupIDAndPersonID(long groupID, long personID) throws ServiceException {
        try {
            return touristDAL.findTouristsByExcursionGroupIDAndPersonID(groupID, personID)
                    .stream()
                    .map(touristMapper::fromEntityToDTO)
                    .collect(toList());
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    //CPU: O(n)
    //RAM: O(1)
    @Override
    @Transaction
    public void deleteTouristsFromGroup(Long[] touristsID, long groupID) throws ServiceException {
        try {
            for (Long touristID : touristsID) {
                touristDAL.delete(touristID);
            }
            excursionGroupService.removeTouristsFromGroup(touristsID.length, groupID);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public TouristDTO save(TouristDTO element) throws ServiceException, EntityException {
        throw new UnsupportedOperationException();
    }

    @Override
    public TouristDTO get(Long touristID) throws ServiceException {
        try {
            return touristMapper.fromEntityToDTO(touristDAL.get(touristID));
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public void update(Long touristID, TouristDTO tourist) throws ServiceException {
        Tourist touristEntity = touristMapper.fromDTOtoEntity(tourist);
        try {
            touristDAL.update(touristID, touristEntity);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    //CPU: O(n)
    //RAM: O(1)
    @Override
    public ValidationResult<TouristDTO> editTourists(List<TouristDTO> tourists) {
        ValidationResult<TouristDTO> validationResult = new ValidationResult<>();
        List<TouristDTO> alreadyRegistered = getAlreadyRegisteredTouristsDefGroup(tourists);
        if (!alreadyRegistered.isEmpty()) {
            validationResult.setBadData(alreadyRegistered);
            return validationResult;
        }
        validationResult.setValidated(true);
        tourists.forEach(LambdaExceptionsWrapper.throwingConsumerWrapper(tourist -> this.update(Long.parseLong(tourist.getId()), tourist)));
        return validationResult;
    }

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            touristDAL.delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<TouristDTO> findAll() throws ServiceException {
        throw new UnsupportedOperationException();
    }

    private List<TouristDTO> getAlreadyRegisteredTouristsDefTime(List<TouristDTO> touristData) {
        return touristData
                .stream()
                .filter(touristDTO -> touristDAL.watchTouristByNameLastNameBirthDatePhoneAndExcursionDateTime(touristDTO))
                .collect(toList());
    }

    private List<TouristDTO> getAlreadyRegisteredTouristsDefGroup(List<TouristDTO> touristData) {
        return touristData
                .stream()
                .filter(touristDTO -> touristDAL.watchTouristByNameLastNameBirthDatePhoneAndGroupID(touristDTO))
                .collect(toList());
    }

    private void setAnswerMessage(TouristDTO element, ValidationResult<TouristDTO> validationResult) {
        if (element.getExcursionGroup().getExecutionStageID() == ExecutionStage.ASSEMBLE.getId()) {
            validationResult.setAnswer("text.excursion_registration_is_ok_stage_assemble");
        } else {
            validationResult.setAnswer("text.excursion_registration_is_ok_stage_will_take_place");
        }
    }
}
