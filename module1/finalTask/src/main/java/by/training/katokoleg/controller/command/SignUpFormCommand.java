package by.training.katokoleg.controller.command;

import by.training.katokoleg.core.Bean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Bean("sign_up_form_command")
public class SignUpFormCommand implements Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/sign_up_form.jsp").forward(request, response);
    }
}