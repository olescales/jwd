package by.training.katokoleg.mapper;

public interface Mapper<T, E> {

    E fromDTOtoEntity(T dto);

    T fromEntityToDTO(E entity);
}
