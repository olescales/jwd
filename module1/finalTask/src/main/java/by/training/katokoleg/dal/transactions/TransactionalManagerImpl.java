package by.training.katokoleg.dal.transactions;

import by.training.katokoleg.dal.connectionpool.DataSource;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

public class TransactionalManagerImpl implements TransactionalManager {

    private static final Logger LOGGER = Logger.getLogger(TransactionalManagerImpl.class);
    private DataSource dataSource;
    private ThreadLocal<Connection> localConnection = new ThreadLocal<>();

    public TransactionalManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void prepareTransaction() {
        try {
            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            localConnection.set(connection);
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new RuntimeException("Error while preparing transaction");
        }
    }

    @Override
    public void commitTransaction() {
        Connection connection = localConnection.get();
        try {
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new RuntimeException("Failed to commit transaction");
        }
        localConnection.remove();
    }

    @Override
    public void rollBackTransaction() {
        Connection connection = localConnection.get();
        try {
            connection.rollback();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new RuntimeException("Failed to rollback transaction");
        }
        localConnection.remove();
    }

    @Override
    public Connection getConnection() {
        if (localConnection.get() != null) {
            return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Connection.class},
                    (proxy, method, args) -> {
                        if (method.getName().equals("close")) {
                            return null;
                        } else {
                            Connection realConnection = localConnection.get();
                            return method.invoke(realConnection, args);
                        }
                    });
        } else {
            return dataSource.getConnection();
        }
    }
}