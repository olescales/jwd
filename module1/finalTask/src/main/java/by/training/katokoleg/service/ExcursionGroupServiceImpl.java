package by.training.katokoleg.service;

import by.training.katokoleg.controller.command.criteria.Criteria;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dal.ExcursionGroupDAL;
import by.training.katokoleg.dto.ExcursionGroupDTO;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.entity.ExcursionGroup;
import by.training.katokoleg.entity.ExecutionStage;
import by.training.katokoleg.exceptions.DAOException;
import by.training.katokoleg.exceptions.EntityException;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.mapper.Mapper;
import by.training.katokoleg.service.lambda_exception_handlers.LambdaExceptionsWrapper;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Bean
public class ExcursionGroupServiceImpl implements ExcursionGroupService {

    private static final Logger LOGGER = Logger.getLogger(ExcursionGroupServiceImpl.class);
    private ExcursionGroupDAL excursionGroupDAL;
    private Mapper<ExcursionGroupDTO, ExcursionGroup> excursionGroupMapper;

    public ExcursionGroupServiceImpl(ExcursionGroupDAL excursionGroupDAL, Mapper<ExcursionGroupDTO, ExcursionGroup> excursionGroupMapper) {
        this.excursionGroupDAL = excursionGroupDAL;
        this.excursionGroupMapper = excursionGroupMapper;

    }

    @Override
    public ExcursionGroupDTO save(ExcursionGroupDTO element) throws ServiceException, EntityException {
        throw new UnsupportedOperationException();
    }

    @Override
    public ExcursionGroupDTO get(Long id) throws ServiceException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Long id, ExcursionGroupDTO element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<ExcursionGroupDTO> findAll() throws ServiceException {
        throw new UnsupportedOperationException();
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public List<ExcursionGroupDTO> getExcursionsByPersonID(long personID) throws ServiceException {
        try {
            return excursionGroupDAL.getExcursionsGroupByTouristID(personID)
                    .stream()
                    .map(excursionGroupMapper::fromEntityToDTO)
                    .collect(Collectors.toList());
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public List<ExcursionGroup> getExcursionGroupsByExcursionIDAndExcursionDateTime(String excursionID, String excursionDate) throws ServiceException {
        try {
            return excursionGroupDAL.getExcursionGroupsByExcursionIDAndExcursionDate(excursionID, excursionDate);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public ExcursionGroup getExcursionGroupByID(long excursionParamsID) throws ServiceException {
        try {
            Optional<ExcursionGroup> excursionGroup = excursionGroupDAL.getExcursionGroupByID(excursionParamsID);
            if (excursionGroup.isPresent()) {
                return excursionGroup.get();
            } else {
                throw new ServiceException("no such excursion group excursion");
            }
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public ExcursionGroup createNewExcursionGroup(ExcursionGroup excursionGroup) throws ServiceException {
        try {
            return excursionGroupDAL.createNewExcursionGroup(excursionGroup);
        } catch (DAOException e) {
            LOGGER.log(Level.ERROR, e);
            throw new ServiceException(e.getMessage(), e);
        }
    }

    //CPU: O(n)
    //RAM: O(1)
    @Override
    public void registerTouristsInExcursionGroup(List<TouristDTO> tourists) throws ServiceException {
        //add lock when set group to tourist and increment registered_tourist field
        List<ExcursionGroup> planningExcursionGroupsOnCurrentDateTime = getExcursionGroupsByExcursionIDAndExcursionDateTime(tourists.get(0).getExcursionID(), tourists.get(0).getExcursionDateTime());
        ExcursionGroup planningGroup = planningExcursionGroupsOnCurrentDateTime
                .stream()
                .min(Comparator.comparingInt(ExcursionGroup::getRegisteredTourist)).get();

        if (planningGroup.getRegisteredTourist() + tourists.size() <= planningGroup.getGroupSize()) {
            tourists.forEach(LambdaExceptionsWrapper.throwingConsumerWrapper(tourist -> {
                addTouristToExcursionGroup(planningGroup.getId(), tourists.size());
                ExcursionGroupDTO excursionGroup = excursionGroupMapper.fromEntityToDTO(planningGroup);
                tourist.setExcursionGroup(excursionGroup);
            }));
        } else {
            ExcursionGroup newExcursionGroup = createNewExcursionGroup(planningGroup);
            tourists.forEach(LambdaExceptionsWrapper.throwingConsumerWrapper(tourist -> {
                ExcursionGroupDTO excursionGroup = excursionGroupMapper.fromEntityToDTO(newExcursionGroup);
                tourist.setExcursionGroup(excursionGroup);
                addTouristToExcursionGroup(newExcursionGroup.getId(), tourists.size());
            }));
        }
    }

    //CPU: O(1)
    //RAM: O(1)
    @Override
    public List<ExcursionGroup> getExcursionSchedule(long excursionID) throws ServiceException {
        try {
            return excursionGroupDAL.getExcursionSchedule(excursionID);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public ExecutionStage getGroupExecutionStageByExecutionStageID(long executionStageID) throws ServiceException {
        try {
            return excursionGroupDAL.getExecutionStageByID(executionStageID);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public void removeTouristsFromGroup(int numberOfTourists, long groupID) throws ServiceException {
        try {
            excursionGroupDAL.reduceTheNumberOfRegisteredTourists(numberOfTourists, groupID);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public <E> List<ExcursionGroupDTO> findExcursionGroups(Criteria<E> criteria) throws ServiceException {
        String criteriaQuery = getQueryStringFromCriteria(criteria);
        try {
            return excursionGroupDAL.getExcursionGroupsByCriteria(criteriaQuery)
                    .stream()
                    .map(excursionGroupMapper::fromEntityToDTO)
                    .collect(Collectors.toList());
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private <E> String getQueryStringFromCriteria(Criteria<E> criteria) {
        StringBuilder criteriaQuery = new StringBuilder("SELECT * FROM excursions.")
                .append(criteria.getSearchObject().getSimpleName())
                .append(" WHERE");

        criteria.getCriteria().forEach((key, value) -> {
            String[] valueArray = value.toString().split(",");
            if (valueArray.length > 1) {
                for (String param : valueArray) {
                    criteriaQuery.append(" ")
                            .append(key.toString().toLowerCase())
                            .append(" = ")
                            .append(param)
                            .append(",");
                }
            } else {
                criteriaQuery.append(" ")
                        .append(key.toString().toLowerCase())
                        .append(" = ")
                        .append(value)
                        .append(",");
            }
        });
        criteriaQuery.deleteCharAt(criteriaQuery.length() - 1); // remove "," at the end of the criteriaQuery
        return criteriaQuery.toString();
    }

    private void addTouristToExcursionGroup(long groupID, int numberOfTourists) throws ServiceException {
        try {
            excursionGroupDAL.addTouristsToExcursionGroup(groupID, numberOfTourists);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }


}
