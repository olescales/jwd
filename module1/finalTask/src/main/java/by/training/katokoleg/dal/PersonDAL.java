package by.training.katokoleg.dal;

import by.training.katokoleg.entity.Person;
import by.training.katokoleg.exceptions.DAOException;

import java.util.Optional;

public interface PersonDAL extends CRUD<Long, Person> {

    Optional<Person> findByLogin (String login);

    Optional<Person> findByEmail (String email);

    long getPersonID(Person person) throws DAOException;
}
