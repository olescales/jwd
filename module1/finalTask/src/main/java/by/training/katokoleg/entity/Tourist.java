package by.training.katokoleg.entity;

import java.time.LocalDate;

public class Tourist {

    private long id;
    private String name;
    private String lastName;
    private LocalDate birthDate;
    private long ticketID;
    private long excursionGroupID;
    private String phone;
    private long personID;

    public Tourist() {
    }

    public Tourist(long id, String name, String lastName, LocalDate birthDate, long ticketID, long excursionGroupID, String phone, long personID) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.ticketID = ticketID;
        this.excursionGroupID = excursionGroupID;
        this.phone = phone;
        this.personID = personID;
    }

    public Tourist(String name, String lastName, LocalDate birthDate, long excursionGroupID, String phone, long personID) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.excursionGroupID = excursionGroupID;
        this.phone = phone;
        this.personID = personID;
    }

    public Tourist(String name, String lastName, LocalDate date) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = date;
    }

    public Tourist(long id, String name, String lastName, LocalDate date, String phone) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.birthDate = date;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public long getTicketID() {
        return ticketID;
    }

    public void setTicketID(long ticketID) {
        this.ticketID = ticketID;
    }

    public long getExcursionGroupID() {
        return excursionGroupID;
    }

    public void setExcursionGroupID(long excursionGroupID) {
        this.excursionGroupID = excursionGroupID;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getPersonID() {
        return personID;
    }

    public void setPersonID(long personID) {
        this.personID = personID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tourist)) return false;

        Tourist tourist = (Tourist) o;

        if (id != tourist.id) return false;
        if (ticketID != tourist.ticketID) return false;
        if (excursionGroupID != tourist.excursionGroupID) return false;
        if (personID != tourist.personID) return false;
        if (name != null ? !name.equals(tourist.name) : tourist.name != null) return false;
        if (lastName != null ? !lastName.equals(tourist.lastName) : tourist.lastName != null) return false;
        if (birthDate != null ? !birthDate.equals(tourist.birthDate) : tourist.birthDate != null) return false;
        return phone != null ? phone.equals(tourist.phone) : tourist.phone == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (int) (ticketID ^ (ticketID >>> 32));
        result = 31 * result + (int) (excursionGroupID ^ (excursionGroupID >>> 32));
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (int) (personID ^ (personID >>> 32));
        return result;
    }
}
