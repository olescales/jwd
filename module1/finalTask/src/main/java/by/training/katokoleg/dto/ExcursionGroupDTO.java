package by.training.katokoleg.dto;

import by.training.katokoleg.entity.ExecutionStage;

import java.time.LocalDateTime;
import java.util.List;

public class ExcursionGroupDTO {

    private long id;
    private ExcursionDTO excursion;
    private long excursionID;
    private LocalDateTime excursionDateTime;
    private int executionStageID;
    private ExecutionStage executionStage;
    private PersonDTO guide;
    private long guideID;
    private List<TouristDTO> registeredTourist;
    private int amountRegisteredTourists;
    private int groupSize;

    public ExecutionStage getExecutionStage() {
        return executionStage;
    }

    public void setExecutionStage(ExecutionStage executionStage) {
        this.executionStage = executionStage;
    }

    public int getAmountRegisteredTourists() {
        return amountRegisteredTourists;
    }

    public void setAmountRegisteredTourists(int amountRegisteredTourists) {
        this.amountRegisteredTourists = amountRegisteredTourists;
    }

    public long getExcursionID() {
        return excursionID;
    }

    public void setExcursionID(long excursionID) {
        this.excursionID = excursionID;
    }

    public long getGuideID() {
        return guideID;
    }

    public void setGuideID(int guideID) {
        this.guideID = guideID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ExcursionDTO getExcursion() {
        return excursion;
    }

    public void setExcursion(ExcursionDTO excursion) {
        this.excursion = excursion;
    }

    public LocalDateTime getExcursionDateTime() {
        return excursionDateTime;
    }

    public void setExcursionDateTime(LocalDateTime excursionDateTime) {
        this.excursionDateTime = excursionDateTime;
    }

    public long getExecutionStageID() {
        return executionStageID;
    }

    public void setExecutionStageID(int executionStageID) {
        this.executionStageID = executionStageID;
    }

    public PersonDTO getGuide() {
        return guide;
    }

    public void setGuide(PersonDTO guide) {
        this.guide = guide;
    }

    public List<TouristDTO> getRegisteredTourist() {
        return registeredTourist;
    }

    public void setRegisteredTourist(List<TouristDTO> registeredTourist) {
        this.registeredTourist = registeredTourist;
    }

    public int getGroupSize() {
        return groupSize;
    }

    public void setGroupSize(int groupSize) {
        this.groupSize = groupSize;
    }

    public void setGuideID(long guideID) {
        this.guideID = guideID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExcursionGroupDTO)) return false;

        ExcursionGroupDTO that = (ExcursionGroupDTO) o;

        if (id != that.id) return false;
        if (excursionID != that.excursionID) return false;
        if (executionStageID != that.executionStageID) return false;
        if (guideID != that.guideID) return false;
        if (amountRegisteredTourists != that.amountRegisteredTourists) return false;
        if (groupSize != that.groupSize) return false;
        if (excursion != null ? !excursion.equals(that.excursion) : that.excursion != null) return false;
        if (excursionDateTime != null ? !excursionDateTime.equals(that.excursionDateTime) : that.excursionDateTime != null)
            return false;
        if (executionStage != that.executionStage) return false;
        if (guide != null ? !guide.equals(that.guide) : that.guide != null) return false;
        return registeredTourist != null ? registeredTourist.equals(that.registeredTourist) : that.registeredTourist == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (excursion != null ? excursion.hashCode() : 0);
        result = 31 * result + (int) (excursionID ^ (excursionID >>> 32));
        result = 31 * result + (excursionDateTime != null ? excursionDateTime.hashCode() : 0);
        result = 31 * result + executionStageID;
        result = 31 * result + (executionStage != null ? executionStage.hashCode() : 0);
        result = 31 * result + (guide != null ? guide.hashCode() : 0);
        result = 31 * result + (int) (guideID ^ (guideID >>> 32));
        result = 31 * result + (registeredTourist != null ? registeredTourist.hashCode() : 0);
        result = 31 * result + amountRegisteredTourists;
        result = 31 * result + groupSize;
        return result;
    }
}
