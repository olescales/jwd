package by.training.katokoleg.controller.command.criteria;

import java.util.HashMap;
import java.util.Map;

public class Criteria<E> {

    private Class<E> searchObject;
    private Map<E, Object> criteria;

    public Criteria(Class<E> searchObject) {
        this.searchObject = searchObject;
        this.criteria = new HashMap<>();
    }

    public Class<E> getSearchObject() {
        return searchObject;
    }

    public void setSearchObject(Class<E> searchObject) {
        this.searchObject = searchObject;
    }

    public Map<E, Object> getCriteria() {
        return criteria;
    }

    public void setCriteria(Map<E, Object> criteria) {
        this.criteria = criteria;
    }

    public void add (E searchCriteria, Object value) {
        criteria.put(searchCriteria, value);
    }
}
