package by.training.katokoleg.dto;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ValidationResult<T> {

    private boolean validated;
    private Map<String, FieldValidationData> wrongFields;
    private Collection<T> badData;
    private String answer;

    public ValidationResult() {
        validated = true;
        this.wrongFields = new HashMap<>();
    }

    public Collection<T> getBadData() {
        return badData;
    }

    public void setBadData(Collection<T> badData) {
        this.badData = badData;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public Map<String, FieldValidationData> getWrongFields() {
        return wrongFields;
    }

    public void setWrongFields(Map<String, FieldValidationData> wrongFields) {
        this.wrongFields = wrongFields;
    }
}
