package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Bean("watch_person_account_command")
public class WatchPersonAccountCommand implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isAdmin = Boolean.parseBoolean(request.getParameter("is_admin"));
        if (isAdmin) {
            request.getSession().setAttribute(AppConstants.INDEX_INCLUDE_VIEW.getName(), "admin_account");
        } else {
            request.getSession().setAttribute(AppConstants.INDEX_INCLUDE_VIEW.getName(), "person_account");
        }
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
