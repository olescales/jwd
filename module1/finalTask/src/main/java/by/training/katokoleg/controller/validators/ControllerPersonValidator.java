package by.training.katokoleg.controller.validators;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.FieldValidationData;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.dto.ValidationResult;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Bean
public class ControllerPersonValidator implements EntityValidator<PersonDTO> {

    @Override
    public ValidationResult validate(PersonDTO person) {
        ValidationResult validationResult = new ValidationResult();

        String personName = person.getName();
        if (personName.isEmpty()) {
            validationResult.getWrongFields().put("personName", new FieldValidationData(person.getName(),"<fmt:message key=\"validation.js.cant_be_blank\"/>"));
        } else if (!checkName(personName)) {
            validationResult.getWrongFields().put("personName", new FieldValidationData(person.getName(),"<fmt:message key=\"validation.js.invalid_name\"/>"));
        }

        String lastName = person.getLastName();
        if (lastName.isEmpty()) {

        } else if (!checkName(lastName)) {
            validationResult.getWrongFields().put("lastName", new FieldValidationData(person.getLastName(),"<fmt:message key=\"validation.js.invalid_name\"/>"));
        }

        String birthDate = person.getBirthDate();
        if (!checkBirthDateFormat(birthDate) || !checkBirthDateContent(birthDate)) {
            validationResult.getWrongFields().put("birthDate", new FieldValidationData(person.getBirthDate(),"<fmt:message key=\"validation.js.invalid_date\"/>"));
        }

        String email = person.getEmail();
        if (email.isEmpty()) {
            validationResult.getWrongFields().put("email", new FieldValidationData(person.getEmail(),"<fmt:message key=\"validation.js.cant_be_blank\"/>"));
        } else if (!checkEmail(email)) {
            validationResult.getWrongFields().put("email", new FieldValidationData(person.getEmail(),"<fmt:message key=\"validation.js.invalid_email\"/>"));
        }

        String login = person.getLogin();
        if (login.isEmpty()) {
            validationResult.getWrongFields().put("login", new FieldValidationData(person.getLogin(),"<fmt:message key=\"validation.js.cant_be_blank\"/>"));
        } else if (login.length() < 3) {
            validationResult.getWrongFields().put("login", new FieldValidationData(person.getLogin(),"<fmt:message key=\"validation.js.min_login_length\"/>"));
        } else if (login.length() > 30) {
            validationResult.getWrongFields().put("login", new FieldValidationData(person.getLogin(),"<fmt:message key=\"validation.js.max_login_length\"/>"));
        } else if (!checkLogin(login)) {
            validationResult.getWrongFields().put("login", new FieldValidationData(person.getLogin(),"<fmt:message key=\"validation.js.invalid_login\"/>"));
        }

        String password = person.getPassword();
        if (password.isEmpty()) {
            validationResult.getWrongFields().put("password", new FieldValidationData(person.getPassword(),"<fmt:message key=\"validation.js.cant_be_blank\"/>"));
        } else if (password.length() < 8) {
            validationResult.getWrongFields().put("password", new FieldValidationData(person.getPassword(),"<fmt:message key=\"validation.js.min_password_length\"/>"));
        } else if (password.length() > 20) {
            validationResult.getWrongFields().put("password", new FieldValidationData(person.getPassword(),"<fmt:message key=\"validation.js.max_password_length\"/>"));
        } else if (!checkPassword(password)) {
            validationResult.getWrongFields().put("password", new FieldValidationData(person.getPassword(),"<fmt:message key=\"validation.js.invalid_password\"/>"));
        }

        String passwordConfirmation = person.getPasswordConfirmation();
        if (passwordConfirmation.isEmpty()) {
            validationResult.getWrongFields().put("passwordConfirmation", new FieldValidationData(person.getPasswordConfirmation(),"<fmt:message key=\"validation.js.cant_be_blank\"/>"));
        } else if (!passwordConfirmation.equals(password)) {
            validationResult.getWrongFields().put("passwordConfirmation", new FieldValidationData(person.getPasswordConfirmation(),"<fmt:message key=\"validation.js.passwords_doesnt_match\"/>"));
        }

        String phone = person.getPhoneNumber();
        if (phone.isEmpty()) {

        } else if (!checkPhone(phone)) {
            validationResult.getWrongFields().put("phone", new FieldValidationData(person.getPhoneNumber(),"<fmt:message key=\"validation.js.invalid_phone\"/>"));
        }

        if (!validationResult.getWrongFields().isEmpty()) {
            validationResult.setValidated(false);
        }
        return validationResult;
    }

    private boolean checkName(String name) {
        return name.matches("^[A-Z][a-z]{1,29}$|^[А-Я][а-я]{1,29}$");
    }

    private boolean checkLogin(String login) {
        return login.matches("^(?=.*[A-z0-9]$)[A-z][A-z\\d.-]{2,19}$");
    }

    private boolean checkPassword(String password) {
        return password.matches("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})");
    }

    private boolean checkPhone(String phone) {
        return phone.matches("^(25|29|33|44)-[\\d]{3}-[\\d]{2}-[\\d]{2}$");
    }

    private boolean checkEmail(String email) {
        return email.matches("^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
    }

    private boolean checkBirthDateFormat(String birthDate) {
        return birthDate.matches("^[\\d]{4}-[\\d]{2}-[\\d]{2}");
    }

    private boolean checkBirthDateContent(String birthDate) {
        List<Integer> date = Arrays.stream(birthDate.split("-")).map(Integer::parseInt).collect(Collectors.toList());
        int year = date.get(0);
        int month = date.get(1);
        int days = date.get(2);
        return DateChecker.checkDate(days, month, year);
    }

}
