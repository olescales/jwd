package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.ExcursionDTO;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.ExcursionService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Bean("watch_excursions_command")
public class WatchExcursionsCommand implements Command {

    private ExcursionService excursionService;

    public WatchExcursionsCommand(ExcursionService excursionService) {
        this.excursionService = excursionService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<ExcursionDTO> excursions = excursionService.findAll();
            request.getServletContext().setAttribute("excursions", excursions);
            request.getSession().setAttribute(AppConstants.INDEX_INCLUDE_VIEW.getName(), "excursions");
        } catch (ServiceException e) {
            request.setAttribute(AppConstants.MESSAGE.getName(), e.getMessage());
            request.getSession().setAttribute(AppConstants.INDEX_INCLUDE_VIEW.getName(), "message");
        }
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}