package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.CommentDTO;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.CommentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Bean("watch_excursion_comments_command")
public class WatchExcursionCommentsCommand implements Command {

    private CommentService commentService;

    public WatchExcursionCommentsCommand(CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long excursionID = Long.parseLong(request.getParameter("excursion_id"));
        try {
            List<CommentDTO> comments = commentService.findAllByExcursionID(excursionID);
            request.setAttribute("comments", comments);
            request.setAttribute(AppConstants.EXCURSION_INCLUDE_VIEW.getName(), "comments");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } catch (ServiceException e) {
            request.setAttribute(AppConstants.MESSAGE.getName(), e.getMessage());
            request.setAttribute(AppConstants.EXCURSION_INCLUDE_VIEW.getName(), AppConstants.MESSAGE.getName());
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

    }
}
