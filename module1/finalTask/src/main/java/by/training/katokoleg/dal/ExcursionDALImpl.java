package by.training.katokoleg.dal;

import by.training.katokoleg.dal.transactions.ConnectionManager;
import by.training.katokoleg.entity.Excursion;
import by.training.katokoleg.exceptions.DAOException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ExcursionDALImpl implements ExcursionDAL {

    private static final Logger LOGGER = Logger.getLogger(ExcursionDALImpl.class);
    private ConnectionManager connectionManager;

    private static final String SAVE_QUERY = "INSERT INTO excursions.excursion(topic, description, cost) VALUES (?,?,?)";
    private static final String FIND_ALL_EXCURSIONS_QUERY = "SELECT * FROM excursions.excursion ";
    private static final String GET_EXCURSION_BY_ID_QUERY = "SELECT * FROM excursions.excursion WHERE id = ?";

    public ExcursionDALImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(Excursion entity) throws DAOException {
        long excursionIDForReturn = 0L;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SAVE_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, entity.getTopic());
            preparedStatement.setString(2, entity.getDescription());
            preparedStatement.setBigDecimal(3, entity.getCost());
            preparedStatement.executeQuery();
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Save person failed.");
            }
            ResultSet generatedKey = preparedStatement.getGeneratedKeys();
            if (generatedKey.next()) {
                excursionIDForReturn = generatedKey.getLong(1);
            }
            return excursionIDForReturn;
        } catch (SQLException e) {
            LOGGER.log(Level.TRACE, e.getMessage());
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public Excursion get(Long id) throws DAOException {
        Excursion excursionForReturn = null;
        try {
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(GET_EXCURSION_BY_ID_QUERY)) {
                preparedStatement.setLong(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        Long excursionID = resultSet.getLong("id");
                        String topic = resultSet.getNString("topic");
                        String description = resultSet.getNString("description");
                        BigDecimal cost = resultSet.getBigDecimal("cost");
                        String photoReference = resultSet.getNString("photo_reference");
                        String location = resultSet.getNString("location");
                        excursionForReturn = new Excursion(excursionID, topic, description, cost, photoReference, location);
                    }
                    return excursionForReturn;
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public void update(Long id, Excursion entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long id) throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Excursion> findAll() throws DAOException {
        List<Excursion> excursions = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_EXCURSIONS_QUERY);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                String topic = resultSet.getString("topic");
                String description = resultSet.getString("description");
                BigDecimal cost = resultSet.getBigDecimal("cost");
                Long excursionID = resultSet.getLong("id");
                String photoReference = resultSet.getString("photo_reference");
                excursions.add(new Excursion(excursionID, topic, description, cost, photoReference));
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
            throw new DAOException(e.getMessage(), e);
        }
        return excursions;
    }




}