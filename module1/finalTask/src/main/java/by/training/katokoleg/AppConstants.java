package by.training.katokoleg;

public enum AppConstants {

    MESSAGE("message"),
    INDEX_INCLUDE_VIEW("index_include_view"),
    WRONG_FIELDS("wrong_fields"),
    PERSON_ACCOUNT_INCLUDE_VIEW("person_account_include_view"),
    EXCURSION_INCLUDE_VIEW("excursion_include_view"),
    REQUEST("request"),
    SESSION("session"),
    CLIENT("client");

    private final String name;

    AppConstants(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
