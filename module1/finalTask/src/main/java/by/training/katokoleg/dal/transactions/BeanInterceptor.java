package by.training.katokoleg.dal.transactions;

import java.lang.reflect.Method;

public interface BeanInterceptor {

    void before(Method method);

    void success(Method method);

    void fail(Method method);
}
