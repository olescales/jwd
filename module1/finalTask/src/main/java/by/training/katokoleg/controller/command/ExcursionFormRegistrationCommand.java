package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Bean("excursion_form_registration_command")
public class ExcursionFormRegistrationCommand implements Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/view/excursion_registration_form.jsp").forward(request, response);
    }
}