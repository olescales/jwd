package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.ExcursionGroupDTO;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.PersonAccountService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Bean("watch_person_excursions_command")
public class WatchPersonExcursionsCommand implements Command {

    private PersonAccountService personAccountService;

    public WatchPersonExcursionsCommand(PersonAccountService personAccountService) {
        this.personAccountService = personAccountService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PersonDTO personDTO = (PersonDTO) request.getSession().getAttribute("person");
        try {
            List<ExcursionGroupDTO> personExcursions = personAccountService.getExcursionsByPersonID(personDTO.getId());
            request.setAttribute("excursions", personExcursions);
            request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), "person_excursions");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } catch (ServiceException e) {
            request.setAttribute(AppConstants.MESSAGE.getName(), "exception.message.error_occurred");
            request.setAttribute(AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW.getName(), AppConstants.MESSAGE.getName());
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}