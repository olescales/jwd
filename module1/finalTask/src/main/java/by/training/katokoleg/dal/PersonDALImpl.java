package by.training.katokoleg.dal;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dal.transactions.ConnectionManager;
import by.training.katokoleg.entity.Person;
import by.training.katokoleg.exceptions.DAOException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Bean
public class PersonDALImpl implements PersonDAL {

    private static final Logger LOGGER = Logger.getLogger(PersonDALImpl.class);
    private static final String SAVE_QUERY = "INSERT INTO excursions.person(name, last_name, birth_date, login, password, phone_number, email) VALUES (?,?,?,?,?,?,?)";
    private static final String FIND_PERSON_BY_LOGIN_QUERY = "SELECT * FROM excursions.person WHERE login=?";
    private static final String FIND_PERSON_BY_EMAIL_QUERY = "SELECT * FROM excursions.person WHERE email=?";
    private static final String FIND_PERSON_BY_ID = "SELECT * FROM excursions.person WHERE id = ?";
    private static final String DELETE_BY_ID = "DELETE FROM excursions.person WHERE id = ?";
    private static final String GET_PERSON_ID_BY_NAME_DATE_LOGIN = "SELECT id FROM excursions.person WHERE name = ? AND birth_date = ? AND login = ?";
    private ConnectionManager connectionManager;

    public PersonDALImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(Person entity) throws DAOException {
        long personID = 0L;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SAVE_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getLastName());
            preparedStatement.setDate(3, Date.valueOf(entity.getBirthDate()));
            preparedStatement.setString(4, entity.getLogin());
            preparedStatement.setString(5, entity.getPassword());
            preparedStatement.setString(6, entity.getPhoneNumber());
            preparedStatement.setString(7, entity.getEmail());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Save person failed.");
            }
            ResultSet generatedKey = preparedStatement.getGeneratedKeys();
            if (generatedKey.next()) {
                personID = generatedKey.getLong(1);
            }
            return personID;
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
            throw new DAOException("exception.message.db_save_error", e);
        }
    }

    @Override
    public Person get(Long aLong) throws DAOException {
        Person person = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_PERSON_BY_ID)) {
            preparedStatement.setLong(1, aLong);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Long id = resultSet.getLong("id");
                    String name = resultSet.getString("name");
                    String lastName = resultSet.getString("last_name");
                    LocalDate birthDate = resultSet.getDate("birth_date").toLocalDate();
                    String personLogin = resultSet.getString("login");
                    String personPassword = resultSet.getString("password");
                    String phoneNumber = resultSet.getString("phone_number");
                    String photoReference = resultSet.getString("photo_reference");
                    person = new Person(id, name, lastName, birthDate, personLogin, personPassword, phoneNumber, photoReference);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
            throw new DAOException(e.getMessage(), e);
        }
        return person;
    }

    @Override
    public void update(Long aLong, Person entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long id) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public List<Person> findAll() throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<Person> findByLogin(String loginForCheck) {
        Optional<Person> person = Optional.empty();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_PERSON_BY_LOGIN_QUERY)) {
            preparedStatement.setString(1, loginForCheck);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Long id = resultSet.getLong(1);
                    String name = resultSet.getString(2);
                    String personLogin = resultSet.getString(7);
                    String personPassword = resultSet.getString(8);
                    person = Optional.of(new Person(id, name, personLogin, personPassword));
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
        }
        return person;
    }

    @Override
    public Optional<Person> findByEmail(String loginForCheck) {
        Optional<Person> person = Optional.empty();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_PERSON_BY_EMAIL_QUERY)) {
            preparedStatement.setString(1, loginForCheck);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Long id = resultSet.getLong(1);
                    String name = resultSet.getString(2);
                    String personLogin = resultSet.getString(7);
                    String personPassword = resultSet.getString(8);
                    person = Optional.of(new Person(id, name, personLogin, personPassword));
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
        }
        return person;
    }

    @Override
    public long getPersonID(Person person) throws DAOException {
        long personID = 0L;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_PERSON_ID_BY_NAME_DATE_LOGIN)) {
            preparedStatement.setString(1, person.getName());
            preparedStatement.setDate(2, Date.valueOf(person.getBirthDate()));
            preparedStatement.setString(3, person.getLogin());
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    personID = resultSet.getLong("id");
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
            throw new DAOException(e.getMessage(), e);
        }
        return personID;
    }
}