package by.training.katokoleg.entity;

import java.time.LocalDateTime;

public class ExcursionGroup {

    private long id;
    private long excursionID;
    private LocalDateTime excursionDateTime;
    private int executionStageID;
    private long guideID;
    private int registeredTourist;
    private int groupSize;

    public ExcursionGroup() {
    }

    public ExcursionGroup(LocalDateTime excursionDateTime) {
        this.excursionDateTime = excursionDateTime;
    }

    public ExcursionGroup(long id, long excursionID, LocalDateTime excursionDateTime, int executionStageID, long guideID, int registeredTourist, int groupSize) {
        this.id = id;
        this.excursionID = excursionID;
        this.excursionDateTime = excursionDateTime;
        this.executionStageID = executionStageID;
        this.guideID = guideID;
        this.registeredTourist = registeredTourist;
        this.groupSize = groupSize;
    }

    public ExcursionGroup(long id, long excursionID, LocalDateTime excursionDateTime, int executionStageID, long guideID, int registeredTourist) {
        this.id = id;
        this.excursionID = excursionID;
        this.excursionDateTime = excursionDateTime;
        this.executionStageID = executionStageID;
        this.guideID = guideID;
        this.registeredTourist = registeredTourist;

    }
    public ExcursionGroup(long id, long excursionID, LocalDateTime excursionDateTime, int executionStageID, long guideID) {
        this.id = id;
        this.excursionID = excursionID;
        this.excursionDateTime = excursionDateTime;
        this.executionStageID = executionStageID;
        this.guideID = guideID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getExcursionID() {
        return excursionID;
    }

    public void setExcursionID(long excursionID) {
        this.excursionID = excursionID;
    }

    public LocalDateTime getExcursionDateTime() {
        return excursionDateTime;
    }

    public void setExcursionDateTime(LocalDateTime excursionDateTime) {
        this.excursionDateTime = excursionDateTime;
    }

    public int getExecutionStageID() {
        return executionStageID;
    }

    public void setExecutionStageID(int executionStageID) {
        this.executionStageID = executionStageID;
    }

    public long getGuideID() {
        return guideID;
    }

    public void setGuideID(long guideID) {
        this.guideID = guideID;
    }

    public int getRegisteredTourist() {
        return registeredTourist;
    }

    public void setRegisteredTourist(int registeredTourist) {
        this.registeredTourist = registeredTourist;
    }

    public int getGroupSize() {
        return groupSize;
    }

    public void setGroupSize(int groupSize) {
        this.groupSize = groupSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExcursionGroup)) return false;

        ExcursionGroup that = (ExcursionGroup) o;

        if (id != that.id) return false;
        if (excursionID != that.excursionID) return false;
        if (executionStageID != that.executionStageID) return false;
        if (guideID != that.guideID) return false;
        if (registeredTourist != that.registeredTourist) return false;
        if (groupSize != that.groupSize) return false;
        return excursionDateTime != null ? excursionDateTime.equals(that.excursionDateTime) : that.excursionDateTime == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (excursionID ^ (excursionID >>> 32));
        result = 31 * result + (excursionDateTime != null ? excursionDateTime.hashCode() : 0);
        result = 31 * result + executionStageID;
        result = 31 * result + (int) (guideID ^ (guideID >>> 32));
        result = 31 * result + registeredTourist;
        result = 31 * result + groupSize;
        return result;
    }
}
