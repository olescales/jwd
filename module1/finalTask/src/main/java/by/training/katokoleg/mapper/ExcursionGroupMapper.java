package by.training.katokoleg.mapper;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dto.ExcursionGroupDTO;
import by.training.katokoleg.entity.ExcursionGroup;

@Bean
public class ExcursionGroupMapper implements Mapper<ExcursionGroupDTO, ExcursionGroup> {
    @Override
    public ExcursionGroup fromDTOtoEntity(ExcursionGroupDTO dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ExcursionGroupDTO fromEntityToDTO(ExcursionGroup entity) {
        ExcursionGroupDTO excursionGroupDTO = new ExcursionGroupDTO();
        if (entity.getId() > 0) {
            excursionGroupDTO.setId(entity.getId());
        }
        if (entity.getExcursionID() > 0) {
            excursionGroupDTO.setExcursionID(entity.getExcursionID());
        }
        if (entity.getExcursionDateTime() != null) {
            excursionGroupDTO.setExcursionDateTime(entity.getExcursionDateTime());
        }
        if (entity.getExecutionStageID() > 0) {
            excursionGroupDTO.setExecutionStageID(entity.getExecutionStageID());
        }
        if (entity.getGroupSize() > 0) {
            excursionGroupDTO.setGroupSize(excursionGroupDTO.getGroupSize());
        }
        if (entity.getGuideID() > 0) {
            excursionGroupDTO.setGuideID(entity.getGuideID());
        }
        if (entity.getRegisteredTourist() > 0) {
            excursionGroupDTO.setAmountRegisteredTourists(entity.getRegisteredTourist());
        }
        return excursionGroupDTO;
    }
}
