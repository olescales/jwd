package by.training.katokoleg.controller.command;

import by.training.katokoleg.AppConstants;
import by.training.katokoleg.core.Bean;
import by.training.katokoleg.SecurityContext;
import by.training.katokoleg.dto.Message;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.service.PersonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Bean("login_command")
public class LoginCommand implements Command {

    PersonService personService;

    public LoginCommand(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        Message<PersonDTO> answer = personService.login(login, password);
        PersonDTO personDTO = answer.getDto();
        if (personDTO != null) {
            HttpSession session = request.getSession();
            session.setMaxInactiveInterval(15 * 60);
            SecurityContext.getInstance().login(session.getId(), personDTO);
            session.setAttribute("person", personDTO);
        } else {
            request.setAttribute(AppConstants.MESSAGE.getName(), answer);
        }
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}