package by.training.katokoleg.dal;

import by.training.katokoleg.entity.Comment;
import by.training.katokoleg.exceptions.DAOException;

import java.util.List;

public interface CommentDAL extends CRUD<Long, Comment> {

    List<Comment> findAllByExcursionID(long excursionID) throws DAOException;

    List<Comment> findAllCommentsByPersonID(long personID) throws DAOException;
}
