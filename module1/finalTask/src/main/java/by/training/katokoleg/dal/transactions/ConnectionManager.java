package by.training.katokoleg.dal.transactions;

import java.sql.Connection;

public interface ConnectionManager {

    Connection getConnection();

}
