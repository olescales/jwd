package by.training.katokoleg.core;

import by.training.katokoleg.dal.transactions.BeanInterceptor;
import by.training.katokoleg.dal.transactions.Interceptor;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

public class BeanRegistryImpl implements BeanRegistry {

    private static final Logger LOGGER = Logger.getLogger(BeanRegistryImpl.class);
    private static final Map<Class<?>, Object> beanStorage = new HashMap<>();
    private Properties properties;
    private List<String> commonInterfaces;

    public BeanRegistryImpl() {
        initProperties();
        commonInterfaces = List.of(properties.getProperty("common_interfaces").split(","));
    }

    @Override
    public <T> void addBean(T bean) {
        beanStorage.put(bean.getClass().getInterfaces()[0], bean);
    }

    @Override
    public void addBean(Class<?> beanClass) {
        initBean(beanClass);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getBean(Class<?> beanClass) {
        return (T) beanStorage.get(beanClass);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getBean(String beanValue) {
        return (T) beanStorage.values()
                .stream()
                .filter(bean -> bean.getClass().getAnnotation(Bean.class) != null)
                .filter(bean -> bean.getClass().getAnnotation(Bean.class).value().equals(beanValue))
                .findFirst()
                .orElse(null);
    }

    @Override
    public <T> void removeBean(T bean) {
        beanStorage.remove(bean.getClass());
    }

    @Override
    public void destroy() {
        beanStorage.clear();
    }

    @SuppressWarnings("unchecked")
    private <T, E> void initBean(Class<?> beanClass) {
        Bean annotation = beanClass.getAnnotation(Bean.class);
        if (annotation == null) {
            throw new IllegalArgumentException("Class doesn't have bean annotation");
        }
        Constructor<?>[] constructors = beanClass.getDeclaredConstructors();
        if (constructors.length > 1) {
            throw new InstantiationBeanException("Can't instantiate bean. Ambiguity. Bean have more than one constructor");
        }
        Parameter[] parameters = constructors[0].getParameters();
        Object[] args = new Object[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            Parameter parameter = parameters[i];
            Class<?> parameterClass = parameter.getType();
            args[i] = getParameterBean(parameterClass, beanClass);
        }
        try {
            E beanInstance = (E) constructors[0].newInstance(args);
            E beanProxy = proxyBean(beanInstance);
            putBeanToStorage(beanClass, beanProxy);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    private <E> void putBeanToStorage(Class<?> beanClass, E beanInstance) {
        if (beanClass.getInterfaces().length == 1 && !commonInterfaces.contains(beanClass.getInterfaces()[0].getSimpleName())) {
            beanStorage.put(beanClass.getInterfaces()[0], beanInstance);
        } else {
            beanStorage.put(beanClass, beanInstance);
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T getParameterBean(Class<?> parameterClass, Class<?> beanClass) {
        T bean = (T) beanStorage.get(parameterClass);
        if (bean == null) {
            try {
                String paramClassAddress = getParamClassAddress(parameterClass, beanClass);
                Class<?> parameterBeanClass = Class.forName(paramClassAddress);
                bean = (T) beanStorage.get(parameterBeanClass);
            } catch (ClassNotFoundException e) {
                LOGGER.log(Level.ERROR, e.getMessage());
                throw new ParameterBeanClassNotFoundException("exception.unable_to_initialize_class");
            }
        }
        return bean;
    }

    private String getParamClassAddress(Class<?> parameterClass, Class<?> beanClass) {
        String propertyKey = beanClass.getSimpleName() + "_" + parameterClass.getSimpleName();
        String className = properties.getProperty(propertyKey);
        String[] classCanonicalName = parameterClass.getCanonicalName().split("\\.");
        Arrays.fill(classCanonicalName, classCanonicalName.length-1, classCanonicalName.length, className);
        return String.join(".", classCanonicalName);
    }

    @SuppressWarnings("unchecked")
    private <T> T proxyBean(T bean) {
        Set<BeanInterceptor> beanInterceptors = getBeanInterceptors(bean);
        if (beanInterceptors.isEmpty()) {
            return bean;
        }
        return (T) Proxy.newProxyInstance(bean.getClass().getClassLoader(), bean.getClass().getInterfaces(),
                (proxy, method, args) -> {
                    try {
                        for (BeanInterceptor interceptor : beanInterceptors) {
                            interceptor.before(method);
                        }
                        Object invoke = method.invoke(bean, args);
                        for (BeanInterceptor interceptor : beanInterceptors) {
                            interceptor.success(method);
                        }
                        return invoke;
                    } catch (Exception e) {
                        for (BeanInterceptor interceptor : beanInterceptors) {
                            interceptor.fail(method);
                        }
                        throw new IllegalStateException(e.getMessage(), e);
                    }
                });
    }

    private <T> Set<BeanInterceptor> getBeanInterceptors(T bean) {
        return beanStorage.values()
                .stream()
                .filter(value -> value.getClass().isAnnotationPresent(Interceptor.class))
                .map(value -> (BeanInterceptor) value)
                .filter(interceptor -> bean.getClass().isAnnotationPresent(interceptor.getClass().getAnnotation(Interceptor.class).clazz()))
                .collect(Collectors.toSet());
    }

    private void initProperties() {
        InputStream resourceAsStream = getClass().getResourceAsStream("/init_data/app_configuration_init_data.properties");
        try {
            properties = new Properties();
            properties.load(resourceAsStream);
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to load properties.");
        }
    }
}
