package by.training.katokoleg.dal;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dal.transactions.ConnectionManager;
import by.training.katokoleg.entity.UserRole;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Bean
public class UserRoleDALImpl implements UserRoleDAL {

    private static final Logger LOGGER = Logger.getLogger(PersonDALImpl.class);
    private static final String FIND_USER_ROLES_BY_PERSON_ID = "SELECT * FROM excursions.person_has_user_role WHERE person_id=?";
    private static final String SAVE_USER_ROLES = "INSERT INTO excursions.person_has_user_role (person_id, user_role_id) VALUES (?, ?) ";
    private static final String FIND_USER_ROLE_ID = "SELECT * FROM excursions.user_role WHERE role=?";
    private static final String FIND_USER_ROLE_BY_ID = "SELECT * FROM excursions.user_role WHERE id=?";
    private ConnectionManager connectionManager;

    public UserRoleDALImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public List<UserRole> findAllByPersonID(Long personID) {
        List<UserRole> roles = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_ROLES_BY_PERSON_ID)) {
            preparedStatement.setLong(1, personID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Long userRoleID = resultSet.getLong(2);
                    String userRole = getUserRoleByID(userRoleID);
                    roles.add(UserRole.valueOf(userRole));
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.TRACE, e);
        }
        return roles;
    }

    @Override
    public void saveUserRole(Long personID, Long userRoleID) {
        try (Connection connection = connectionManager.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SAVE_USER_ROLES)) {
            preparedStatement.setLong(1, personID);
            preparedStatement.setLong(2, userRoleID);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.TRACE, e);
        }
    }

    @Override
    public Long getUserRoleID(UserRole userRole) {
        Long userRoleID = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_ROLE_ID)) {
            preparedStatement.setString(1, userRole.name());
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    userRoleID = resultSet.getLong(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.TRACE, e);
        }
        return userRoleID;
    }

    public String getUserRoleByID (Long roleID) {
        String userRole = null;
        try (Connection connection = connectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_ROLE_BY_ID)) {
            preparedStatement.setLong(1, roleID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    userRole = resultSet.getString(2);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.TRACE, e);
        }
        return userRole;
    }
}