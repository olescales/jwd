package by.training.katokoleg.dal;

import by.training.katokoleg.dal.transactions.ConnectionManager;
import by.training.katokoleg.entity.Comment;
import by.training.katokoleg.exceptions.DAOException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CommentDALImpl implements CommentDAL {

    private static final Logger LOGGER = Logger.getLogger(CommentDALImpl.class);
    private static final String WATCH_COMMENTS_BY_EXCURSION_ID_QUERY = "SELECT * FROM excursions.comment WHERE excursion_id = ?";
    private static final String WATCH_COMMENTS_BY_PERSON_ID_QUERY = "SELECT * FROM excursions.comment WHERE person_id = ?";
    private ConnectionManager connectionManager;

    public CommentDALImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(Comment entity) throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Comment get(Long aLong) throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Long aLong, Comment entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long aLong) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Comment> findAll() throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Comment> findAllByExcursionID(long excursionID) throws DAOException {
        List<Comment> comments = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(WATCH_COMMENTS_BY_EXCURSION_ID_QUERY)) {
            preparedStatement.setString(1, String.valueOf(excursionID));
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Long personID = resultSet.getLong("person_id");
                    String textContent = resultSet.getString("text_content");
                    LocalDateTime commentDateTime = resultSet.getTimestamp("comment_date_time").toLocalDateTime();
                    Comment comment = new Comment(personID, textContent, commentDateTime);
                    comments.add(comment);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException(e.getMessage(), e);
        }
        return comments;
    }

    @Override
    public List<Comment> findAllCommentsByPersonID(long personID) throws DAOException {
        List<Comment> comments = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(WATCH_COMMENTS_BY_PERSON_ID_QUERY)) {
            preparedStatement.setLong(1, personID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    long excursionID = resultSet.getLong("excursion_id");
                    String textContent = resultSet.getString("text_content");
                    LocalDateTime commentDateTime = resultSet.getTimestamp("comment_date_time").toLocalDateTime();
                    Comment comment = new Comment(textContent, excursionID, commentDateTime);
                    comments.add(comment);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            throw new DAOException(e.getMessage(), e);
        }
        return comments;
    }
}
