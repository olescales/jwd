package by.training.katokoleg.controller.validators;

import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class DateChecker {

    private static Map<Integer, Integer> daysOfMonth = new ConcurrentHashMap<>();

    static {
        daysOfMonth.put(1, 31);
        daysOfMonth.put(3, 31);
        daysOfMonth.put(4, 30);
        daysOfMonth.put(5, 31);
        daysOfMonth.put(6, 30);
        daysOfMonth.put(7, 31);
        daysOfMonth.put(8, 31);
        daysOfMonth.put(9, 30);
        daysOfMonth.put(10, 31);
        daysOfMonth.put(11, 30);
        daysOfMonth.put(12, 31);
    }

    public static boolean checkDate(int days, int month, int year) {
        LocalDate nowDate = LocalDate.now();
        if (year == nowDate.getYear()) {
            if (month > nowDate.getMonthValue()) {
                return false;
            } else if (month == nowDate.getMonthValue()) {
                return days > 0 && days < nowDate.getDayOfMonth();
            } else {
                return isDayExist(days, month, year);
            }
        }
        if (year > 1920 && year < nowDate.getYear() && month > 0 && month < 13 && days > 0) {
            return isDayExist(days, month, year);
        }
        return false;
    }

    private static boolean isDayExist(int day, int month, int year) {
        if (month == 2) {
            if (isBissextileYear(year)) {
                return day <= 29;
            } else {
                return day <= 28;
            }
        }
        return day <= daysOfMonth.get(month);
    }

    private static boolean isBissextileYear(int year) {
        return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
    }


}
