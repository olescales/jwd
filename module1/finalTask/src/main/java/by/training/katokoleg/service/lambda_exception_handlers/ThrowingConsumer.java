package by.training.katokoleg.service.lambda_exception_handlers;

@FunctionalInterface
public interface ThrowingConsumer<T, E extends Exception> {

    void accept(T t) throws E;
}
