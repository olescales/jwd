package by.training.katokoleg.service;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.dal.CommentDAL;
import by.training.katokoleg.dto.*;
import by.training.katokoleg.entity.Comment;
import by.training.katokoleg.exceptions.DAOException;
import by.training.katokoleg.exceptions.EntityException;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.mapper.CommentMapper;
import by.training.katokoleg.mapper.Mapper;

import java.util.ArrayList;
import java.util.List;

@Bean
public class CommentServiceImpl implements CommentService {

    private CommentDAL commentDAL;
    private PersonService personService;
    private ExcursionService excursionService;
    private Mapper<CommentDTO, Comment> commentMapper;

    public CommentServiceImpl(CommentDAL commentDAL, PersonService personService, ExcursionService excursionService, Mapper<CommentDTO, Comment> commentMapper) {
        this.commentDAL = commentDAL;
        this.personService = personService;
        this.excursionService = excursionService;
        this.commentMapper = commentMapper;
    }

    @Override
    public CommentDTO save(CommentDTO element) throws ServiceException, EntityException {
       throw new UnsupportedOperationException();
    }

    @Override
    public CommentDTO get(Long id) throws ServiceException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Long id, CommentDTO element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<CommentDTO> findAll() throws ServiceException {
        throw new UnsupportedOperationException();
    }

    //CPU: O(n)
    //RAM: O(n)
    @Override
    public List<CommentDTO> findAllByExcursionID(long excursionID) throws ServiceException {
        List<CommentDTO> comments = new ArrayList<>();
        try {
            List<Comment> defExcursionComments = commentDAL.findAllByExcursionID(excursionID);
            for (Comment comment : defExcursionComments) {
                PersonDTO personDTO = personService.get(comment.getPersonID());
                CommentDTO commentDTO = commentMapper.fromEntityToDTO(comment);
                commentDTO.setPerson(personDTO);
                comments.add(commentDTO);
            }
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return comments;
    }

    //CPU: O(n)
    //RAM: O(n)
    @Override
    public List<CommentDTO> findAllByPersonID(long personID) throws ServiceException {
        List<CommentDTO> commentsForReturn = new ArrayList<>();
        try {
            List<Comment> comments = commentDAL.findAllCommentsByPersonID(personID);
            for (Comment comment : comments) {
                ExcursionDTO excursion = excursionService.get(comment.getExcursionID());
                CommentDTO commentForReturn = commentMapper.fromEntityToDTO(comment);
                commentForReturn.setExcursionDTO(excursion);
                commentsForReturn.add(commentForReturn);
            }
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return commentsForReturn;
    }
}
