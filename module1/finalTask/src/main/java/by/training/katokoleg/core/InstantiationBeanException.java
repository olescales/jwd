package by.training.katokoleg.core;

public class InstantiationBeanException extends RuntimeException {

    public InstantiationBeanException() {
        super();
    }

    public InstantiationBeanException(String message) {
        super(message);
    }

    public InstantiationBeanException(String message, Throwable cause) {
        super(message, cause);
    }
}
