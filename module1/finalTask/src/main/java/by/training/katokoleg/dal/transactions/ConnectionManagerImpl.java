package by.training.katokoleg.dal.transactions;

import java.sql.Connection;

public class ConnectionManagerImpl implements ConnectionManager {

    private TransactionalManager transactionalManager;

    public ConnectionManagerImpl(TransactionalManager transactionalManager) {
        this.transactionalManager = transactionalManager;
    }

    @Override
    public Connection getConnection() {
        return transactionalManager.getConnection();
    }
}
