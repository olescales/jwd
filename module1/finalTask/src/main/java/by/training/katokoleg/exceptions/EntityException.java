package by.training.katokoleg.exceptions;

public class EntityException extends Exception {

    public EntityException(String message) {
        super(message);
    }
}
