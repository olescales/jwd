package by.training.katokoleg.controller.command;

import by.training.katokoleg.core.Bean;
import by.training.katokoleg.SecurityContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Bean("logout_command")
public class LogoutCommand implements Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SecurityContext.getInstance().logout(request.getSession().getId());
        request.getSession().invalidate();
        response.sendRedirect("/excursionServlet");
    }
}
