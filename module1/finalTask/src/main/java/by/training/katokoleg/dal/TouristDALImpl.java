package by.training.katokoleg.dal;

import by.training.katokoleg.dal.transactions.ConnectionManager;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.entity.Tourist;
import by.training.katokoleg.exceptions.DAOException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TouristDALImpl implements TouristDAL {

    private static final Logger LOGGER = Logger.getLogger(TouristDALImpl.class);
    private static final String SAVE_QUERY = "INSERT INTO excursions.tourist(name, last_name, birth_date, excursion_group_id, phone, person_id) VALUES (?,?,?,?,?,?)";
    private static final String FIND_TOURIST_BY_NAME_LASTNAME_BIRTHDATE_PHONE_AND_EXCURSIONDATETIME = "SELECT * FROM excursions.tourist as t INNER JOIN excursions.excursion_group as e " +
            "ON t.excursion_group_id = e.id WHERE name = ? and last_name = ? and birth_date = ? and phone = ? and excursion_date_time = ?";
    private static final String FIND_TOURIST_BY_NAME_LASTNAME_BIRTHDATE_PHONE_AND_GROUPID = "SELECT * FROM excursions.tourist " +
            "WHERE name = ? and last_name = ? and birth_date = ? and phone = ? and excursion_group_id = ?";
    private static final String FIND_TOURISTS_BY_EXCURSION_GROUP_ID_AND_PERSON_ID = "SELECT * FROM excursions.tourist WHERE excursion_group_id = ? AND person_id = ?";
    private static final String DELETE_TOURIST_BY_ID = "DELETE FROM excursions.tourist WHERE id = ?";
    private static final String GET_TOURIST_BY_ID_QUERY = "SELECT * FROM excursions.tourist WHERE id = ?";
    private static final String UPDATE_TOURIST_DATA = "UPDATE excursions.tourist SET name = ?, last_name = ?, birth_date = ?, phone = ? WHERE id = ?";
    private static final String GET_TOURIST_ID = "SELECT id FROM excursions.tourist WHERE name = ? and last_name = ? and birth_date = ? and excursion_group_id = ? and phone = ? and person_id = ?";

    //multithreading connection manager??
    private ConnectionManager connectionManager;

    public TouristDALImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(Tourist entity) throws DAOException {
        long touristID = 0L;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SAVE_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getLastName());
            preparedStatement.setDate(3, Date.valueOf(entity.getBirthDate()));
            preparedStatement.setLong(4, entity.getExcursionGroupID());
            preparedStatement.setString(5, entity.getPhone());
            preparedStatement.setLong(6, entity.getPersonID());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Save tourist failed.");
            }
            ResultSet generatedKey = preparedStatement.getGeneratedKeys();
            if (generatedKey.next()) {
                touristID = generatedKey.getLong(1);
            }
            return touristID;
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
            throw new DAOException("exception.message.db_save_error", e);
        }
    }

    @Override
    public Tourist get(Long touristID) throws DAOException {
        Tourist tourist = null;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_TOURIST_BY_ID_QUERY)) {
            preparedStatement.setLong(1, touristID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    long id = resultSet.getLong("id");
                    String name = resultSet.getString("name");
                    String lastName = resultSet.getString("last_name");
                    LocalDate birthDate = resultSet.getDate("birth_date").toLocalDate();
                    String phone = resultSet.getString("phone");
                    tourist = new Tourist(id, name, lastName, birthDate, phone);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
            throw new DAOException("exception.message.db_get_data_error", e);
        }
        return tourist;
    }

    @Override
    public void update(Long touristID, Tourist tourist) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TOURIST_DATA)) {
            preparedStatement.setString(1, tourist.getName());
            preparedStatement.setString(2, tourist.getLastName());
            preparedStatement.setDate(3, Date.valueOf(tourist.getBirthDate()));
            preparedStatement.setString(4, tourist.getPhone());
            preparedStatement.setLong(5, tourist.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
            throw new DAOException("exception.message.db_update_tourists_error", e);
        }
    }

    @Override
    public void delete(Long touristID) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TOURIST_BY_ID)) {
            preparedStatement.setLong(1, touristID);
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
            throw new DAOException("exception.message.db_delete_data_error", e);
        }
    }

    @Override
    public List<Tourist> findAll() throws DAOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean watchTouristByNameLastNameBirthDatePhoneAndExcursionDateTime(TouristDTO touristDTO) {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_TOURIST_BY_NAME_LASTNAME_BIRTHDATE_PHONE_AND_EXCURSIONDATETIME)) {
            preparedStatement.setString(1, touristDTO.getName());
            preparedStatement.setString(2, touristDTO.getLastName());
            preparedStatement.setDate(3, Date.valueOf(touristDTO.getBirthDate()));
            preparedStatement.setString(4, touristDTO.getPhone());
            LocalDateTime localDateTime = LocalDateTime.parse(touristDTO.getExcursionDateTime());
            preparedStatement.setTimestamp(5, Timestamp.valueOf(localDateTime));
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return resultSet.next();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return false;
    }

    @Override
    public List<Tourist> findTouristsByExcursionGroupIDAndPersonID(long groupID, long personID) throws DAOException {
        List<Tourist> tourists = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_TOURISTS_BY_EXCURSION_GROUP_ID_AND_PERSON_ID)) {
            preparedStatement.setLong(1, groupID);
            preparedStatement.setLong(2, personID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    long id = resultSet.getLong("id");
                    String name = resultSet.getString("name");
                    String lastName = resultSet.getString("last_name");
                    LocalDate birthDate = resultSet.getDate("birth_date").toLocalDate();
                    long ticketID = resultSet.getLong("ticket_id");
                    String phone = resultSet.getString("phone");
                    Tourist tourist = new Tourist(id, name, lastName, birthDate, ticketID, groupID, phone, personID);
                    tourists.add(tourist);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
            throw new DAOException(e.getMessage(), e);
        }
        return tourists;
    }

    @Override
    public boolean watchTouristByNameLastNameBirthDatePhoneAndGroupID(TouristDTO touristDTO) {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_TOURIST_BY_NAME_LASTNAME_BIRTHDATE_PHONE_AND_GROUPID)) {
            preparedStatement.setString(1, touristDTO.getName());
            preparedStatement.setString(2, touristDTO.getLastName());
            preparedStatement.setDate(3, Date.valueOf(touristDTO.getBirthDate()));
            preparedStatement.setString(4, touristDTO.getPhone());
            preparedStatement.setString(5, touristDTO.getExcursionGroupID());
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return resultSet.next();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return false;
    }

    @Override
    public long getTouristID(Tourist tourist) throws DAOException {
        Long touristID = 0L;
        try (Connection connection = connectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_TOURIST_ID)) {
            preparedStatement.setString(1, tourist.getName());
            preparedStatement.setString(2, tourist.getLastName());
            preparedStatement.setDate(3, Date.valueOf(tourist.getBirthDate()));
            preparedStatement.setLong(4, tourist.getExcursionGroupID());
            preparedStatement.setString(5, tourist.getPhone());
            preparedStatement.setLong(6, tourist.getPersonID());
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    touristID = resultSet.getLong("id");
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e);
            throw new DAOException(e.getMessage(), e);
        }
        return touristID;
    }
}
