package by.training.katokoleg.service;

import by.training.katokoleg.dto.ExcursionGroupDTO;
import by.training.katokoleg.exceptions.ServiceException;

import java.util.List;

public interface PersonAccountService {

    List<ExcursionGroupDTO> getExcursionsByPersonID(long personID) throws ServiceException;
}

