package by.training.katokoleg.filters;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "language_filter_order_3")
public class LanguageFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(LanguageFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        LOGGER.log(Level.INFO, "Query string: " + ((HttpServletRequest) request).getQueryString());
        LOGGER.log(Level.INFO, "URI: " + ((HttpServletRequest) request).getRequestURI());
        LOGGER.log(Level.INFO, "URL: " + ((HttpServletRequest) request).getRequestURL());
        LOGGER.log(Level.INFO, "Servlet path: " + ((HttpServletRequest) request).getServletPath());
        LOGGER.log(Level.INFO, "Context path: " + ((HttpServletRequest) request).getContextPath());
        LOGGER.log(Level.INFO, "Path info: " + ((HttpServletRequest) request).getPathInfo());
        LOGGER.log(Level.INFO, "----------------------------------------------------------------");
        request.setAttribute("url", ((HttpServletRequest) request).getRequestURL());
        String langParam = request.getParameter("lang");
        HttpSession session = ((HttpServletRequest) request).getSession();
        if ("en".equals(langParam) || "ru".equals(langParam)) {
            session.setAttribute("lang", langParam);
        } else if (session.getAttribute("lang") != null) {

        } else {
            session.setAttribute("lang", "en");
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
