const form = document.getElementById('form');
const personName = document.getElementById('person_name');
const lastName = document.getElementById('last_name');
const email = document.getElementById('e-mail');
const login = document.getElementById('login');
const password = document.getElementById('password');
const passwordConfirmation = document.getElementById('password_confirmation');
const phoneNumber = document.getElementById('phone_number');

const nameRegexp = /^[A-Z][a-z]{1,29}$|^[А-Я][а-я]{1,29}$/;
const loginRegexp = /^(?=.*[A-z0-9]$)[A-z][A-z\d.-]{2,19}$/;
const passwordRegexp = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})/;
const phoneRegexp = /^25|29|33|44-[\d]{3}-[\d]{2}-[\d]{2}$/;
const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

form.addEventListener('submit', (event) => {
   event.preventDefault();

   checkInputs();
});

function checkInputs() {
    const personNameValue = personName.value.trim();
    const lastNameValue = lastName.value.trim();
    const emailValue = email.value.trim();
    const loginValue = login.value.trim();
    const passwordValue = password.value.trim();
    const passwordConfirmationValue = passwordConfirmation.value.trim();
    const phoneNumberValue = phoneNumber.value.trim();

    if (personNameValue === '') {
        setErrorFor(personName, '<fmt:message key="validation.js.cant_be_blank"/>');
    } else if (!personNameValue.match(/^[A-Z].{1,29}$|^[А-Я].{1,29}$/)) {
        setErrorFor(personName, '<fmt:message key="validation.js.invalid_first_letter"/>');
    } else if (!personNameValue.match(nameRegexp)) {
        setErrorFor(personName, '<fmt:message key="validation.js.invalid_name"/>');
    } else {
        setSucceedFor(personName);
    }

    if (lastNameValue.value === '') {

    } else if (!lastNameValue.match(/^[A-Z].{1,29}$|^[А-Я].{1,29}$/)) {
        setErrorFor(lastName, '<fmt:message key="validation.js.invalid_first_letter"/>');
    } else if (!lastNameValue.match(nameRegexp)) {
        setErrorFor(lastName, '<fmt:message key="validation.js.invalid_last_name"/>');
    } else {
        setSucceedFor(lastName);
    }

    if (emailValue === '') {
        setErrorFor(email, '<fmt:message key="validation.js.cant_be_blank"/>');
    } else if (!emailValue.match(emailRegexp)) {
        setErrorFor(email, '<fmt:message key="validation.js.invalid_email"/>');
    } else {
        setSucceedFor(email);
    }

    if (loginValue === '') {
        setErrorFor(login, '<fmt:message key="validation.js.cant_be_blank"/>');
    } else if (loginValue.length < 3 ) {
        setErrorFor(login, '<fmt:message key="validation.js.min_login_length"/>');
    }else if (loginValue.length > 30) {
        setErrorFor(login, '<fmt:message key="validation.js.max_login_length"/>');
    }else if (!loginValue.match(loginRegexp)) {
        setErrorFor(login, '<fmt:message key="validation.js.invalid_login"/>');
    } else {
        setSucceedFor(login);
    }

    if (passwordValue === '') {
        setErrorFor(password, '<fmt:message key="validation.js.cant_be_blank"/>');
    } else if (passwordValue.length < 8 ) {
        setErrorFor(password, '<fmt:message key="validation.js.min_password_length"/>');
    }else if (passwordValue.length > 20) {
        setErrorFor(password, '<fmt:message key="validation.js.max_password_length"/>');
    }else if (!passwordValue.match(passwordRegexp)) {
        setErrorFor(password, '<fmt:message key="validation.js.invalid_password"/>');
    } else {
        setSucceedFor(password);
    }

    if (passwordConfirmationValue === '') {
        setErrorFor(passwordConfirmation, '<fmt:message key="validation.js.cant_be_blank"/>');
    } else if (passwordValue !== passwordConfirmationValue) {
        setErrorFor(passwordConfirmation, '<fmt:message key="validation.js.passwords_doesnt_match"/>');
    } else {
        setSucceedFor(passwordConfirmation);
    }

    if (phoneNumberValue === '') {

    } else if (!phoneNumberValue.match(phoneRegexp)) {
        setErrorFor(phoneNumber, '<fmt:message key="validation.js.invalid_phone"/>');
    } else {
        setSucceedFor(phoneNumber);
    }
}

function setErrorFor(input, message) {
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');

    small.innerText = message;
    formControl.className = 'form-control error';
}

function setSucceedFor(input) {
    const formControl = input.parentElement;

    formControl.className = 'form-control success';
}