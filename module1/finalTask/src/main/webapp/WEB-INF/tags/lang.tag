<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${sessionScope.lang}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n/ApplicationLanguage" scope="application"/>

<c:choose>
    <c:when test="${ not empty pageContext.request.queryString}">
        <c:set var="query" value="${pageContext.request.queryString}"/>
        <c:choose>
            <c:when test="${query.contains('&lang')}">
                <c:set var="query" value="${query.replace('&lang='.concat(param.lang), '')}"/>
                <c:set var="url" value="${requestScope.url}?${query}&"/>
            </c:when>
            <c:when test="${query.contains('lang')}">
                <c:set var="query" value="${query.replace('lang='.concat(param.lang), '')}"/>
                <c:set var="url" value="${requestScope.url}?"/>
            </c:when>
            <c:otherwise>
                <c:set var="url" value="${requestScope.url}?${query}&"/>
            </c:otherwise>
        </c:choose>
    </c:when>
    <c:otherwise>
        <c:set var="url" value="${requestScope.url}?"/>
    </c:otherwise>
</c:choose>

<c:forTokens items="en,ru" delims="," var="lang">
    <a href="${url}lang=${lang}"><fmt:message key="links.lang.${lang}"/></a>
</c:forTokens>
