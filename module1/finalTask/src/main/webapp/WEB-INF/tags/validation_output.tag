<%@ attribute name="wrong_field" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:if test="${requestScope.wrong_fields.containsKey(wrong_field)}">
    <c:set var="field" value="${requestScope.wrong_fields.get(wrong_field)}"/>
    <c:set value="${field.getFieldContent()}" var="field_content"/>
    <c:set value="${field.getValidationMessage()}" var="validation_message"/>
    <fmt:message key="${validation_message}" var="answer"/>
    <c:out value="${answer} : ${field_content}"/>
</c:if>

