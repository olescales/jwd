<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="by.training.katokoleg.controller.command.CommandType" %>

<div class="sign-in">
    <c:if test="${not empty requestScope.message}">
        <c:out value="${requestScope.message.badMessage}"/>
    </c:if>
    <form method="post" class="sign-in-form" action="${pageContext.request.contextPath}/login">
        <label for="login"><fmt:message key="text.log_in"/>:</label><br>
        <input type="text" class="intup" id="login" name="login"><br><br>
        <label for="password"><fmt:message key="text.password"/>:</label><br>
        <input type="password" class="intup" id="password" name="password"><br><br>
        <input type="hidden" name="command_name" value="${CommandType.LOGIN_COMMAND.getName()}">
        <input type="submit" class="sign-button" value="<fmt:message key="button.sing_in"/> ">
    </form>
    <form action="${pageContext.request.contextPath}/sign_up">
        <input type="hidden" name="command_name" value="${CommandType.SIGN_UP_FORM_COMMAND.getName()}">
        <input type="submit" class="sign-button" value="<fmt:message key="button.sing_up"/>">
    </form>
</div>