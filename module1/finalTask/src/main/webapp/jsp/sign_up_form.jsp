<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.katokoleg.controller.command.CommandType" %>
<%@ taglib prefix="my_tags" tagdir="/WEB-INF/tags" %>
<html>
<head>
    <title>Tourist company</title>
    <link rel="stylesheet" href="static/css/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="static/css/sign_in_form.css">
</head>
<body>
<div class="choose-localization">
    <my_tags:lang/>
</div>

<div class="container">
    <div class="header">
        <h2><fmt:message key="text.user_registration"/></h2>
    </div>
    <form class="form" id="form" method="post" action="${pageContext.request.contextPath}/sign_up">
        <div class="form-control">
            <label for="person_name" class="label"><fmt:message key="text.name"/>
                <div class="asteriks">*</div>
            </label>
            <input type="text" id="person_name" name="person_name" class="person_name field">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="personName"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>

        <div class="form-control">
            <label for="last_name"><fmt:message key="text.last_name"/></label>
            <input type="text" id="last_name" name="last_name" class="last_name">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="lastName"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>

        <div class="form-control">
            <label for="birth_date" class="label"
                   title="<fmt:message key="validation.js.date_format_requirement"/>">
                <fmt:message key="text.date_of_birth"/>
                <div class="asteriks">*</div>
            </label>
            <input type="date" placeholder="yyyy-mm-dd" id="birth_date" name="birth_date"
                   class="birth_date field">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="birthDate"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>

        <div class="form-control">
            <label for="e-mail" class="label"><fmt:message key="text.e-mail"/>
                <div class="asteriks">*</div>
            </label>
            <input type="text" placeholder="<fmt:message key="validation.js.email_requirement"/>" id="e-mail"
                   name="e-mail"
                   class="e-mail field">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="email"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>

        <div class="form-control">
            <label for="login" class="label"><fmt:message key="text.log_in"/>
                <div class="asteriks">*</div>
            </label>
            <input type="text" placeholder="<fmt:message key="validation.js.login_requirement"/>" id="login"
                   name="login"
                   class="login field">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="login"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>

        <div class="form-control">
            <label for="password" title="<fmt:message key="validation.js.password_requirement"/>" class="label">
                <fmt:message key="text.password"/>
                <div class="asteriks">*</div>
            </label>
            <input type="password" placeholder="<fmt:message key="validation.js.password_length_requirement"/>"
                   id="password"
                   name="password" class="password field">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="password"/>
            </small>
            <small class="front-validation">Error message</small>
            <label for="togglePassword" class="label"><fmt:message key="validation.js.show_password"/><input
                    type="checkbox"
                    id="togglePassword"></label>
        </div>

        <div class="form-control">
            <label for="password_confirmation" class="label"><fmt:message key="text.password_confirmation"/>
                <div class="asteriks">*</div>
            </label>
            <input type="password" id="password_confirmation" name="password_confirmation"
                   class="password_confirmation field">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="passwordConfirmation"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>

        <div class="form-control">
            <label for="phone_number"><fmt:message key="text.phone"/></label>
            <input type="text" placeholder="хх-ххх-хх-хх" id="phone_number" name="phone_number"
                   class="phone_number">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="phone"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>

        <input type="hidden" name="command_name" value="${CommandType.SIGN_UP_COMMAND.getName()}">
        <input type="submit" class="register-button" value="<fmt:message key="button.sing_up"/>"><br>
        <small class="small">
            <div class="asteriks">*</div>
            - <fmt:message key="text.fields_with_asterisk"/>
        </small>
    </form>
</div>

<script type="text/javascript">
    const form = document.getElementById('form');
    const personName = document.getElementById('person_name');
    const lastName = document.getElementById('last_name');
    const birthDate = document.getElementById('birth_date');
    const email = document.getElementById('e-mail');
    const login = document.getElementById('login');
    const password = document.getElementById('password');
    const passwordConfirmation = document.getElementById('password_confirmation');
    const phoneNumber = document.getElementById('phone_number');

    const nameRegexp = /^[A-Z][a-z]{1,29}$|^[А-Я][а-я]{1,29}$/;
    const loginRegexp = /^(?=.*[A-z0-9]$)[A-z][A-z\d.-]{2,19}$/;
    const passwordRegexp = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})/;
    const phoneRegexp = /^(25|29|33|44)-[\d]{3}-[\d]{2}-[\d]{2}$/;
    const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    form.addEventListener('submit', (event) => {
        if (!checkInputs()) {
            event.preventDefault();
        }
    });

    function checkInputs() {
        const personNameValue = personName.value.trim();
        const lastNameValue = lastName.value.trim();
        const birthDateValue = birthDate.value;
        const emailValue = email.value.trim();
        const loginValue = login.value.trim();
        const passwordValue = password.value.trim();
        const passwordConfirmationValue = passwordConfirmation.value.trim();
        const phoneNumberValue = phoneNumber.value.trim();

        let result = true;

        if (personNameValue === '') {
            setErrorFor(personName, "<fmt:message key="validation.js.cant_be_blank"/>");
        } else if (!personNameValue.match(/^[A-Z].*$|^[А-Я].*$/)) {
            setErrorFor(personName, "<fmt:message key="validation.js.invalid_first_letter"/>");
        } else if (!personNameValue.match(nameRegexp)) {
            setErrorFor(personName, "<fmt:message key="validation.js.invalid_name"/>");
        } else {
            setSucceedFor(personName);
        }

        if (lastNameValue === '') {
            setNeutralFor(lastName);
        } else if (lastNameValue !== '' && !lastNameValue.match(/^[A-Z].*$|^[А-Я].*$/)) {
            setErrorFor(lastName, "<fmt:message key="validation.js.invalid_first_letter"/>");
        } else if (!lastNameValue.match(nameRegexp)) {
            setErrorFor(lastName, "<fmt:message key="validation.js.invalid_last_name"/>");
        } else {
            setSucceedFor(lastName);
        }

        if (birthDateValue === '') {
            setErrorFor(birthDate, "<fmt:message key="validation.js.cant_be_blank"/>");
        } else {
            setSucceedFor(birthDate);
        }

        if (emailValue === '') {
            setErrorFor(email, "<fmt:message key="validation.js.cant_be_blank"/>");
        } else if (!emailValue.match(emailRegexp)) {
            setErrorFor(email, "<fmt:message key="validation.js.invalid_email"/>");
        } else {
            setSucceedFor(email);
        }

        if (loginValue === '') {
            setErrorFor(login, "<fmt:message key="validation.js.cant_be_blank"/>");
        } else if (loginValue.length < 3) {
            setErrorFor(login, "<fmt:message key="validation.js.min_login_length"/>");
        } else if (loginValue.length > 30) {
            setErrorFor(login, "<fmt:message key="validation.js.max_login_length"/>");
        } else if (!loginValue.match(loginRegexp)) {
            setErrorFor(login, "<fmt:message key="validation.js.invalid_login"/>");
        } else {
            setSucceedFor(login);
        }

        if (passwordValue === '') {
            setErrorFor(password, "<fmt:message key="validation.js.cant_be_blank"/>");
        } else if (passwordValue.length < 8) {
            setErrorFor(password, "<fmt:message key="validation.js.min_password_length"/>");
        } else if (passwordValue.length > 20) {
            setErrorFor(password, "<fmt:message key="validation.js.max_password_length"/>");
        } else if (!passwordValue.match(passwordRegexp)) {
            setErrorFor(password, "<fmt:message key="validation.js.invalid_password"/>");
        } else {
            setSucceedFor(password);
        }

        if (passwordConfirmationValue === '') {
            setErrorFor(passwordConfirmation, "<fmt:message key="validation.js.cant_be_blank"/>");
        } else if (passwordValue !== passwordConfirmationValue) {
            setErrorFor(passwordConfirmation, "<fmt:message key="validation.js.passwords_doesnt_match"/>");
        } else {
            setSucceedFor(passwordConfirmation);
        }

        if (phoneNumberValue === '') {
            setNeutralFor(phoneNumber);
        } else if (!phoneNumberValue.match(phoneRegexp)) {
            setErrorFor(phoneNumber, "<fmt:message key="validation.js.invalid_phone"/>");
        } else {
            setSucceedFor(phoneNumber);

        }

        function setErrorFor(input, message) {
            result = false;
            const formControl = input.parentElement;
            const small = formControl.querySelector('small.front-validation');

            small.innerText = message;
            formControl.className = 'form-control error';
        }

        function setSucceedFor(input) {
            const formControl = input.parentElement;

            formControl.className = 'form-control success';
        }

        function setNeutralFor(input) {
            const formControl = input.parentElement;

            formControl.className = 'form-control';
        }

        return result;
    }
</script>
<script type="text/javascript">
    <%@include file="/resources/js/password_toggle.js"%>
</script>
</body>
</html>
