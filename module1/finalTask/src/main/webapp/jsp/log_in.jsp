<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="by.training.katokoleg.controller.command.CommandType" %>
<%@page import="by.training.katokoleg.entity.UserRole" %>

<div class="login">
    <c:out value="${sessionScope.person.name}"/>
    <form action="${pageContext.request.contextPath}/excursions/">
        <input type="hidden" name="command_name" value="${CommandType.LOGOUT_COMMAND.getName()}">
        <input type="submit" value="<fmt:message key="button.logout"/>">
    </form>

    <c:set var="admin" value="${UserRole.ADMIN}"/>
    <c:if test="${sessionScope.person.roles.contains(admin)}">
        <c:set var="is_admin" value="true"/>
    </c:if>
    <form action="${pageContext.request.contextPath}/excursions/person">
        <input type="hidden" name="is_admin" value="${is_admin}"/>
        <input type="hidden" name="command_name" value="${CommandType.WATCH_PERSON_ACCOUNT_COMMAND.getName()}">
        <input type="submit" value="<fmt:message key="button.personal_account"/>">
    </form>
</div>
