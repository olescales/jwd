<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="by.training.katokoleg.controller.command.CommandType" %>

<div class="single-excursion-info">
    <div class="image">
        <img src="${pageContext.request.contextPath}${sessionScope.excursion.photoReference}"
             alt="${sessionScope.excursion.topic}">
    </div>
    <div class="info-block">
        <div class="description">
            <c:out value="${sessionScope.excursion.description}"/>
        </div>
        <div class="location">
            <fmt:message key="text.place"/>:
            <c:out value="${sessionScope.excursion.location}"/>
        </div>
        <div class="cost">
            <fmt:message key="text.cost"/>:
            <c:out value="${sessionScope.excursion.cost}"/>
        </div>
        <div class="schedule">
            <c:choose>
                <c:when test="${empty sessionScope.excursion.formattedExcursionSchedule}">
                    <fmt:message key="text.no_available_excursions_now"/>
                </c:when>
                <c:otherwise>
                    <fmt:message key="text.choose_date" var="choose_date"/>
                    <c:out value="${choose_date}"/>
                    <form class="register" action="${pageContext.request.contextPath}/excursions/registration">
                        <select name="excursion_date_time">
                            <option disabled><c:out value="${choose_date}"/></option>
                            <c:forEach items="${sessionScope.excursion.excursionSchedule}"
                                       var="excursion_date">
                                <option value="${excursion_date}"><c:out value="${excursion_date}"/></option>
                            </c:forEach>
                        </select>
                        <input type="hidden" name="excursion_id" value="${sessionScope.excursion.id}">
                        <input type="hidden" name="command_name"
                               value="${CommandType.EXCURSION_FORM_REGISTRATION_COMMAND.getName()}">
                        <input type="submit" class="register-excursion-button"
                               value="<fmt:message key="button.register"/>">
                    </form>
                </c:otherwise>
            </c:choose>
        </div>
        <form class="watch-comments" action="${pageContext.request.contextPath}/excursions/comments">
            <input type="hidden" name="excursion_id" value="${param.excursion_id}">
            <input type="hidden" name="command_name" value="${CommandType.WATCH_EXCURSION_COMMENTS_COMMAND.getName()}">
            <input type="submit" class="watch-comments-button" value="<fmt:message key="button.comment"/>">
        </form>
    </div>
</div>

<c:if test="${not empty requestScope.excursion_include_view}">
    <jsp:include page="${requestScope.excursion_include_view}.jsp"/>
</c:if>