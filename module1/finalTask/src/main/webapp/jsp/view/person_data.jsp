<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="person_info">
    Personal information:
    <c:out value="${sessionScope.person.name}"/>
    <c:out value="${sessionScope.person.birthDate}"/>
    <c:out value="${sessionScope.person.phoneNumber}"/>
</div>
