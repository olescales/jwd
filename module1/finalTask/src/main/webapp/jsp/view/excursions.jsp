<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.training.katokoleg.controller.command.CommandType" %>
<%@ page import="by.training.katokoleg.AppConstants" %>

<c:forEach items="${applicationScope.excursions}" var="excursion">
    <div class="excursion-info">
        <div class="image">
            <img src="${pageContext.request.contextPath}${excursion.minPhotoReference}"
                 alt="${excursion.topic}">
        </div>
        <div class="info-block">
            <div class="description">
                <c:out value="${excursion.description}"/>
            </div>
            <div class="cost">
                <fmt:message key="text.cost"/>:
                <c:out value="${excursion.cost}"/>
            </div>
            <div class="schedule">
                <c:choose>
                    <c:when test="${empty excursion.formattedExcursionSchedule}">
                        <fmt:message key="text.no_available_excursions_now"/>
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="text.available_dates" var="available_dates"/>
                        <c:out value="${available_dates}"/>
                        <ul>
                            <c:forEach items="${excursion.formattedExcursionSchedule}" var="excursion_date">
                                <li><c:out value="${excursion_date}"/></li>
                            </c:forEach>
                        </ul>
                    </c:otherwise>
                </c:choose>
            </div>
            <form class="watch-info" action="${pageContext.request.contextPath}/excursions/${excursion.id}">
                <input type="hidden" name="command_name" value="${CommandType.WATCH_EXCURSION_COMMAND.getName()}">
                <input type="hidden" name="excursion_id" value="${excursion.id}">
                <input type="hidden" name="scope" value="${AppConstants.SESSION}">
                <input type="hidden" name="view_name" value="${AppConstants.INDEX_INCLUDE_VIEW}">
                <input type="submit" class="watch-info-button" value="<fmt:message key="button.show_info"/>">
            </form>
            <form class="watch-comments" action="${pageContext.request.contextPath}/excursions/comments">
                <input type="hidden" name="excursion_id" value="${excursion.id}">
                <input type="hidden" name="command_name"
                       value="${CommandType.WATCH_EXCURSION_COMMENTS_COMMAND.getName()}">
                <input type="submit" class="watch-comments-button" value="<fmt:message key="button.comment"/>">
            </form>
        </div>
    </div>
</c:forEach>