<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div class="modal_message">
    <fmt:message key="${sessionScope.message}" var="message"/>
    <c:out value="${message}"/>
    <a href="${sessionScope.reference}"><fmt:message key="button.OK"/></a>
</div>
