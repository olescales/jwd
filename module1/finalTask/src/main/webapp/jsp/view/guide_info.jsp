<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<img class="guide_logo" src="${pageContext.request.contextPath}${requestScope.guide.photoReference}"
     alt="Avatar"/><br>
<fmt:message key="text.name"/>: <c:out value="${requestScope.guide.name}"/><br>
<fmt:message key="text.phone"/>: <c:out value="${requestScope.guide.phoneNumber}"/>