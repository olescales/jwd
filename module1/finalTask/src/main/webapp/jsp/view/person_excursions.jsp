<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.training.katokoleg.AppConstants" %>
<%@ page import="by.training.katokoleg.controller.command.CommandType" %>
<%@ taglib prefix="my_tags" tagdir="/WEB-INF/tags" %>

<div class="groups-info">
    <table id="excursion-groups-table">
        <caption><fmt:message key="header.my_excursions"/></caption>
        <c:choose>
            <c:when test="${empty requestScope.excursions}">
                <fmt:message key="text.no_excursions_yet"/>
            </c:when>
            <c:otherwise>
                <c:forEach items="${requestScope.excursions}" var="group">
                    <tr>
                        <th scope="col"><fmt:message key="header.excursion_topic"/></th>
                        <th scope="col"><fmt:message key="header.excursion_date_time"/></th>
                        <th scope="col"><fmt:message key="header.excursion_execution_stage"/></th>
                        <th scope="col"><fmt:message key="header.guide"/></th>
                        <th scope="col"></th>
                    </tr>
                    <tr>
                        <td>
                            <a href="${requestScope.url}?command_name=${CommandType.WATCH_EXCURSION_COMMAND.getName()}&guide_id=${group.excursion.id}&view_name=${AppConstants.PERSON_ACCOUNT_INCLUDE_VIEW}&scope=${AppConstants.REQUEST}&excursion_id=${group.excursion.id}"><c:out
                                    value="${group.excursion.topic}"/></a></td>
                        <td><c:out value="${group.excursionDateTime}"/></td>
                        <td><c:out value="${group.executionStage}"/></td>
                        <td>
                            <a href="${requestScope.url}?command_name=${CommandType.WATCH_PERSON_INFO_COMMAND.getName()}&guide_id=${group.guide.id}"><c:out
                                    value="${group.guide.name}"/></a></td>
                        <form action="${pageContext.request.contextPath}/personal_account/excursions"
                              onsubmit="return confirm('<fmt:message key="text.are_you_sure"/>')">
                            <input type="hidden" name="group_id" value="${group.id}"/>
                            <input type="hidden" name="command_name"
                                   value="${CommandType.DELETE_REGISTERED_TOURISTS_COMMAND.getName()}">
                            <c:set var="tourists" value="${group.registeredTourist}" scope="session"/>
                            <input type="hidden" name="many_tourists" value="true">
                            <td>
                                <input type="submit" value="<fmt:message key="text.delete"/>">
                            </td>
                        </form>
                    </tr>
                    <tr>
                        <td>
                            <table id="tourists-table">
                                <caption><fmt:message key="header.my_tourists"/></caption>
                                <tr>
                                    <th scope="col"><fmt:message key="text.name"/></th>
                                    <th scope="col"><fmt:message key="text.last_name"/></th>
                                    <th scope="col"><fmt:message key="text.date_of_birth"/></th>
                                    <th scope="col"><fmt:message key="text.phone"/></th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                </tr>
                                <c:forEach items="${group.registeredTourist}" var="tourist">
                                    <tr>
                                        <td><c:out value="${tourist.name}"/></td>
                                        <td><c:out value="${tourist.lastName}"/></td>
                                        <td><c:out value="${tourist.birthDate}"/></td>
                                        <td><c:out value="${tourist.phone}"/></td>
                                        <form action="${pageContext.request.contextPath}/personal_account/excursions"
                                              onsubmit="return confirm('<fmt:message key="text.are_you_sure"/>')">
                                            <input type="hidden" name="group_id" value="${group.id}"/>
                                            <input type="hidden" name="command_name"
                                                   value="${CommandType.DELETE_REGISTERED_TOURISTS_COMMAND.getName()}">
                                            <input type="hidden" name="tourist_id" value="${tourist.id}">
                                            <td>
                                                <input type="submit" value="<fmt:message key="text.delete"/>">
                                            </td>
                                        </form>
                                        <form action="${pageContext.request.contextPath}/personal_account/excursions">
                                            <input type="hidden" name="command_name"
                                                   value="${CommandType.WATCH_TOURIST_DATA_COMMAND.getName()}">
                                            <input type="hidden" name="tourist_id" value="${tourist.id}">
                                            <input type="hidden" name="group_id" value="${group.id}">
                                            <td>
                                                <input type="submit" value="<fmt:message key="text.edit"/>">
                                            </td>
                                        </form>

                                    </tr>
                                </c:forEach>
                            </table>
                        </td>
                    </tr>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </table>
</div>