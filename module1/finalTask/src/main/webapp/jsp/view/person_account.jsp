<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.training.katokoleg.controller.command.CommandType" %>

<ul id="nav-bar">
    <form class="watch_excursions_button" action="${pageContext.request.contextPath}/personal_account/excursions">
        <input type="hidden" name="command_name" value="${CommandType.WATCH_PERSON_EXCURSIONS_COMMAND.getName()}">
        <li>
            <input type="submit" value="<fmt:message key="button.my_excursions"/>">
        </li>
    </form>
    <form class="watch_comments_button" action="${pageContext.request.contextPath}/personal_account/comments">
        <input type="hidden" name="command_name" value="${CommandType.WATCH_PERSON_COMMENTS_COMMAND.getName()}">
        <li>
            <input type="submit" value="<fmt:message key="button.my_comments"/>">
        </li>
    </form>
</ul>
<c:choose>
    <c:when test="${empty requestScope.person_account_include_view}">
        <jsp:include page="person_data.jsp"/>
    </c:when>
    <c:otherwise>
        <jsp:include page="${requestScope.person_account_include_view}.jsp"/>
    </c:otherwise>
</c:choose>



