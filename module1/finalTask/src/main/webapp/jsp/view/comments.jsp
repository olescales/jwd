<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:choose>
    <c:when test="${empty requestScope.comments}">
        <fmt:message key="message.no_comments_yet" var="no_comments"/>
        <c:out value="${no_comments}"/>
    </c:when>
    <c:otherwise>
            <c:forEach items="${requestScope.comments}" var="comment">
                <div class="comment">
                    <c:out value="${comment.person.name}"/>
                    <c:out value="${comment.textContent}"/>
                    <c:out value="${comment.commentDateTime}"/>
                </div>
            </c:forEach>
    </c:otherwise>
</c:choose>