<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="by.training.katokoleg.controller.command.CommandType" %>
<%@ taglib prefix="my_tags" tagdir="/WEB-INF/tags"%>

<style>
    <%@include file="/static/css/sign_in_form.css"%>
</style>

<div class="container">
    <div class="header">
        <h2><fmt:message key="text.tourist_registration"/></h2>
    </div>
    <form class="form" id="form" method="post" action="${pageContext.request.contextPath}/excursions/registration">
        <div class="form-control">
            <label for="tourist_name" class="label"><fmt:message key="text.name"/>
                <div class="asteriks">*</div>
            </label>
            <input type="text" id="tourist_name" name="tourist_name" class="person_name field">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="touristName"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>

        <div class="form-control">
            <label for="last_name" class="label"><fmt:message key="text.last_name"/>
                <div class="asteriks">*</div>
            </label>
            <input type="text" id="last_name" name="last_name" class="last_name">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="lastName"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>

        <div class="form-control">
            <label for="birth_date" class="label"
                   title="<fmt:message key="validation.js.date_format_requirement"/>">
                <fmt:message key="text.date_of_birth"/>
                <div class="asteriks">*</div>
            </label>
            <input type="date" placeholder="yyyy-mm-dd" id="birth_date" name="birth_date"
                   class="birth_date field">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="birthDate"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>

        <div class="form-control">
            <label for="phone_number" class="label"><fmt:message key="text.phone"/><div class="asteriks">*</div></label>
            <input type="text" placeholder="хх-ххх-хх-хх" id="phone_number" name="phone_number"
                   class="phone_number">
            <i class="fa fa-check-circle"></i>
            <i class="fa fa-exclamation-circle"></i>
            <small class="back-validation">
                <my_tags:validation_output wrong_field="phone"/>
            </small>
            <small class="front-validation">Error message</small>
        </div>
        <small class="back-validation">
            <c:if test="${not empty requestScope.message}">
                <fmt:message key="${requestScope.message}" var="report"/>
                <c:out value="${report}"/>
            </c:if>
        </small>
        <input type="hidden" name="excursion_date_time" value="${param.excursion_date_time}"/>
        <input type="hidden" name="excursion_id" value="${param.excursion_id}">
        <input type="hidden" name="person_id" value="${sessionScope.person.id}">
        <input type="hidden" name="command_name" value="${CommandType.EXCURSION_REGISTRATION_COMMAND.getName()}">
        <input type="submit" class="register-button" value="<fmt:message key="button.sing_up"/>"><br>
        <small class="small">
            <div class="asteriks">*</div>
            - <fmt:message key="text.fields_with_asterisk"/>
        </small>
    </form>
</div>

<div class="message">
    <section>
        <div class="message">
            <c:if test="${not empty requestScope.message}">
                <fmt:message key="${requestScope.message}"/>
            </c:if>
        </div>
    </section>
</div>

<script type="text/javascript">
    const form = document.getElementById('form');
    const touristName = document.getElementById('tourist_name');
    const lastName = document.getElementById('last_name');
    const birthDate = document.getElementById('birth_date');
    const phoneNumber = document.getElementById('phone_number');

    const nameRegexp = /^[A-Z][a-z]{1,29}$|^[А-Я][а-я]{1,29}$/;
    const phoneRegexp = /^(25|29|33|44)-[\d]{3}-[\d]{2}-[\d]{2}$/;

    form.addEventListener('submit', (event) => {
        if (!checkInputs()) {
            event.preventDefault();
        }
    });

    function checkInputs() {
        const touristNameValue = touristName.value.trim();
        const lastNameValue = lastName.value.trim();
        const birthDateValue = birthDate.value;
        const phoneNumberValue = phoneNumber.value.trim();

        let result = true;

        if (touristNameValue === '') {
            setErrorFor(touristName, "<fmt:message key="validation.js.cant_be_blank"/>");
        } else if (!touristNameValue.match(/^[A-Z].*$|^[А-Я].*$/)) {
            setErrorFor(touristName, "<fmt:message key="validation.js.invalid_first_letter"/>");
        } else if (!touristNameValue.match(nameRegexp)) {
            setErrorFor(touristName, "<fmt:message key="validation.js.invalid_name"/>");
        } else {
            setSucceedFor(touristName);
        }

        if (lastNameValue === '') {
            setErrorFor(lastName, "<fmt:message key="validation.js.cant_be_blank"/>");
        } else if (lastNameValue !== '' && !lastNameValue.match(/^[A-Z].*$|^[А-Я].*$/)) {
            setErrorFor(lastName, "<fmt:message key="validation.js.invalid_first_letter"/>");
        } else if (!lastNameValue.match(nameRegexp)) {
            setErrorFor(lastName, "<fmt:message key="validation.js.invalid_last_name"/>");
        } else {
            setSucceedFor(lastName);
        }

        if (birthDateValue === '') {
            setErrorFor(birthDate, "<fmt:message key="validation.js.cant_be_blank"/>");
        } else {
            setSucceedFor(birthDate);
        }

        if (phoneNumberValue === '') {
            setErrorFor(phoneNumber, "<fmt:message key="validation.js.cant_be_blank"/>");
        } else if (!phoneNumberValue.match(phoneRegexp)) {
            setErrorFor(phoneNumber, "<fmt:message key="validation.js.invalid_phone"/>");
        } else {
            setSucceedFor(phoneNumber);

        }

        function setErrorFor(input, message) {
            result = false;
            const formControl = input.parentElement;
            const small = formControl.querySelector('small.front-validation');

            small.innerText = message;
            formControl.className = 'form-control error';
        }

        function setSucceedFor(input) {
            const formControl = input.parentElement;

            formControl.className = 'form-control success';
        }

        function setNeutralFor(input) {
            const formControl = input.parentElement;

            formControl.className = 'form-control';
        }

        return result;
    }
</script>