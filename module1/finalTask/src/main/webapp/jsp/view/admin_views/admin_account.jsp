<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.training.katokoleg.controller.command.CommandType" %>

<form action="${pageContext.request.contextPath}/personal_account/excursions">
    <input type="submit" name="command_name" value="${CommandType.WATCH_PLANNED_EXCURSIONS.getName()}"/>
    <input type="submit" value="<fmt:message key="button.planned_excursions"/>">
</form>

<form action="${pageContext.request.contextPath}/personal_account/excursions">
    <input type="submit" name="command_name" value="${CommandType.WATCH_EXCURSIONS_TOURISTS.getName()}"/>
    <input type="submit" value="<fmt:message key="button.my_excursions"/>">
</form>

