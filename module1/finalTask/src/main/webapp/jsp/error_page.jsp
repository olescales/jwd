<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="lang" tagdir="/WEB-INF/tags" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE>
<html>
    <head>
    <title>Error occurred</title>
    </head>
    <body>
        <img src="${pageContext.request.contextPath}/static/img/something_go_wrong.png" alt="something_go_wrong" height="385" width="500">
        <c:choose>
            <c:when test="${not empty requestScope.message}">
                <fmt:message key="${requestScope.message}" var="error"/>
                <c:out value="${error}"/>
            </c:when>
            <c:when test="${not empty pageContext.exception}">
                <c:out value="${pageContext.exception}"/>
            </c:when>
            <c:otherwise>

            </c:otherwise>
        </c:choose>
        <br><br>

        <a href="${pageContext.request.contextPath}"><fmt:message key="text.return_to_main_page"/></a>
    </body>
</html>