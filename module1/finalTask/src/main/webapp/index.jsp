<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.katokoleg.controller.command.CommandType" %>
<%@ taglib prefix="lang" tagdir="/WEB-INF/tags" %>
<%@ page errorPage="/jsp/error_page.jsp" %>
<html>
<head>
    <title>Tourist agency</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/style.css">
</head>
<body>
<div class="choose-localization">
    <lang:lang/>
</div>
<header>

    <a href="${pageContext.request.contextPath}?command_name=${CommandType.WATCH_HOMEPAGE_COMMAND.getName()}">
        <img class="grafic-logo" width="100%" src="${pageContext.request.contextPath}/static/img/logo_skorohod.jpg"
             alt="Logo"></a>
    <nav>
        <div class="topnav">
            <a href="${pageContext.request.contextPath}?command_name=${CommandType.WATCH_HOMEPAGE_COMMAND.getName()}"><fmt:message
                    key="topnav.home"/></a>
            <a href="${pageContext.request.contextPath}/jsp/view/contacts.jsp"><fmt:message key="topnav.contacts"/></a>
            <a href="${pageContext.request.contextPath}/jsp/view/about.jsp"><fmt:message key="topnav.about"/></a>
            <a href="${pageContext.request.contextPath}/jsp/view/comments.jsp"><fmt:message key="topnav.comments"/></a>
            <a href="${pageContext.request.contextPath}?command_name=${CommandType.WATCH_EXCURSIONS_COMMAND.getName()}"><fmt:message
                    key="topnav.excursions"/></a>
        </div>
    </nav>
    <div class="sign-in-menu">
        <c:choose>
            <c:when test="${not empty sessionScope.person}">
                <jsp:include page="jsp/log_in.jsp"/>
            </c:when>
            <c:otherwise>
                <jsp:include page="jsp/sign_in.jsp"/>
            </c:otherwise>
        </c:choose>
    </div>
</header>

<div class="working-board">
    <div class="modal-messages">
        <c:if test="${sessionScope.flag == 1}">
            <jsp:include page="jsp/view/modal_message.jsp"/>
        </c:if>
        <c:if test="${not empty requestScope.security_context_answer}">
            <fmt:message key="${requestScope.security_context_answer}" var="security_message"/>
            <c:out value="${security_message}"/>
        </c:if>
    </div>
    <div class="view">
        <c:choose>
            <c:when test="${not empty sessionScope.index_include_view}">
                <jsp:include page="jsp/view/${sessionScope.index_include_view}.jsp"/>
            </c:when>
            <c:when test="${empty requestScope.index_include_view}">
                <jsp:include page="jsp/view/excursions.jsp"/>
            </c:when>
            <c:otherwise>
                <jsp:include page="jsp/view/${requestScope.index_include_view}.jsp"/>
            </c:otherwise>
        </c:choose>
    </div>
</div>

</body>
</html>