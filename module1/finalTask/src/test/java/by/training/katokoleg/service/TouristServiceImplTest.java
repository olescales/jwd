package by.training.katokoleg.service;

import by.training.katokoleg.ApplicationContext;
import by.training.katokoleg.controller.security.PasswordAuthenticationWrapper;
import by.training.katokoleg.dal.*;
import by.training.katokoleg.dal.connectionpool.DataSource;
import by.training.katokoleg.dal.connectionpool.MySQLDataSource;
import by.training.katokoleg.dal.transactions.ConnectionManager;
import by.training.katokoleg.dal.transactions.ConnectionManagerImpl;
import by.training.katokoleg.dal.transactions.TransactionalManager;
import by.training.katokoleg.dal.transactions.TransactionalManagerImpl;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.dto.ValidationResult;
import by.training.katokoleg.entity.Person;
import by.training.katokoleg.entity.Tourist;
import by.training.katokoleg.mapper.Mapper;
import by.training.katokoleg.mapper.PersonMapper;
import by.training.katokoleg.mapper.TouristMapper;
import lombok.SneakyThrows;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TouristServiceImplTest {

    private static TouristService touristService;
    private static TouristDAL touristDAL;

    @BeforeClass
    public static void initData() {
        ApplicationContext.getInstance().initBeans();
        touristService = ApplicationContext.getInstance().getBean(TouristService.class);
        touristDAL = ApplicationContext.getInstance().getBean(TouristDAL.class);
    }

    @SneakyThrows
    @Test
    public void saveTourists() {
        TouristDTO touristDTO = new TouristDTO("Oleg", "Olegovich", "1988-09-30", "29-233-55-88", "22", "13", "2020-09-30T00:00");
        ValidationResult<TouristDTO> saveResult = touristService.saveTourists(List.of(touristDTO));
        Assert.assertTrue(saveResult.isValidated());
        Assert.assertNull(saveResult.getBadData());
    }

    @SneakyThrows
    @AfterClass
    public static void removeRemnants() {
        Tourist tourist = new Tourist("Oleg", "Olegovich", LocalDate.of(1988, 9, 30), 1L, "29-233-55-88", 22L);
        long touristID = touristDAL.getTouristID(tourist);
        Long [] touristsID = {touristID};
        touristService.deleteTouristsFromGroup(touristsID, 1L);
    }
}