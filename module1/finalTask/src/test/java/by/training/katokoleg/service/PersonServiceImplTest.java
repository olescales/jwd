package by.training.katokoleg.service;

import by.training.katokoleg.ApplicationContext;
import by.training.katokoleg.dto.Message;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.dto.ValidationResult;
import by.training.katokoleg.entity.UserRole;
import lombok.SneakyThrows;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class PersonServiceImplTest {

    private static PersonService personService;

    @BeforeClass
    public static void prepareFields() {
        ApplicationContext.getInstance().initBeans();
        personService = ApplicationContext.getInstance().getBean(PersonService.class);
    }

    @SneakyThrows
    @AfterClass
    public static void removeRemnants() {
        PersonDTO testPerson = new PersonDTO();
        testPerson.setName("Igor");
        testPerson.setBirthDate("1964-09-11");
        testPerson.setLogin("igorek");
        testPerson.setPassword("Igoresha");
        testPerson.setEmail("Igor@rogi.by");
        long testPersonID = personService.getPersonID(testPerson);
        personService.delete(testPersonID);
    }

    @SneakyThrows
    @Test
    public void signUpReturnFalse() {
        PersonDTO person = new PersonDTO();
        person.setLogin("moneta");
        person.setPassword("Igroman1988");
        person.setName("Oleg");
        person.setEmail("krokko@tut.by");
        person.setBirthDate("1988-09-30");
        ValidationResult<PersonDTO> validationResult = personService.signUp(person);
        assertFalse(validationResult.isValidated());
    }

    @SneakyThrows
    @Test
    public void signUpReturnTrue() {
        PersonDTO testPerson = new PersonDTO();
        testPerson.setName("Igor");
        testPerson.setBirthDate("1964-09-11");
        testPerson.setLogin("igorek");
        testPerson.setPassword("Igoresha");
        testPerson.setEmail("Igor@rogi.by");
        testPerson.setRoles(List.of(UserRole.CLIENT));
        ValidationResult<PersonDTO> validationResult = personService.signUp(testPerson);
        boolean actualResult = validationResult.isValidated();
        assertTrue(actualResult);
    }

    @Test
    public void logInAndGetPerson() {
        Message<PersonDTO> message = personService.login("moneta", "Igroman1988");
        assertNotNull(message.getDto());
    }
}