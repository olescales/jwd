package by.training.katokoleg.controller.validators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class DateCheckerTest {

    @Test
    public void checkDateValidationOK() {
        boolean condition = DateChecker.checkDate(25, 9, 2019);
        assertTrue(condition);
    }

    @Test
    public void checkDateValidationFalling() {
        boolean condition = DateChecker.checkDate(25, 9, 2021);
        assertFalse(condition);
    }

    @Test
    public void checkDateNowYearValidationIsOK() {
        boolean condition = DateChecker.checkDate(19, 9, 2020);
        assertTrue(condition);
    }

    @Test
    public void checkDateWrongDayValidationFalling() {
        boolean condition = DateChecker.checkDate(32, 9, 2019);
        assertFalse(condition);
    }
}