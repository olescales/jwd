package by.training.katokoleg.service;

import by.training.katokoleg.ApplicationContext;
import by.training.katokoleg.controller.security.PasswordAuthenticationWrapper;
import by.training.katokoleg.dal.*;
import by.training.katokoleg.dal.connectionpool.DataSource;
import by.training.katokoleg.dal.connectionpool.MySQLDataSource;
import by.training.katokoleg.dal.transactions.ConnectionManager;
import by.training.katokoleg.dal.transactions.ConnectionManagerImpl;
import by.training.katokoleg.dal.transactions.TransactionalManager;
import by.training.katokoleg.dal.transactions.TransactionalManagerImpl;
import by.training.katokoleg.dto.PersonDTO;
import by.training.katokoleg.entity.ExcursionGroup;
import by.training.katokoleg.entity.Person;
import by.training.katokoleg.mapper.Mapper;
import by.training.katokoleg.mapper.PersonMapper;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ExcursionGroupServiceImplTest {

    private static ExcursionGroupDAL excursionGroupDAL;

    @BeforeClass
    public static void init () {
        ApplicationContext.getInstance().initBeans();
        excursionGroupDAL = ApplicationContext.getInstance().getBean(ExcursionGroupDAL.class);
    }

    @SneakyThrows
    @Test
    public void registerTouristsInExcursionGroup() {
        int numberOfTourists = 1;
        int groupSizeBeforeInsert = excursionGroupDAL.getExcursionGroupByID(3L).get().getRegisteredTourist();
        excursionGroupDAL.addTouristsToExcursionGroup(3, numberOfTourists);
        int groupSizeAfterInsert = excursionGroupDAL.getExcursionGroupByID(3L).get().getRegisteredTourist();
        Assert.assertEquals(groupSizeBeforeInsert + numberOfTourists, groupSizeAfterInsert);
    }

    @SneakyThrows
    @Test
    public void removeTouristsFromGroup() {
        int numberOfTourists = 1;
        int groupSizeBeforeInsert = excursionGroupDAL.getExcursionGroupByID(3L).get().getRegisteredTourist();
        excursionGroupDAL.reduceTheNumberOfRegisteredTourists(numberOfTourists, 3);
        int groupSizeAfterInsert = excursionGroupDAL.getExcursionGroupByID(3L).get().getRegisteredTourist();
        Assert.assertEquals(groupSizeBeforeInsert - numberOfTourists, groupSizeAfterInsert);
    }
}