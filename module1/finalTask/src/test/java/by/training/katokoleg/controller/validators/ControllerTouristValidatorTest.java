package by.training.katokoleg.controller.validators;

import by.training.katokoleg.dto.TouristDTO;
import by.training.katokoleg.dto.ValidationResult;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ControllerTouristValidatorTest {

    EntityValidator<TouristDTO> entityValidator;

    @Before
    public void prepare() {
        entityValidator = new ControllerTouristValidator();
    }

    @Test
    public void validateIsOK() {
        TouristDTO touristDTO = new TouristDTO();
        touristDTO.setName("Олег");
        touristDTO.setLastName("Каток");
        touristDTO.setBirthDate("1988-09-30");
        touristDTO.setPhone("29-345-56-89");
        ValidationResult validationMessage = entityValidator.validate(touristDTO);
        Assert.assertTrue(validationMessage.isValidated());
    }

    @Test
    public void validateIsFalse() {
        TouristDTO touristDTO = new TouristDTO();
        touristDTO.setName("Ол");
        touristDTO.setLastName("Каток");
        touristDTO.setBirthDate("1988-09-30");
        touristDTO.setPhone("347727739");
        ValidationResult validationMessage = entityValidator.validate(touristDTO);
        Assert.assertFalse(validationMessage.isValidated());
    }

    @Test
    public void validate() {

    }
}