package by.training.katokoleg.dal;

import by.training.katokoleg.dal.connectionpool.ConnectionPool;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class ConnectionPoolTest {

    ConnectionPool connectionPool;

    @Before
    public void createConnectionPool() {
        String jdbcDriver = "com.mysql.cj.jdbc.Driver";
        String jdbcUrl = "jdbc:mysql://127.0.0.1:3306/excursions";
        String jdbcLogin = "root";
        String jdbcPassword = "admin";
        this.connectionPool = ConnectionPool.getInstance(7, jdbcDriver, jdbcUrl, jdbcLogin, jdbcPassword);
    }

    @Test
    public void closedConnectionIsStillAlive() {
        Connection connection = connectionPool.getConnection();
        try {
            connection.close();
            assertTrue(connection.isValid(2));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}