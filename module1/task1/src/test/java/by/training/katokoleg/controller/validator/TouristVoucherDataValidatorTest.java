package by.training.katokoleg.controller.validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TouristVoucherDataValidatorTest {

    private Validator validator;

    @Before
    public void initValidator() {
        validator = new TouristVoucherDataValidator();
    }

    @Test
    public void extensionIsOkTest() {
        String filePath = "E:\\Oleg\\input.txt";
        boolean result = validator.validateExtension(filePath);
        Assert.assertTrue(result);
    }

    @Test
    public void invalidExtensionTest() {
        String filePath = "E:\\Oleg\\input.txt";
        boolean result = validator.validateExtension(filePath);
        Assert.assertTrue(result);
    }
}
