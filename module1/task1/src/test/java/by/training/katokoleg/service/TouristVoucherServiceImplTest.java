package by.training.katokoleg.service;

import by.training.katokoleg.dal.TouristVoucherDALImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class TouristVoucherServiceImplTest {

    private TouristVoucherDALImpl touristVoucherDAL;
    private TouristVoucherService touristVoucherService;

    @Before
    public void initTestFields () {
        touristVoucherDAL = new TouristVoucherDALImpl();
        touristVoucherService = new TouristVoucherServiceImpl(touristVoucherDAL);
    }

    @Test
    public void test() {
        String touristVoucherData = "SHOPPING/2020-04-12/11/CAR/3222/HB";
        touristVoucherService.loadTouristVoucherToDB(List.of(touristVoucherData));
        int actualSize = touristVoucherDAL.getTouristVouchers().size();
        assertEquals(6, actualSize);
    }
}