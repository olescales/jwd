import by.training.katokoleg.command.Command;
import by.training.katokoleg.command.GetFilePathCommand;
import by.training.katokoleg.controller.CommandFactory;
import by.training.katokoleg.controller.CommandFactoryImpl;
import by.training.katokoleg.controller.validator.TouristVoucherDataValidator;
import by.training.katokoleg.controller.validator.Validator;
import by.training.katokoleg.dal.TouristVoucherDAL;
import by.training.katokoleg.dal.TouristVoucherDALImpl;
import by.training.katokoleg.dto.ResponseMessage;
import by.training.katokoleg.service.TouristVoucherService;
import by.training.katokoleg.service.TouristVoucherServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TouristVoucherAppTest {

    private TouristVoucherDAL touristVoucherDAL;
    private TouristVoucherService touristVoucherService;
    private CommandFactory commandFactory;
    private Validator validator;

    @Before
    public void initTestFields () {
        touristVoucherDAL = new TouristVoucherDALImpl();
        touristVoucherService = new TouristVoucherServiceImpl(touristVoucherDAL);
        validator = new TouristVoucherDataValidator();
        Map<String,Command> commands = new HashMap<>();
        commands.put("getFilePathCommand", new GetFilePathCommand(touristVoucherService,validator));
        commandFactory = new CommandFactoryImpl(commands);
    }

    @Test
    public void addDataToInMemoryDataBaseIsOkTest () {
        Map<String,String> requestData = new HashMap<>();
        requestData.put("absolutePathToFile", "E:\\Oleg\\input.txt");
        Command command = commandFactory.getCommand("getFilePathCommand");
        String actual = command.execute(requestData);

        File fileWithData = new File("E:\\Oleg\\input.txt");
        ResponseMessage responseMessage = validator.validateFile(fileWithData);
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/html/fileAddedTemplate.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        String template = bufferedReader.lines().collect(Collectors.joining());
        String expected = MessageFormat.format(template, "Data from file were added successfully", responseMessage.getValidData());

        assertEquals(expected,actual);
    }

    @After
    public void cleanTestFields () {
        touristVoucherDAL = null;
        touristVoucherService = null;
        commandFactory = null;
        validator = null;
    }
}
