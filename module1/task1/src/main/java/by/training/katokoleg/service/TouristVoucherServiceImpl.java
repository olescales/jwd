package by.training.katokoleg.service;

import by.training.katokoleg.dal.TouristVoucherDAL;
import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.entity.TypeOfFeeding;
import by.training.katokoleg.entity.TypeOfRest;
import by.training.katokoleg.entity.TypeOfTransport;
import by.training.katokoleg.exceptions.DALException;
import by.training.katokoleg.exceptions.ServiceException;

import javax.sql.rowset.serial.SerialException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TouristVoucherServiceImpl implements TouristVoucherService {

    private TouristVoucherDAL touristVoucherDAL;

    public TouristVoucherServiceImpl(TouristVoucherDAL touristVoucherDAL) {
        this.touristVoucherDAL = touristVoucherDAL;
    }

    @Override
    public List<TouristVoucher> findAllTouristVouchers() {
        return touristVoucherDAL.findAllTouristVouchers();
    }

    @Override
    public List<TouristVoucher> findTouristVouchersByFeedingType(String typeOfFeeding) {
        return  touristVoucherDAL.findTouristVouchersByFeeding(typeOfFeeding);
    }

    @Override
    public List<TouristVoucher> findAllTouristVouchersSortedByCondition(String comparator) throws ServiceException {
        try {
            return touristVoucherDAL.findAllTouristVouchersSortedByCondition(comparator);
        } catch (DALException dalException) {
            throw new ServiceException(dalException.getMessage(),dalException);
        }
    }

    @Override
    public void loadTouristVoucherToDB(List<String> touristVouchersDataToSave) {
        touristVouchersDataToSave.stream().map(this::convertDataToTouristVoucher).forEach(touristVoucherDAL::loadTouristVoucherToDB);
    }

    private TouristVoucher convertDataToTouristVoucher(String dataLine) {
        String[] data = dataLine.split("/");
        return new TouristVoucher(TypeOfRest.valueOf(data[0]),
                LocalDate.parse(data[1]),
                Integer.parseInt(data[2]),
                TypeOfTransport.valueOf(data[3]),
                new BigDecimal(data[4]),
                TypeOfFeeding.valueOf(data[5]));
    }
}
