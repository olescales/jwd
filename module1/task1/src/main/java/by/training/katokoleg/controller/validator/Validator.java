package by.training.katokoleg.controller.validator;

import by.training.katokoleg.dto.ResponseMessage;

import java.io.File;

public interface Validator {

    ResponseMessage validateFile (File fileWithTouristVouchersData);

    boolean validateExtension(String filePath);

}
