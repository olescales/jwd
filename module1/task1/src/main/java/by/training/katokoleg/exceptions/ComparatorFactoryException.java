package by.training.katokoleg.exceptions;

public class ComparatorFactoryException extends Exception {

    public ComparatorFactoryException() {
    }

    public ComparatorFactoryException(String message) {
        super(message);
    }
}
