package by.training.katokoleg.controller;

import by.training.katokoleg.command.Command;

public interface CommandFactory {

    Command getCommand (String commandName);
}
