package by.training.katokoleg.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class TouristVoucher implements Comparable<TouristVoucher> {

    private TypeOfRest typeOfRest;

    private LocalDate beginningDateOfTheTour;
    private LocalDate endingDateOfTheTour;
    private Integer durationInNight;
    private Set<Accommodation> accommodation;
    private List<Destination> holidayDestination;
    private TypeOfFeeding typeOfFeeding;
    private TypeOfTransport typeOfTransport;
    private BigDecimal cost;
    private Currency currency;
    private TourCompany tourOperator;

    public TouristVoucher() {
    }

    public TouristVoucher(TypeOfRest typeOfRest, LocalDate beginningDateOfTheTour, LocalDate endingDateOfTheTour,
                          Integer durationInNight, Set<Accommodation> accommodation, List<Destination> holidayDestination,
                          TypeOfFeeding typeOfFeeding, TypeOfTransport typeOfTransport, BigDecimal cost, Currency currency,
                          TourCompany tourOperator) {
        this.typeOfRest = typeOfRest;
        this.beginningDateOfTheTour = beginningDateOfTheTour;
        this.endingDateOfTheTour = endingDateOfTheTour;
        this.durationInNight = durationInNight;
        this.accommodation = accommodation;
        this.holidayDestination = holidayDestination;
        this.typeOfFeeding = typeOfFeeding;
        this.typeOfTransport = typeOfTransport;
        this.cost = cost;
        this.currency = currency;
        this.tourOperator = tourOperator;
    }

    public TouristVoucher(TypeOfRest typeOfRest, LocalDate beginningDateOfTheTour, Integer durationInNight,
                          TypeOfTransport typeOfTransport, BigDecimal cost, TypeOfFeeding typeOfFeeding) {
        this.typeOfRest = typeOfRest;
        this.beginningDateOfTheTour = beginningDateOfTheTour;
        this.durationInNight = durationInNight;
        this.typeOfTransport = typeOfTransport;
        this.cost = cost;
        this.typeOfFeeding = typeOfFeeding;
        currency = Currency.BYN;
        endingDateOfTheTour = beginningDateOfTheTour.plusDays(durationInNight);
    }

    public TouristVoucher(TypeOfRest typeOfRest, LocalDate beginningDateOfTheTour, Integer durationInNight,
                          TypeOfTransport typeOfTransport, BigDecimal cost) {
        this.typeOfRest = typeOfRest;
        this.beginningDateOfTheTour = beginningDateOfTheTour;
        this.durationInNight = durationInNight;
        this.typeOfTransport = typeOfTransport;
        this.cost = cost;
        typeOfFeeding = TypeOfFeeding.RR;
        currency = Currency.BYN;
        endingDateOfTheTour = beginningDateOfTheTour.plusDays(durationInNight);
    }
    public TypeOfRest getTypeOfRest() {
        return typeOfRest;
    }

    public void setTypeOfRest(TypeOfRest typeOfRest) {
        this.typeOfRest = typeOfRest;
    }

    public LocalDate getBeginningDateOfTheTour() {
        return beginningDateOfTheTour;
    }

    public void setBeginningDateOfTheTour(LocalDate beginningDateOfTheTour) {
        this.beginningDateOfTheTour = beginningDateOfTheTour;
    }

    public LocalDate getEndingDateOfTheTour() {
        return endingDateOfTheTour;
    }

    public void setEndingDateOfTheTour(LocalDate endingDateOfTheTour) {
        this.endingDateOfTheTour = endingDateOfTheTour;
    }

    public Integer getDurationInNight() {
        return durationInNight;
    }

    public void setDurationInNight(Integer durationInNight) {
        this.durationInNight = durationInNight;
    }

    public Set<Accommodation> getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(Set<Accommodation> accommodation) {
        this.accommodation = accommodation;
    }

    public List<Destination> getHolidayDestination() {
        return holidayDestination;
    }

    public void setHolidayDestination(List<Destination> holidayDestination) {
        this.holidayDestination = holidayDestination;
    }

    public TypeOfFeeding getTypeOfFeeding() {
        return typeOfFeeding;
    }

    public void setTypeOfFeeding(TypeOfFeeding typeOfFeeding) {
        this.typeOfFeeding = typeOfFeeding;
    }

    public TypeOfTransport getTypeOfTransport() {
        return typeOfTransport;
    }

    public void setTypeOfTransport(TypeOfTransport typeOfTransport) {
        this.typeOfTransport = typeOfTransport;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public TourCompany getTourOperator() {
        return tourOperator;
    }

    public void setTourOperator(TourCompany tourOperator) {
        this.tourOperator = tourOperator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TouristVoucher that = (TouristVoucher) o;

        if (typeOfRest != that.typeOfRest) return false;
        if (!Objects.equals(beginningDateOfTheTour, that.beginningDateOfTheTour))
            return false;
        if (!Objects.equals(endingDateOfTheTour, that.endingDateOfTheTour))
            return false;
        if (!Objects.equals(durationInNight, that.durationInNight))
            return false;
        if (!Objects.equals(accommodation, that.accommodation))
            return false;
        if (!Objects.equals(holidayDestination, that.holidayDestination))
            return false;
        if (typeOfFeeding != that.typeOfFeeding) return false;
        if (typeOfTransport != that.typeOfTransport) return false;
        if (!Objects.equals(cost, that.cost)) return false;
        if (currency != that.currency) return false;
        return Objects.equals(tourOperator, that.tourOperator);
    }

    @Override
    public int hashCode() {
        int result = typeOfRest != null ? typeOfRest.hashCode() : 0;
        result = 31 * result + (beginningDateOfTheTour != null ? beginningDateOfTheTour.hashCode() : 0);
        result = 31 * result + (endingDateOfTheTour != null ? endingDateOfTheTour.hashCode() : 0);
        result = 31 * result + (durationInNight != null ? durationInNight.hashCode() : 0);
        result = 31 * result + (accommodation != null ? accommodation.hashCode() : 0);
        result = 31 * result + (holidayDestination != null ? holidayDestination.hashCode() : 0);
        result = 31 * result + (typeOfFeeding != null ? typeOfFeeding.hashCode() : 0);
        result = 31 * result + (typeOfTransport != null ? typeOfTransport.hashCode() : 0);
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (tourOperator != null ? tourOperator.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TouristVoucher{" +
                "typeOfRest=" + typeOfRest +
                ", beginningDateOfTheTour=" + beginningDateOfTheTour +
                ", endingDateOfTheTour=" + endingDateOfTheTour +
                ", durationInNight=" + durationInNight +
                ", accommodation=" + accommodation +
                ", holidayDestination=" + holidayDestination +
                ", typeOfFeeding=" + typeOfFeeding +
                ", typeOfTransport=" + typeOfTransport +
                ", cost=" + cost +
                ", currency=" + currency +
                ", tourOperator=" + tourOperator +
                '}';
    }

    @Override
    public int compareTo(TouristVoucher o) {
        return this.getCost().compareTo(o.getCost());
    }
}
