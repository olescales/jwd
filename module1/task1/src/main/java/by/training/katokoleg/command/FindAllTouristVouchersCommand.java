package by.training.katokoleg.command;

import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.service.TouristVoucherService;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.training.katokoleg.TouristVouchersApp.LOGGER;

public class FindAllTouristVouchersCommand implements Command {

    private TouristVoucherService touristVoucherService;

    public FindAllTouristVouchersCommand(TouristVoucherService touristVoucherService) {
        this.touristVoucherService = touristVoucherService;
    }

    @Override
    public String execute(Map<String, String> request) {
        LOGGER.info("findAllTouristVouchersCommand");
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/html/template.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        String template = bufferedReader.lines().collect(Collectors.joining());

        StringBuilder touristVoucherResponse = new StringBuilder();
        touristVoucherResponse.append("<ul>");
        List<TouristVoucher> touristVouchers = touristVoucherService.findAllTouristVouchers();
        for (TouristVoucher touristVoucher : touristVouchers) {
            touristVoucherResponse.append("<li>");
            touristVoucherResponse.append(touristVoucher.toString());
            touristVoucherResponse.append("</li>");
        }
        touristVoucherResponse.append("</ul>");


        return MessageFormat.format(template, touristVoucherResponse);
    }
}
