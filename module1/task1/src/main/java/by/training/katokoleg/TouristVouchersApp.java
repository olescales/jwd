package by.training.katokoleg;

import by.training.katokoleg.command.*;
import by.training.katokoleg.controller.CommandFactory;
import by.training.katokoleg.controller.CommandFactoryImpl;
import by.training.katokoleg.controller.TouristVouchersHandler;
import by.training.katokoleg.controller.validator.TouristVoucherDataValidator;
import by.training.katokoleg.controller.validator.Validator;
import by.training.katokoleg.dal.TouristVoucherDAL;
import by.training.katokoleg.dal.TouristVoucherDALImpl;
import by.training.katokoleg.dal.comparator.ComparatorFactory;
import by.training.katokoleg.dal.comparator.ComparatorFactoryImpl;
import by.training.katokoleg.dal.comparator.DescCostComparator;
import by.training.katokoleg.dal.comparator.TypeOfTransportComparator;
import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.service.TouristVoucherService;
import by.training.katokoleg.service.TouristVoucherServiceImpl;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TouristVouchersApp {

    public static final Logger LOGGER = LogManager.getLogger(TouristVouchersApp.class);

    public static void main(String[] args) throws IOException {

        InetSocketAddress localhost = new InetSocketAddress("localhost", 49090);
        HttpServer server = HttpServer.create(localhost, 0);

        Map<String,Comparator<TouristVoucher>> comparators = new HashMap<>();
        Comparator<TouristVoucher> comparatorOne = new DescCostComparator();
        Comparator<TouristVoucher> comparatorTwo = new TypeOfTransportComparator();
        comparators.put("descCostComparator",comparatorOne);
        comparators.put("typeOfTransportComparator",comparatorTwo);
        ComparatorFactory comparatorFactory = new ComparatorFactoryImpl(comparators);

        TouristVoucherDAL touristVoucherDAL = new TouristVoucherDALImpl(comparatorFactory);
        TouristVoucherService touristVoucherService = new TouristVoucherServiceImpl(touristVoucherDAL);

        Validator validator = new TouristVoucherDataValidator();

        Map<String, Command> commandMap = new HashMap<>();
        Command commandOne = new FindAllTouristVouchersCommand(touristVoucherService);
        Command commandTwo = new FindTouristVouchersByFeedingCommand(touristVoucherService);
        Command commandThree = new OutputGreetingPageCommand();
        Command commandFour = new GetFilePathCommand(touristVoucherService, validator);
        Command commandFive = new FindSortedTouristVouchersByComparatorCommand(touristVoucherService);
        commandMap.put("findAllTouristVouchersCommand", commandOne);
        commandMap.put("findTouristVouchersByFeedingCommand", commandTwo);
        commandMap.put("outputGreetingPageCommand", commandThree);
        commandMap.put("getFilePathCommand", commandFour);
        commandMap.put("findSortedTouristVouchersByComparatorCommand", commandFive);

        CommandFactory commandFactory = new CommandFactoryImpl(commandMap);

        HttpContext serverContext = server.createContext("/touristVouchers", new TouristVouchersHandler(commandFactory));

        serverContext.getFilters().add(new Filter() {
            @Override
            public void doFilter(HttpExchange exchange, Filter.Chain chain) throws IOException {
                LOGGER.info("Hello from filter");
                LOGGER.info("Hello from filter");
                exchange.setAttribute("fromFilter", "filterValue");
                chain.doFilter(exchange);
            }

            @Override
            public String description() {
                return "Simple Filter";
            }
        });
        ExecutorService executor = Executors.newFixedThreadPool(4);
        server.setExecutor(executor);
        server.start();

        LOGGER.info("TouristVouchersApp started on " + server.getAddress().toString());
    }
}