package by.training.katokoleg.dal.comparator;

import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.exceptions.ComparatorFactoryException;
import org.apache.log4j.Level;

import java.util.Comparator;
import java.util.Map;

import static by.training.katokoleg.TouristVouchersApp.LOGGER;

public class ComparatorFactoryImpl implements ComparatorFactory {

    private Map<String,Comparator<TouristVoucher>> comparators;

    public ComparatorFactoryImpl(Map<String, Comparator<TouristVoucher>> comparators) {
        this.comparators = comparators;
    }

    @Override
    public Comparator<TouristVoucher> getComparator(String comparatorName) throws ComparatorFactoryException {
        if (comparators.containsKey(comparatorName)) {
            return comparators.get(comparatorName);
        } else {
            LOGGER.log(Level.ERROR, "Such comparator " + comparatorName + " not exist");
            throw new ComparatorFactoryException("Such comparator " + comparatorName + " not exist");
        }
    }
}