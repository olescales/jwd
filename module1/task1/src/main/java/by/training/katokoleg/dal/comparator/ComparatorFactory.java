package by.training.katokoleg.dal.comparator;

import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.exceptions.ComparatorFactoryException;

import java.util.Comparator;

public interface ComparatorFactory {

    Comparator<TouristVoucher> getComparator (String comparatorName) throws ComparatorFactoryException;

}
