package by.training.katokoleg.exceptions;

public class NoTouristVouchersWithSuchParametersException extends Exception {
    public NoTouristVouchersWithSuchParametersException() {
    }

    public NoTouristVouchersWithSuchParametersException(String message) {
        super(message);
    }
}
