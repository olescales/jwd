package by.training.katokoleg.dal;

import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.exceptions.DALException;

import java.util.Comparator;
import java.util.List;

public interface TouristVoucherDAL {

    List<TouristVoucher> findAllTouristVouchers();

    List<TouristVoucher> findAllTouristVouchersSortedByCondition(String comparator) throws DALException;

    List<TouristVoucher> findTouristVouchersByFeeding(String typeOfFeeding);

    void loadTouristVoucherToDB(TouristVoucher touristVoucher);

}
