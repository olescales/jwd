package by.training.katokoleg.command;

import by.training.katokoleg.controller.validator.Validator;
import by.training.katokoleg.dto.ResponseMessage;
import by.training.katokoleg.service.TouristVoucherService;
import org.apache.log4j.Level;

import java.io.File;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import static by.training.katokoleg.TouristVouchersApp.LOGGER;

public class GetFilePathCommand implements Command {

    private TouristVoucherService touristVoucherService;
    private Validator validator;

    public GetFilePathCommand(TouristVoucherService touristVoucherService, Validator validator) {
        this.touristVoucherService = touristVoucherService;
        this.validator = validator;
    }

    @Override
    public String execute(Map<String, String> request) {

        String filePath = URLDecoder.decode(request.get("absolutePathToFile"), StandardCharsets.UTF_8);
        File fileWithData = new File(filePath);
        boolean isValidFileExtension = validator.validateExtension(filePath);

        if (!fileWithData.exists()) {
            LOGGER.log(Level.DEBUG, "Cant find file with path: " + filePath);
            String template = getStringTemplateFromHTML("/html/greetingTemplate.html");
            return MessageFormat.format(template, "Invalid path: " + filePath + "\r\n Try again");
        }

        if (!isValidFileExtension) {
            LOGGER.log(Level.INFO, "Invalid file extension: " + filePath);
            String template = getStringTemplateFromHTML("/html/greetingTemplate.html");
            return MessageFormat.format(template, "Invalid file extension. File extension must be .txt or .cvs");
        }

        ResponseMessage responseMessage = validator.validateFile(fileWithData);
        if (responseMessage.isValid()) {
            touristVoucherService.loadTouristVoucherToDB(responseMessage.getValidData());
            String template = getStringTemplateFromHTML("/html/fileAddedTemplate.html");
            return MessageFormat.format(template, "Data from file were added successfully", responseMessage.getValidData());
        } else {
            LOGGER.log(Level.DEBUG, responseMessage.getInvalidData());
            String template = getStringTemplateFromHTML("/html/fileAddedTemplate.html");
            String invalidData = getStringFromMap(responseMessage.getInvalidData());
            return MessageFormat.format(template, "File contains invalid data. Check file", invalidData);
        }
    }

    private String getStringFromMap(Map<Integer, List<String>> invalidData) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<Integer, List<String>> pair : invalidData.entrySet()) {
            stringBuilder.append("In line ").append(pair.getKey()).append(System.lineSeparator());
            stringBuilder.append("{").append("\r\n");
            pair.getValue().forEach(stringBuilder::append);
            stringBuilder.append("}").append("\r\n");
        }
        return stringBuilder.toString();
    }
}