package by.training.katokoleg.entity;

public enum TypeOfFeeding {

    RR,
    BB,
    HB,
    FB,
    ALL,
    UALL
}
