package by.training.katokoleg.service;

import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.exceptions.ServiceException;

import java.util.Comparator;
import java.util.List;

public interface TouristVoucherService {

    List<TouristVoucher> findAllTouristVouchers();

    List<TouristVoucher> findTouristVouchersByFeedingType(String typeOfFeeding);

    List<TouristVoucher> findAllTouristVouchersSortedByCondition(String comparator) throws ServiceException;

    void loadTouristVoucherToDB (List<String> dataLine);
}
