package by.training.katokoleg.dal.comparator;

import by.training.katokoleg.entity.TouristVoucher;

import java.util.Comparator;

public class TypeOfTransportComparator implements Comparator<TouristVoucher> {

    @Override
    public int compare(TouristVoucher o1, TouristVoucher o2) {
        return o1.getTypeOfTransport().compareTo(o2.getTypeOfTransport());
    }
}
