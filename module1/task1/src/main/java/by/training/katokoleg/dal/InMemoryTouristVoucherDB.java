package by.training.katokoleg.dal;

import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.entity.TypeOfFeeding;
import by.training.katokoleg.entity.TypeOfRest;
import by.training.katokoleg.entity.TypeOfTransport;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InMemoryTouristVoucherDB {

        public static Set<TouristVoucher> loadTouristVouchers() {
            TouristVoucher seaTour = new TouristVoucher(TypeOfRest.SEATOUR,
                    LocalDate.of(2020,7,26),
                    10,
                    TypeOfTransport.AIR,
                    BigDecimal.valueOf(3203),
                    TypeOfFeeding.ALL);
            TouristVoucher excursion = new TouristVoucher(TypeOfRest.EXCURSION,
                    LocalDate.of(2020,5,30),
                    5,
                    TypeOfTransport.BUS,
                    BigDecimal.valueOf(515));
            TouristVoucher cruise = new TouristVoucher(TypeOfRest.CRUISE,
                    LocalDate.of(2020,4,28),
                    25,
                    TypeOfTransport.CRUISELINER,
                    BigDecimal.valueOf(12500),
                    TypeOfFeeding.ALL);
            TouristVoucher oneMoreSeaTour = new TouristVoucher(TypeOfRest.SEATOUR,
                    LocalDate.of(2020,8,30),
                    12,
                    TypeOfTransport.AIR,
                    BigDecimal.valueOf(3245));
            TouristVoucher treatment = new TouristVoucher(TypeOfRest.TREATMENT,
                    LocalDate.of(2020,5,30),
                    21,
                    TypeOfTransport.TRAIN,
                    BigDecimal.valueOf(1456));
            Set<TouristVoucher> touristVouchers = new HashSet<>();
            touristVouchers.add(seaTour);
            touristVouchers.add(excursion);
            touristVouchers.add(cruise);
            touristVouchers.add(oneMoreSeaTour);
            touristVouchers.add(treatment);
            return touristVouchers;
        }
}
