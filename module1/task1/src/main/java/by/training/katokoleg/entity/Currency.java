package by.training.katokoleg.entity;

public enum Currency {
    BYN,
    USD,
    EUR,
    RUB
}
