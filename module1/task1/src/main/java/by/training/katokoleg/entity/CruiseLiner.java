package by.training.katokoleg.entity;

import java.util.List;

public class CruiseLiner implements Accommodation {

    private List<Room> rooms;

    @Override
    public void provideAccommodation() {

    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }
}
