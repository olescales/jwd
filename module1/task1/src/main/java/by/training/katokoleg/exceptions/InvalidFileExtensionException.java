package by.training.katokoleg.exceptions;

public class InvalidFileExtensionException extends Exception {
    public InvalidFileExtensionException() {
    }

    public InvalidFileExtensionException(String message) {
        super(message);
    }

    public InvalidFileExtensionException(String message, Throwable cause) {
        super(message, cause);
    }
}
