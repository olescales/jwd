package by.training.katokoleg.command;

import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.exceptions.ServiceException;
import by.training.katokoleg.service.TouristVoucherService;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

public class FindSortedTouristVouchersByComparatorCommand implements Command {

    private TouristVoucherService touristVoucherService;

    public FindSortedTouristVouchersByComparatorCommand(TouristVoucherService touristVoucherService) {
        this.touristVoucherService = touristVoucherService;
    }

    @Override
    public String execute(Map<String, String> request) {
        String template = getStringTemplateFromHTML("/html/template.html");
        String comparator = request.get("comparator");
        StringBuilder touristVoucherResponse = new StringBuilder();
        touristVoucherResponse.append("<ul>");
        List<TouristVoucher> touristVouchers = null;
        try {
            touristVouchers = touristVoucherService.findAllTouristVouchersSortedByCondition(comparator);
        } catch (ServiceException e) {
            return MessageFormat.format(template, "Something broken. We're already fixing it. Sorry for disturbance");
        }
        for (TouristVoucher touristVoucher : touristVouchers) {
            touristVoucherResponse.append("<li>");
            touristVoucherResponse.append(touristVoucher.toString());
            touristVoucherResponse.append("</li>");
        }
        touristVoucherResponse.append("</ul>");
        return MessageFormat.format(template, touristVoucherResponse);
    }
}
