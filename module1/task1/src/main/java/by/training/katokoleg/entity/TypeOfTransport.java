package by.training.katokoleg.entity;

public enum TypeOfTransport {

    WITHOUTTRANSPORT,
    AIR,
    TRAIN,
    BUS,
    CAR,
    CRUISELINER;
}
