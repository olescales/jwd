package by.training.katokoleg.command;

import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.service.TouristVoucherService;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.training.katokoleg.TouristVouchersApp.LOGGER;

public class FindTouristVouchersByFeedingCommand implements Command {

    private TouristVoucherService touristVoucherService;

    public FindTouristVouchersByFeedingCommand(TouristVoucherService touristVoucherService) {
        this.touristVoucherService = touristVoucherService;
    }

    @Override
    public String execute(Map<String, String> request) {
        LOGGER.info("findTouristVouchersByFeedingCommand");
        String template = getStringTemplateFromHTML("/html/template.html");

        String typeOfFeeding = request.get("typeOfFeeding");
        StringBuilder touristVoucherResponse = new StringBuilder();
        List<TouristVoucher> touristVouchers = touristVoucherService.findTouristVouchersByFeedingType(typeOfFeeding);
        if (touristVouchers.isEmpty()) {
            touristVoucherResponse.append("No touristVouchersWithSuchParam");
        } else {
            touristVoucherResponse.append("<ul>");
            for (TouristVoucher touristVoucher : touristVouchers) {
                touristVoucherResponse.append("<li>");
                touristVoucherResponse.append(touristVoucher.toString());
                touristVoucherResponse.append("</li>");
            }
            touristVoucherResponse.append("</ul>");
        }
        return MessageFormat.format(template, touristVoucherResponse);
    }
}
