package by.training.katokoleg.command;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.Map;
import java.util.stream.Collectors;

import static by.training.katokoleg.TouristVouchersApp.LOGGER;

public class OutputGreetingPageCommand implements Command {
    @Override
    public String execute(Map<String, String> request) {
        LOGGER.info("outputGreetingPageCommand");
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/html/greetingTemplate.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        String template = bufferedReader.lines().collect(Collectors.joining());
        return MessageFormat.format(template, "Enter filepath");
    }
}
