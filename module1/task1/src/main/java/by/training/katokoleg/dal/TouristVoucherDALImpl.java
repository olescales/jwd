package by.training.katokoleg.dal;

import by.training.katokoleg.dal.comparator.ComparatorFactory;
import by.training.katokoleg.entity.TouristVoucher;
import by.training.katokoleg.exceptions.ComparatorFactoryException;
import by.training.katokoleg.exceptions.DALException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TouristVoucherDALImpl implements TouristVoucherDAL {

    private Set<TouristVoucher> touristVouchers;
    private ComparatorFactory comparatorFactory;

    public TouristVoucherDALImpl(ComparatorFactory comparatorFactory) {
        touristVouchers = InMemoryTouristVoucherDB.loadTouristVouchers();
        this.comparatorFactory = comparatorFactory;
    }

    public TouristVoucherDALImpl() {
        touristVouchers = InMemoryTouristVoucherDB.loadTouristVouchers();
    }

    public Set<TouristVoucher> getTouristVouchers() {
        return touristVouchers;
    }

    @Override
    public List<TouristVoucher> findAllTouristVouchers() {
        return new ArrayList<>(touristVouchers);
    }

    @Override
    public List<TouristVoucher> findAllTouristVouchersSortedByCondition(String comparator) throws DALException {
        Comparator<TouristVoucher> touristVoucherComparator = null;
        try {
            touristVoucherComparator = comparatorFactory.getComparator(comparator);
        } catch (ComparatorFactoryException comparatorException) {
            throw new DALException(comparatorException.getMessage(), comparatorException);
        }
        List<TouristVoucher> touristVouchers = new ArrayList<>(this.touristVouchers);
        touristVouchers.sort(touristVoucherComparator);
        return touristVouchers;
    }

    @Override
    public List<TouristVoucher> findTouristVouchersByFeeding(String typeOfFeeding) {
        return touristVouchers.stream().filter(s -> s.getTypeOfFeeding().toString().equalsIgnoreCase(typeOfFeeding)).collect(Collectors.toList());
    }

    @Override
    public void loadTouristVoucherToDB(TouristVoucher touristVoucher) {
        touristVouchers.add(touristVoucher);
    }
}
