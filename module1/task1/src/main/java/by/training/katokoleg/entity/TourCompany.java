package by.training.katokoleg.entity;

import java.util.List;

public class TourCompany {

    private String name;
    private String address;
    private List<TouristVoucher> tours;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<TouristVoucher> getTours() {
        return tours;
    }

    public void setTours(List<TouristVoucher> tours) {
        this.tours = tours;
    }
}
