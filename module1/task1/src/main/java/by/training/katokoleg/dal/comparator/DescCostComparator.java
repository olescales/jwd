package by.training.katokoleg.dal.comparator;

import by.training.katokoleg.entity.TouristVoucher;

import java.math.BigDecimal;
import java.util.Comparator;

public class DescCostComparator implements Comparator<TouristVoucher> {

    @Override
    public int compare(TouristVoucher o1, TouristVoucher o2) {
        BigDecimal resultInBigDecimal = o1.getCost().subtract(o2.getCost());
        return resultInBigDecimal.toBigInteger().intValue();
    }
}
