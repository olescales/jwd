package by.training.katokoleg.controller;

import by.training.katokoleg.command.Command;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static by.training.katokoleg.TouristVouchersApp.LOGGER;

public class TouristVouchersHandler implements HttpHandler {

    private CommandFactory commandFactory;

    public TouristVouchersHandler(CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        String requestMethod = exchange.getRequestMethod();
        URI requestURI = exchange.getRequestURI();
        LOGGER.info("Received incoming request: " + requestMethod + " URI " + requestURI.toString());

        String requestDataFromQuery = requestURI.getQuery();
        LOGGER.info("Queried: " + requestDataFromQuery);

        LOGGER.info("value from filter:");
        LOGGER.info(exchange.getAttribute("fromFilter"));

        String requestDataFromBody = new BufferedReader(new InputStreamReader(exchange.getRequestBody())).lines().collect(Collectors.joining());

        LOGGER.info("Received body:" + requestDataFromBody);

        Map<String, String> request = new HashMap<>();
        String commandName = "outputGreetingPageCommand";
        if (requestDataFromQuery != null) {
            request = getRequestFromRequestData(requestDataFromQuery);
            commandName = request.get("commandName");
        }
        if (!requestDataFromBody.isEmpty()) {
            request = getRequestFromRequestData(requestDataFromBody);
            commandName = request.get("commandName");
        }
        Command command = commandFactory.getCommand(commandName);
        String view = command.execute(request);

        OutputStream responseBody = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, view.length());
        responseBody.write(view.getBytes());
        responseBody.flush();
        responseBody.close();
    }

    private Map<String, String> getRequestFromRequestData(String requestData) {
        Map<String, String> request = new HashMap<>();
        if (requestData != null) {
            String[] queryParams = requestData.split("&");
            if (queryParams.length > 0) {
                for (String queryParam : queryParams) {
                    String[] parameter = queryParam.split("=");
                    request.put(parameter[0], parameter[1]);
                }
            }
        }
        return request;
    }
}
