package by.training.katokoleg.controller;

import by.training.katokoleg.command.Command;
import java.util.Map;

public class CommandFactoryImpl implements CommandFactory {

    private Map<String, Command> commands;

    public CommandFactoryImpl(Map<String, Command> commands) {
        this.commands = commands;
    }

    @Override
    public Command getCommand(String commandName) {
        return commands.getOrDefault(commandName, answer -> "No such command");
    }
}
