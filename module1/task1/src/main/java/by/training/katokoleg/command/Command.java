package by.training.katokoleg.command;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.stream.Collectors;

public interface Command {

    String execute (Map<String,String> request);

    default String getStringTemplateFromHTML (String pathToHtmlFile) {
        InputStream resourceAsStream = this.getClass().getResourceAsStream(pathToHtmlFile);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        return bufferedReader.lines().collect(Collectors.joining());
    }
}
