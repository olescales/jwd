package by.training.katokoleg.entity;

public interface Accommodation {

    void provideAccommodation ();
}
