package by.training.katokoleg.entity;

public enum TypeOfRest {
    NOTDEFINED,
    SEATOUR,
    EXCURSION,
    TREATMENT,
    SHOPPING,
    CRUISE
}
