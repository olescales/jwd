package by.training.katokoleg.dto;

import java.util.List;
import java.util.Map;

public class ResponseMessage {

    private boolean isValid;
    private Map<Integer,List<String>> invalidData;
    private List<String> validData;

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public Map<Integer, List<String>> getInvalidData() {
        return invalidData;
    }

    public void setInvalidData(Map<Integer, List<String>> invalidData) {
        this.invalidData = invalidData;
    }

    public List<String> getValidData() {
        return validData;
    }

    public void setValidData(List<String> validData) {
        this.validData = validData;
    }

    @Override
    public String toString() {
        return "ResponseMessage{" +
                "isValid=" + isValid +
                ", invalidData=" + invalidData +
                ", validData=" + validData +
                '}';
    }
}
