package by.training.katokoleg.controller.validator;

import by.training.katokoleg.dto.ResponseMessage;
import by.training.katokoleg.entity.TypeOfFeeding;
import by.training.katokoleg.entity.TypeOfRest;
import by.training.katokoleg.entity.TypeOfTransport;
import org.apache.log4j.Level;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static by.training.katokoleg.TouristVouchersApp.LOGGER;

public class TouristVoucherDataValidator implements Validator {

    @Override
    public ResponseMessage validateFile(File fileWithTouristVouchersData) {
        ResponseMessage validationResult = new ResponseMessage();
        List<String> validData = new ArrayList<>();
        Map<Integer, List<String>> invalidData = new HashMap<>();

        int invalidDataLineOrderNumber = 0;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileWithTouristVouchersData), StandardCharsets.UTF_8))) {
            while (reader.ready()) {
                invalidDataLineOrderNumber++;
                String lineWithDataAboutTouristVoucher = reader.readLine();
                List<String> result = validateLine(lineWithDataAboutTouristVoucher);
                if (result.isEmpty()) {
                    validData.add(lineWithDataAboutTouristVoucher);
                } else {
                    invalidData.put(invalidDataLineOrderNumber, result);
                }
            }
            if (invalidData.isEmpty()) {
                validationResult.setValid(true);
            }
            validationResult.setValidData(validData);
            validationResult.setInvalidData(invalidData);
        } catch (Exception e) {
            LOGGER.log(Level.ERROR, e);
        }
        return validationResult;
    }

    @Override
    public boolean validateExtension(String filePath) {
        return filePath.endsWith(".txt") || filePath.endsWith(".csv");
    }

    private List<String> validateLine(String touristVoucherData) {
        List<String> invalidFields = new ArrayList<>();

        if (!touristVoucherData.matches("[^/]+/[^/]+/[^/]+/[^/]+/[^/]+/[^/]+")) {
            invalidFields.add(touristVoucherData);
        } else {
            String[] touristVoucherFieldsForCheck = touristVoucherData.split("/");

            if (!isTypeOfRestFieldValid(touristVoucherFieldsForCheck[0])) {
                invalidFields.add("Type of rest : " + touristVoucherFieldsForCheck[0]);
            }

            if (!isLocalDateFieldValid(touristVoucherFieldsForCheck[1])) {
                invalidFields.add("Date of trip beginning : " + touristVoucherFieldsForCheck[1]);
            }

            if (!isDurationFieldValid(touristVoucherFieldsForCheck[2])) {
                invalidFields.add("duration : " + touristVoucherFieldsForCheck[2]);
            }

            if (!isTypeOfTransportFieldValid(touristVoucherFieldsForCheck[3])) {
                invalidFields.add("Type of transport : " + touristVoucherFieldsForCheck[3]);
            }

            if (!isCostFieldValid(touristVoucherFieldsForCheck[4])) {
                invalidFields.add("Cost : " + touristVoucherFieldsForCheck[4]);
            }

            if (!isTypeOfFeedingFieldValid(touristVoucherFieldsForCheck[5])) {
                invalidFields.add("Type of feeding : " + touristVoucherFieldsForCheck[5]);
            }
        }
        return invalidFields;
    }

    private boolean isTypeOfRestFieldValid(String field) {
        EnumSet<TypeOfRest> enumSet = EnumSet.allOf(TypeOfRest.class);
        return enumSet.stream().map(Enum::toString).anyMatch(type -> type.equalsIgnoreCase(field));
    }

    private boolean isLocalDateFieldValid(String field) {
        return field.matches("\\d{4}-\\d{2}-\\d{2}");
    }

    private boolean isDurationFieldValid(String field) {
        return field.matches("\\d+");
    }

    private boolean isTypeOfTransportFieldValid(String field) {
        EnumSet<TypeOfTransport> enumSet = EnumSet.allOf(TypeOfTransport.class);
        return enumSet.stream().map(Enum::toString).anyMatch(type -> type.equalsIgnoreCase(field));
    }

    private boolean isCostFieldValid(String field) {
        return field.matches("\\d+");
    }

    private boolean isTypeOfFeedingFieldValid(String field) {
        EnumSet<TypeOfFeeding> enumSet = EnumSet.allOf(TypeOfFeeding.class);
        return enumSet.stream().map(Enum::toString).anyMatch(type -> type.equalsIgnoreCase(field));
    }
}