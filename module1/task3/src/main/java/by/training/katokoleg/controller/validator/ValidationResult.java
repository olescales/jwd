package by.training.katokoleg.controller.validator;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {
    private List<String> validData = new ArrayList<>();
    private List<String> invalidData = new ArrayList<>();
    private boolean isValid;

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        this.isValid = valid;
    }

    public List<String> getValidData() {
        return validData;
    }

    public void setValidData(List<String> validData) {
        this.validData = validData;
    }

    public List<String> getInvalidData() {
        return invalidData;
    }

    public void setInvalidData(List<String> invalidData) {
        this.invalidData = invalidData;
    }
}
