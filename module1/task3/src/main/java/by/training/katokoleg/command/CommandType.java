package by.training.katokoleg.command;

public enum CommandType {

    DEPOSIT_PARSE_XML,
    VIEW_DEPOSIT_DETAILS,
    NO_SUCH_COMMAND
}
