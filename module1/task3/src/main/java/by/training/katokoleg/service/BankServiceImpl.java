package by.training.katokoleg.service;

import by.training.katokoleg.dal.BankDAL;
import by.training.katokoleg.entity.Bank;
import by.training.katokoleg.exception.BankServiceException;
import by.training.katokoleg.exception.DAOException;

import java.util.List;

public class BankServiceImpl implements BankService {

    private BankDAL bankDAL;

    public BankServiceImpl(BankDAL bankDAL) {
        this.bankDAL = bankDAL;
    }

    @Override
    public List<Bank> findAll() {
        return bankDAL.findAll();
    }

    @Override
    public void save(Bank bank) {
        bankDAL.save(bank);
    }

    @Override
    public Bank get(Long key) throws BankServiceException {
        try {
            return bankDAL.get(key);
        } catch (DAOException e) {
            throw new BankServiceException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(Long key) throws BankServiceException {
        try {
            bankDAL.delete(key);
        } catch (DAOException e) {
            throw new BankServiceException(e.getMessage(), e);
        }
    }

    @Override
    public void update(Long key, Bank bank) throws BankServiceException {
        try {
            bankDAL.update(key, bank);
        } catch (DAOException e) {
            throw new BankServiceException(e.getMessage(), e);
        }
    }
}
