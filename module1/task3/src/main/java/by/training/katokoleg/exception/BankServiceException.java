package by.training.katokoleg.exception;

public class BankServiceException extends Exception {

    public BankServiceException() {
    }

    public BankServiceException(String message) {
        super(message);
    }

    public BankServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
