package by.training.katokoleg.service.xmlParcers;

import by.training.katokoleg.entity.Bank;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface XMLParser {
    List<Bank> parseXML (InputStream inputStream) throws IOException, SAXException, ParserConfigurationException, XMLStreamException;
}
