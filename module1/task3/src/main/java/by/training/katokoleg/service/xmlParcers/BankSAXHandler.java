package by.training.katokoleg.service.xmlParcers;

import by.training.katokoleg.entity.Bank;
import by.training.katokoleg.entity.Deposit;
import by.training.katokoleg.entity.DepositType;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BankSAXHandler extends DefaultHandler {

    private List<Bank> banks = new ArrayList<>();
    private Bank bank = null;
    private Deposit deposit = null;
    private String content = null;

    public List<Bank> getBanks() {
        return banks;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        switch (qName) {
            case "bank":
                bank = new Bank();
                List<Deposit> deposits = new ArrayList<>();
                bank.setDeposits(deposits);
                break;
            case "otherDeposit":
            case "metalDeposit":
            case "onDemandDeposit":
                deposit = new Deposit();
                deposit.setAccountID(Long.parseLong(attributes.getValue("accountID")));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName) {
            case "bank":
                banks.add(bank);
                break;
            case "name":
                bank.setName(content);
                break;
            case "country":
                bank.setCountry(content);
                break;
            case "depositor":
                deposit.setDepositorsFullName(content);
                break;
            case "amountOnDeposit":
                deposit.setAmountOnDeposit(new BigDecimal(content));
                break;
            case "depositType":
                deposit.setDepositType(DepositType.valueOf(content.toUpperCase()));
                break;
            case "profitability":
                deposit.setProfitability(Double.parseDouble(content));
                break;
            case "timeConstraints":
                deposit.setDepositConstraints(Integer.parseInt(content));
                break;
            case "otherDeposit":
            case "metalDeposit":
            case "onDemandDeposit":
                bank.getDeposits().add(deposit);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        content = String.copyValueOf(ch, start, length).trim();
    }
}
