package by.training.katokoleg.service.xmlParcers;

import by.training.katokoleg.entity.Bank;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class BanksSAXParser implements XMLParser {

    public List<Bank> parseXML(InputStream inputStreamWithXMLData) throws IOException, SAXException, ParserConfigurationException {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = saxParserFactory.newSAXParser();
        BankSAXHandler bankSaxHandler = new BankSAXHandler();
        saxParser.parse(inputStreamWithXMLData, bankSaxHandler);
        return bankSaxHandler.getBanks();
    }
}
