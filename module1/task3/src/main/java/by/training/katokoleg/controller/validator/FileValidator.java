package by.training.katokoleg.controller.validator;

import java.util.Map;

public interface FileValidator {

    ValidationResult validateLineWithData (String data, Map<Integer, String> fieldsForValidation);
}
