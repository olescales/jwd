package by.training.katokoleg.dal;

import by.training.katokoleg.entity.Bank;

public interface BankDAL extends CrudOperation<Long, Bank> {

}
