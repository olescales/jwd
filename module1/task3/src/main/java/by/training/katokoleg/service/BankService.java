package by.training.katokoleg.service;

import by.training.katokoleg.entity.Bank;
import by.training.katokoleg.exception.BankServiceException;

import java.util.List;

public interface BankService {

    List<Bank> findAll();

    void save(Bank bank);

    Bank get(Long key) throws BankServiceException;

    void delete(Long key) throws BankServiceException;

    void update(Long key, Bank bank) throws BankServiceException;

}
