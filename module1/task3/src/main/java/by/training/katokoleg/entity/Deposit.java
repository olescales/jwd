package by.training.katokoleg.entity;

import java.math.BigDecimal;

public class Deposit {

    private Long accountID;
    private String depositorsFullName;
    private BigDecimal amountOnDeposit;
    private DepositType depositType;
    private int depositConstraints;
    private double profitability;

    public Deposit() {
    }

    public Deposit(Long accountID, String depositorsFullName, BigDecimal amountOnDeposit, DepositType depositType, int depositConstraints) {
        this.accountID = accountID;
        this.depositorsFullName = depositorsFullName;
        this.amountOnDeposit = amountOnDeposit;
        this.depositType = depositType;
        this.depositConstraints = depositConstraints;
    }

    public Deposit(Long accountID, String depositorsFullName, BigDecimal amountOnDeposit, DepositType depositType, int depositConstraints, double profitability) {
        this.accountID = accountID;
        this.depositorsFullName = depositorsFullName;
        this.amountOnDeposit = amountOnDeposit;
        this.depositType = depositType;
        this.depositConstraints = depositConstraints;
        this.profitability = profitability;
    }

    public Deposit(Long accountID, String depositorsFullName, BigDecimal amountOnDeposit, DepositType depositType, double profitability) {
        this.accountID = accountID;
        this.depositorsFullName = depositorsFullName;
        this.amountOnDeposit = amountOnDeposit;
        this.depositType = depositType;
        this.profitability = profitability;
    }

    public double getProfitability() {
        return profitability;
    }

    public void setProfitability(double profitability) {
        this.profitability = profitability;
    }

    public Long getAccountID() {
        return accountID;
    }

    public void setAccountID(Long accountID) {
        this.accountID = accountID;
    }

    public String getDepositorsFullName() {
        return depositorsFullName;
    }

    public void setDepositorsFullName(String depositorsFullName) {
        this.depositorsFullName = depositorsFullName;
    }

    public BigDecimal getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public void setAmountOnDeposit(BigDecimal amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
    }

    public DepositType getDepositType() {
        return depositType;
    }

    public void setDepositType(DepositType depositType) {
        this.depositType = depositType;
    }

    public int getDepositConstraints() {
        return depositConstraints;
    }

    public void setDepositConstraints(int depositConstraints) {
        this.depositConstraints = depositConstraints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Deposit)) return false;

        Deposit deposit = (Deposit) o;

        if (depositConstraints != deposit.depositConstraints) return false;
        if (Double.compare(deposit.profitability, profitability) != 0) return false;
        if (accountID != null ? !accountID.equals(deposit.accountID) : deposit.accountID != null) return false;
        if (depositorsFullName != null ? !depositorsFullName.equals(deposit.depositorsFullName) : deposit.depositorsFullName != null)
            return false;
        if (amountOnDeposit != null ? !amountOnDeposit.equals(deposit.amountOnDeposit) : deposit.amountOnDeposit != null)
            return false;
        return depositType == deposit.depositType;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = accountID != null ? accountID.hashCode() : 0;
        result = 31 * result + (depositorsFullName != null ? depositorsFullName.hashCode() : 0);
        result = 31 * result + (amountOnDeposit != null ? amountOnDeposit.hashCode() : 0);
        result = 31 * result + (depositType != null ? depositType.hashCode() : 0);
        result = 31 * result + depositConstraints;
        temp = Double.doubleToLongBits(profitability);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "accountID=" + accountID +
                ", depositorsFullName='" + depositorsFullName + '\'' +
                ", amountOnDeposit=" + amountOnDeposit +
                ", depositType=" + depositType +
                ", depositConstraints=" + depositConstraints +
                ", profitability=" + profitability +
                '}';
    }
}
