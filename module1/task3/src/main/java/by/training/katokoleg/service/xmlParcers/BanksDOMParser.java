package by.training.katokoleg.service.xmlParcers;

import by.training.katokoleg.entity.Bank;
import by.training.katokoleg.entity.Deposit;
import by.training.katokoleg.entity.DepositType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BanksDOMParser implements XMLParser {

    public List<Bank> parseXML(InputStream xmlData) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();;
        List<Bank> banks = new ArrayList<>();
        Document document = documentBuilder.parse(xmlData);

        NodeList nodeList = document.getDocumentElement().getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodeBank = nodeList.item(i);
            if (nodeBank instanceof Element) {
                Bank bank = new Bank();
                NodeList childNodesBanks = nodeBank.getChildNodes();
                List<Deposit> deposits = new ArrayList<>();
                for (int j = 0; j < childNodesBanks.getLength(); j++) {
                    Node childNodeBank = childNodesBanks.item(j);
                    if (childNodeBank instanceof Element) {
                        String bankNodeContent = childNodeBank.getLastChild().getTextContent().trim();
                        switch (childNodeBank.getNodeName()) {
                            case "name":
                                bank.setName(bankNodeContent);
                                break;
                            case "country":
                                bank.setCountry(bankNodeContent);
                                break;
                            case "metalDeposit":
                            case "otherDeposit":
                            case "onDemandDeposit":
                                Deposit deposit = new Deposit();
                                String accountID = childNodeBank.getAttributes().getNamedItem("accountID").getNodeValue();
                                deposit.setAccountID(Long.parseLong(accountID));
                                NodeList childNodesDeposits = childNodeBank.getChildNodes();
                                for (int k = 0; k < childNodesDeposits.getLength(); k++) {
                                    Node childNodeDeposit = childNodesDeposits.item(k);
                                    if (childNodeDeposit instanceof Element) {
                                        String depositNodeContent = childNodeDeposit.getLastChild().getTextContent().trim();
                                        switch (childNodeDeposit.getNodeName()) {
                                            case "depositor":
                                                deposit.setDepositorsFullName(depositNodeContent);
                                                break;
                                            case "amountOnDeposit":
                                                deposit.setAmountOnDeposit(new BigDecimal(depositNodeContent));
                                                break;
                                            case "depositType":
                                                deposit.setDepositType(DepositType.valueOf(depositNodeContent.toUpperCase()));
                                                break;
                                            case "timeConstraints":
                                                deposit.setDepositConstraints(Integer.parseInt(depositNodeContent));
                                                break;
                                            case "profitability":
                                                deposit.setProfitability(Double.parseDouble(depositNodeContent));
                                                break;
                                        }
                                    }
                                }
                                deposits.add(deposit);
                        }
                    }
                    bank.setDeposits(deposits);
                }
                banks.add(bank);
            }
        }
        return banks;
    }
}