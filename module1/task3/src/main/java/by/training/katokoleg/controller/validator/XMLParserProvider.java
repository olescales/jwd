package by.training.katokoleg.controller.validator;

import by.training.katokoleg.service.xmlParcers.XMLParser;

import java.util.HashMap;
import java.util.Map;

public class XMLParserProvider {

    private Map<String, XMLParser> parsers = new HashMap<>();

    public XMLParserProvider(Map<String, XMLParser> parsers) {
        this.parsers = parsers;
    }

    public XMLParser getXMLParser (String xmlParserName) {
        return parsers.get(xmlParserName);
    }

}
