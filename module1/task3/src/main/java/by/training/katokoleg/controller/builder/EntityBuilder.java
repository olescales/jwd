package by.training.katokoleg.controller.builder;

public interface EntityBuilder<T> {

    T build (String lineWithEntityFields);
}
