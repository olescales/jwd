package by.training.katokoleg.service.xmlParcers;

import by.training.katokoleg.entity.Bank;
import by.training.katokoleg.entity.Deposit;
import by.training.katokoleg.entity.DepositType;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BankStAXParser implements XMLParser{

    public List<Bank> parseXML(InputStream xmlData) throws XMLStreamException {
        List<Bank> banks = null;
        Bank currentBank = null;
        List<Deposit> deposits = null;
        Deposit currentDeposit = null;
        String tagContent = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = factory.createXMLStreamReader(xmlData);

        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    if ("bank".equals(reader.getLocalName())) {
                        currentBank = new Bank();
                        deposits = new ArrayList<>();
                    }
                    if ("banks".equals(reader.getLocalName())) {
                        banks = new ArrayList<>();
                    }
                    if ("otherDeposit".equals(reader.getLocalName()) ||
                            "metalDeposit".equals(reader.getLocalName()) ||
                            "onDemandDeposit".equals(reader.getLocalName())) {
                        currentDeposit = new Deposit();
                        currentDeposit.setAccountID(Long.parseLong(reader.getAttributeValue(0)));
                        deposits.add(currentDeposit);
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    tagContent = reader.getText().trim();
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "bank":
                            currentBank.setDeposits(deposits);
                            banks.add(currentBank);
                            break;
                        case "name":
                            currentBank.setName(tagContent);
                            break;
                        case "country":
                            currentBank.setCountry(tagContent);
                            break;
                        case "depositor":
                            currentDeposit.setDepositorsFullName(tagContent);
                            break;
                        case "amountOnDeposit":
                            currentDeposit.setAmountOnDeposit(new BigDecimal(tagContent));
                            break;
                        case "depositType":
                            currentDeposit.setDepositType(DepositType.valueOf(tagContent.toUpperCase()));
                            break;
                        case "profitability":
                            currentDeposit.setProfitability(Double.parseDouble(tagContent));
                            break;
                        case "timeConstraints":
                            currentDeposit.setDepositConstraints(Integer.parseInt(tagContent));
                            break;

                    }
                case XMLStreamConstants.START_DOCUMENT:

                    break;
            }
        }
        return banks;
    }
}
