package by.training.katokoleg.controller.parcer;

import by.training.katokoleg.controller.builder.EntityBuilder;
import by.training.katokoleg.controller.validator.FileValidator;
import by.training.katokoleg.controller.validator.ValidationResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CSVParserImpl<T> implements CSVParser<T> {


    @Override
    public List<T> parse(InputStream inputStream, EntityBuilder<T> builder, FileValidator validator) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        List<T> entities = new ArrayList<>();
        Map<Integer, String> fieldsStorage = new HashMap<>();
        int lineCounter = 0;

        while (reader.ready()) {
            String data = reader.readLine();
            if (lineCounter == 0) {
                String[] entityFields = data.split(",");
                for (String entityField : entityFields) {
                    fieldsStorage.put(lineCounter, entityField);
                    lineCounter++;
                }
            } else {
                ValidationResult validationResult = validator.validateLineWithData(data, fieldsStorage);
                if (validationResult.isValid()) {
                    T entity = (T) builder.build(data);
                    entities.add(entity);
                } else {
                    //?????
                }
            }
        }
        return entities;
    }
}
