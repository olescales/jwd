package by.training.katokoleg.dal;

import by.training.katokoleg.exception.DAOException;

import java.util.List;

public interface CrudOperation<E,T> {

    List<T> findAll();

    void save(T element);

    T get(E key) throws DAOException;

    void update(E key, T element) throws DAOException;

    void delete(E key) throws DAOException;
}
