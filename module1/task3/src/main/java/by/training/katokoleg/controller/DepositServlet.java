package by.training.katokoleg.controller;

import by.training.katokoleg.AppContext;
import by.training.katokoleg.command.Command;
import by.training.katokoleg.command.CommandProvider;
import by.training.katokoleg.command.CommandType;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/deposits/*"})
@MultipartConfig(location = "E:\\Oleg")
public class DepositServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(DepositServlet.class);
    private CommandProvider commandProvider;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        AppContext.getInstance().initialize();
        commandProvider = AppContext.getInstance().getBean(CommandProvider.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Command command = commandProvider.getCommand(CommandType.VIEW_DEPOSIT_DETAILS);
        command.execute(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String commandName = req.getParameter("command");
        Command command = commandProvider.getCommand(CommandType.valueOf(commandName));
        command.execute(req, resp);
        doGet(req, resp);
    }
}
