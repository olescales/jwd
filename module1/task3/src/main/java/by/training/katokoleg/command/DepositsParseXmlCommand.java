package by.training.katokoleg.command;

import by.training.katokoleg.controller.validator.XMLParserProvider;
import by.training.katokoleg.controller.validator.XmlValidator;
import by.training.katokoleg.entity.Bank;
import by.training.katokoleg.service.BankService;
import by.training.katokoleg.service.xmlParcers.XMLParser;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class DepositsParseXmlCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(DepositsParseXmlCommand.class);
    private BankService bankService;
    private XmlValidator xmlValidator;
    private XMLParserProvider xmlParserProvider;

    public DepositsParseXmlCommand(BankService bankService, XmlValidator xmlValidator, XMLParserProvider xmlParserProvider) {
        this.bankService = bankService;
        this.xmlValidator = xmlValidator;
        this.xmlParserProvider = xmlParserProvider;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        Part xmlFile = req.getPart("xmlFile");
        String parserType = req.getParameter("xml");
        InputStream xmlFileInputStream = xmlFile.getInputStream();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream xsdInputStream = classloader.getResourceAsStream("bankDeposits.xsd");
        boolean validationResult = xmlValidator.validateXML(xmlFileInputStream,xsdInputStream);
        InputStream xmlFileForParse = xmlFile.getInputStream();
        if (validationResult) {
            try {
                XMLParser xmlParser = xmlParserProvider.getXMLParser(parserType);
                List<Bank> parseResult = xmlParser.parseXML(xmlFileForParse);
                parseResult.forEach(bankService::save);
            } catch (SAXException e) {
                LOGGER.log(Level.ERROR, "1");
            } catch (ParserConfigurationException e) {
                LOGGER.log(Level.ERROR, "2");
            } catch (XMLStreamException e) {
                LOGGER.log(Level.ERROR, "StAX parser error");
            }
        } else {
            req.getRequestDispatcher("/jsp/page_not_found.jsp").forward(req,resp);
        }
    }
}