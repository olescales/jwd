package by.training.katokoleg.dto;

public class ValidationResponse {

    private boolean isValid;

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}
