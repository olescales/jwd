package by.training.katokoleg.dal;

import by.training.katokoleg.entity.Bank;
import by.training.katokoleg.exception.DAOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class BankDALImpl implements BankDAL {

    private static final Map<Long, Bank> storage = new HashMap<>();
    private static AtomicLong entityCounter = new AtomicLong(0);

    @Override
    public List<Bank> findAll() {
        return new ArrayList<>(storage.values());
    }

    @Override
    public void save(Bank element) {
        storage.put(entityCounter.incrementAndGet(), element);
    }

    @Override
    public Bank get(Long key) throws DAOException {
        if (storage.containsKey(key)) {
            return storage.get(key);
        } else {
            throw new DAOException("bank with" + key + "not exist");
        }
    }

    @Override
    public void update(Long key, Bank element) throws DAOException {
        if (storage.containsKey(key)) {
            storage.put(key,element);
        } else {
            throw new DAOException("bank with" + key + "not exist");
        }
    }

    @Override
    public void delete(Long key) throws DAOException {
        if (storage.containsKey(key)) {
            storage.remove(key);
        } else {
            throw new DAOException("bank with" + key + "not found");
        }
    }
}
