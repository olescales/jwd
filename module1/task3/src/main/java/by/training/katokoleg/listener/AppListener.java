package by.training.katokoleg.listener;

import by.training.katokoleg.AppContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class AppListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        AppContext.getInstance().initialize();
        System.out.println("init");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        AppContext.getInstance().destroy();
        System.out.println("destroyed");
    }
}
