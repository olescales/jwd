package by.training.katokoleg.entity;

import java.util.List;

public class Bank {

    private String name;
    private String country;
    private List<Deposit> deposits;

    public Bank() {
    }

    public Bank(String name, String country) {
        this.name = name;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Deposit> getDeposits() {
        return deposits;
    }

    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bank)) return false;

        Bank bank = (Bank) o;

        if (name != null ? !name.equals(bank.name) : bank.name != null) return false;
        if (country != null ? !country.equals(bank.country) : bank.country != null) return false;
        return deposits != null ? deposits.equals(bank.deposits) : bank.deposits == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (deposits != null ? deposits.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", deposits=" + deposits +
                '}';
    }
}
