package by.training.katokoleg.command;

import by.training.katokoleg.entity.Bank;
import by.training.katokoleg.service.BankService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ViewDepositDetailsCommand implements Command {

    private BankService bankService;

    public ViewDepositDetailsCommand(BankService bankService) {
        this.bankService = bankService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        List<Bank> banks = bankService.findAll();
        req.setAttribute("parsingResult", banks);
        req.getRequestDispatcher("jsp/watch_deposit_list.jsp").forward(req,resp);
    }
}
