package by.training.katokoleg.controller.validator;

import by.training.katokoleg.dto.ValidationResponse;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

public interface XmlValidator {

    boolean validateXML (InputStream xmlFile, InputStream xsdFile);
}
