package by.training.katokoleg.controller.parcer;

import by.training.katokoleg.controller.builder.EntityBuilder;
import by.training.katokoleg.controller.validator.FileValidator;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface CSVParser<T> {

    List<T> parse(InputStream inputStream, EntityBuilder<T> builder, FileValidator validator) throws IOException;
}
