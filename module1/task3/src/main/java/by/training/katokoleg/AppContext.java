package by.training.katokoleg;

import by.training.katokoleg.command.*;
import by.training.katokoleg.controller.validator.XMLParserProvider;
import by.training.katokoleg.controller.validator.XmlValidator;
import by.training.katokoleg.controller.validator.XmlValidatorImpl;
import by.training.katokoleg.dal.BankDAL;
import by.training.katokoleg.dal.BankDALImpl;
import by.training.katokoleg.service.BankService;
import by.training.katokoleg.service.BankServiceImpl;
import by.training.katokoleg.service.xmlParcers.BankStAXParser;
import by.training.katokoleg.service.xmlParcers.BanksDOMParser;
import by.training.katokoleg.service.xmlParcers.BanksSAXParser;
import by.training.katokoleg.service.xmlParcers.XMLParser;

import java.util.HashMap;
import java.util.Map;

public class AppContext {

    private static final AppContext INSTANCE = new AppContext();
    private final Map<Class<?>, Object> beans = new HashMap<>();

    private AppContext() {
    }

    public static AppContext getInstance() {
        return INSTANCE;
    }

    public void initialize() {
        BankDAL bankDAL = new BankDALImpl();
        BankService bankService = new BankServiceImpl(bankDAL);

        XmlValidator xmlValidator = new XmlValidatorImpl();

        Map<String, XMLParser> parsers = new HashMap<>();
        XMLParser domParser = new BanksDOMParser();
        XMLParser saxParser = new BanksSAXParser();
        XMLParser staxParser = new BankStAXParser();
        parsers.put("domParser", domParser);
        parsers.put("saxParser", saxParser);
        parsers.put("staxParser", staxParser);
        XMLParserProvider xmlParserProvider = new XMLParserProvider(parsers);

        Map<Enum<CommandType>, Command> commands = new HashMap<>();
        Command command1 = new DepositsParseXmlCommand(bankService, xmlValidator, xmlParserProvider);
        Command command2 = new ViewDepositDetailsCommand(bankService);
        commands.put(CommandType.DEPOSIT_PARSE_XML, command1);
        commands.put(CommandType.VIEW_DEPOSIT_DETAILS, command2);
        CommandProvider commandProvider = new CommandProvider(commands);

        beans.put(bankDAL.getClass(), bankDAL);
        beans.put(bankService.getClass(), bankService);
        beans.put(xmlValidator.getClass(), xmlValidator);
        beans.put(xmlParserProvider.getClass(), xmlParserProvider);
        beans.put(commandProvider.getClass(), commandProvider);
    }

    public void destroy() {
        beans.clear();
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) beans.get(clazz);
    }
}
