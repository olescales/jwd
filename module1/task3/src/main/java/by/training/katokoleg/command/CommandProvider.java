package by.training.katokoleg.command;

import java.util.HashMap;
import java.util.Map;

public class CommandProvider {

    private Map<Enum<CommandType>, Command> commands = new HashMap<>();

    public CommandProvider(Map<Enum<CommandType>, Command> commands) {
        this.commands = commands;
    }

    public Command getCommand (CommandType commandName) {
        return commands.getOrDefault(commandName, new NoSuchCommand());
    }
}