<%--
  Created by IntelliJ IDEA.
  User: Олег
  Date: 17.05.2020
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Page for xml parsing</title>
</head>
<body>
        <form action="deposits" method="post" enctype="multipart/form-data">
            <label>Upload xml file here
                <input type="file" name="xmlFile" value="">
            </label>
            <input type="hidden" name="command" value="DEPOSIT_PARSE_XML">
            <select name="xml" id="parsers">
                <option value="domParser" name="parserType">DOMParser</option>
                <option value="saxParser" name="parserType">SAXParser</option>
                <option value="staxParser" name="parserType">StAXParser</option>
            </select>
            <button type="submit">Parse file</button>
        </form>
</body>
</html>
