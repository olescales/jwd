package by.training.katokoleg.service.parcers;

import by.training.katokoleg.entity.Bank;
import by.training.katokoleg.entity.Deposit;
import by.training.katokoleg.entity.DepositType;
import by.training.katokoleg.service.xmlParcers.BanksDOMParser;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class BanksDOMParserTest {

    @Test
    public void parseXMLTestIsOk() throws IOException, SAXException, ParserConfigurationException {
        BanksDOMParser parser = new BanksDOMParser();
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("bankDeposits.xml");
            List<Bank> actual = parser.parseXML(inputStream);
            List<Bank> expected = getBankListForEquality();
            Assertions.assertEquals(expected, actual);
    }

    private List<Bank> getBankListForEquality () {
        List<Bank> banks = new ArrayList<>();

        Bank belgazpromBank = new Bank("Belgazprombank","Belarus");
        List<Deposit> depositsBGB = new ArrayList<>();
        belgazpromBank.setDeposits(depositsBGB);
        Deposit depositBGB1 = new Deposit(1L,"Oleg",new BigDecimal("1000"), DepositType.METAL,1000);
        Deposit depositBGB2 = new Deposit(2L,"Olga",new BigDecimal("2000"), DepositType.SAVINGS,12,8d);
        Deposit depositBGB3 = new Deposit(3L,"Ira",new BigDecimal("3000"), DepositType.ONDEMAND, 15d);
        depositsBGB.add(depositBGB1);
        depositsBGB.add(depositBGB2);
        depositsBGB.add(depositBGB3);

        Bank belAgroPromBank = new Bank("Belagroprombank","Belarus");
        List<Deposit> depositsBAB = new ArrayList<>();
        belAgroPromBank.setDeposits(depositsBAB);
        Deposit depositBAB1 = new Deposit(1L,"Oleg",new BigDecimal("1000"), DepositType.METAL,1000);
        Deposit depositBAB2 = new Deposit(2L,"Olga",new BigDecimal("2000"), DepositType.ACCUMULATIVE,12,8d);
        Deposit depositBAB3 = new Deposit(3L,"Ira",new BigDecimal("3000"), DepositType.ONDEMAND, 15d);
        depositsBAB.add(depositBAB1);
        depositsBAB.add(depositBAB2);
        depositsBAB.add(depositBAB3);

        Bank belarusBank = new Bank("BelarusBank","Belarus");
        List<Deposit> depositsBB = new ArrayList<>();
        belarusBank.setDeposits(depositsBB);
        Deposit depositBB1 = new Deposit(231L,"Oleg",new BigDecimal("1000"), DepositType.METAL,1000);
        Deposit depositBB2 = new Deposit(63L,"Sergei",new BigDecimal("2456"), DepositType.SETTLEMENT,12,8d);
        Deposit depositBB3 = new Deposit(412L,"Leontiy",new BigDecimal("3400"), DepositType.ONDEMAND, 6d);
        depositsBB.add(depositBB1);
        depositsBB.add(depositBB2);
        depositsBB.add(depositBB3);

        banks.add(belAgroPromBank);
        banks.add(belgazpromBank);
        banks.add(belarusBank);

        return banks;
    }
}